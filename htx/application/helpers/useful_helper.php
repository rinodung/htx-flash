<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('time4caithien'))
{
	function time4caithien($cur)
	{
		$CI =& get_instance();
		$CI->load->model("admin/mcauhinh");
                //$CI->mcauhinh->function();
                $n = (int)$CI->mcauhinh->get_HK_CaiThien();
                if($cur >= getHK() - $n)
                    return TRUE;
                return FALSE;
	}
}

if ( ! function_exists('getHK'))
{
	function getHK()
	{
		$CI =& get_instance();
                $thang = (int)(date("n"));
                $nam = (int)(date("y"));
                if($thang>=9)
                    return ($nam - $CI->session->userdata('K') - 5) * 2 + 1;
                return ($nam - $CI->session->userdata('K') - 5) * 2;
	}
}

if ( ! function_exists('getFileSize'))
{
	function getFileSize($size)
	{
            if($size < 1024)
                return $size." B"; 
            elseif ($size >= 1024 && $size < 1048576) 
                return ceil($size/1024)." KB";
            elseif ($size >= 1048576 && $size < 1073741824)
                return ceil($size/1048576)." MB";
            return ceil($size/1073741824)." MB";
	}
}

if( !function_exists('destroy_dir'))
{
    function destroy_dir($dir) 
    { 
        if (!is_dir($dir) || is_link($dir)) 
            return unlink($dir); 
        foreach (scandir($dir) as $file) 
        { 
            if ($file == '.' || $file == '..') 
                continue; 
            if (!destroy_dir($dir . DIRECTORY_SEPARATOR . $file)) 
            { 
                chmod($dir . DIRECTORY_SEPARATOR . $file, 0777); 
                if (!destroy_dir($dir . DIRECTORY_SEPARATOR . $file)) return false; 
            }; 
        } 
        return rmdir($dir); 
    } 
}

function KhongDau($str) 
{ 
	  $coDau=array("Ô","Ơ","Ư","Ê","Ă","Â","ô","ơ","ư","ê","ă","â", "Ă ","Ă¡","áº¡","áº£","Ă£","Ă¢","áº§","áº¥","áº­","áº©","áº«","Äƒ", 
	  "áº±","áº¯","áº·","áº³","áºµ", 
	  "Ă¨","Ă©","áº¹","áº»","áº½","Ăª","á»"     ,"áº¿","á»‡","á»ƒ","á»…", 
	  "Ă¬","Ă­","á»‹","á»‰","Ä©", 
	  "Ă²","Ă³","á»","á»","Ăµ","Ă´","á»“","á»‘","á»™","á»•","á»—","Æ¡" 
	  ,"á»","á»›","á»£","á»Ÿ","á»¡", 
	  "Ă¹","Ăº","á»¥","á»§","Å©","Æ°","á»«","á»©","á»±","á»­","á»¯", 
	  "á»³","Ă½","á»µ","á»·","á»¹", 
	  "Ä‘", 
	  "Ă€","Ă","áº ","áº¢","Ăƒ","Ă‚","áº¦","áº¤","áº¬","áº¨","áºª","Ä‚" 
	  ,"áº°","áº®","áº¶","áº²","áº´", 
	  "Ăˆ","Ă‰","áº¸","áºº","áº¼","Ă","á»€","áº¾","á»†","á»‚","á»„", 
	  "ĂŒ","Ă","á»","á»ˆ","Ä¨", 
	  "Ă’","Ă“","á»Œ","á»","Ă•","Ă”","á»’","á»","á»˜","á»”","á»–","Æ " 
	  ,"á»œ","á»","á»¢","á»","á» ", 
	  "Ă™","Ă","á»¤","á»¦","Å¨","Æ¯","á»ª","á»¨","á»°","á»¬","á»®", 
	  "á»²","Ă","á»´","á»¶","á»¸", 
	  "Ä","Ăª","Ă¹","Ă "); 

	  $khongDau=array("O","O","U","E","A","A","o","o","u","e","a","a","a","a","a","a","a","a","a","a","a","a","a" 
	  ,"a","a","a","a","a","a", 
	  "e","e","e","e","e","e","e","e","e","e","e", 
	  "i","i","i","i","i", 
	  "o","o","o","o","o","o","o","o","o","o","o","o" 
	  ,"o","o","o","o","o", 
	  "u","u","u","u","u","u","u","u","u","u","u", 
	  "y","y","y","y","y", 
	  "d", 
	  "A","A","A","A","A","A","A","A","A","A","A","A" 
	  ,"A","A","A","A","A", 
	  "E","E","E","E","E","E","E","E","E","E","E", 
	  "I","I","I","I","I", 
	  "O","O","O","O","O","O","O","O","O","O","O","O" 
	  ,"O","O","O","O","O", 
	  "U","U","U","U","U","U","U","U","U","U","U", 
	  "Y","Y","Y","Y","Y", 
	  "D","e","u","a");
	  return str_replace($coDau,$khongDau,$str); 
 }

/* End of file url_helper.php */
/* Location: ./system/helpers/url_helper.php */
?>