$(document).ready(function()
{
    $("body").keydown(function(e)
    {        
        if(e.keyCode==27)close_popup();
      
    }); 
   
    $(".overflow,#pclose").click(function()
    {
        close_popup();
    });
     
});

function open_popup(object)
{    

    $(object).css("width","1030px");
	$(".overflow").css("visibility","visible");
	$(object).css({
		
		top: '10px',
		right: '200px'
	});
    $(".overflow").css("visibility","visible");
    reset_all();
}
function close_popup()
{

	$(".overflow").css("visibility","hidden");
    $(".popup_detail").css("width","0px");
	$(".popup_detail").css({
            
            top: '0px',
            right: '10px' 
        });
    $(".overflow").css("visibility","hidden");
}
function reset_all()
{
    $(".popup_detail #pfooter img#save,.popup_detail #pfooter img#process,.popup_detail #pfooter h4").hide();
}

