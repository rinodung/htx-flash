$(document).ready(function()
{
    $("body").keydown(function(e)
    {        
        if(e.keyCode==27)close_popup();
      
    }); 
   
    $(".overflow,#pclose").click(function()
    {
        close_popup();
    });
     
});

function open_popup(object)
{    
    $(object).show();
    $(".overflow").show();
    reset_all();
}
function open_popup_add(object,data)
{    
    /*
        data[0]==>title
        data[1]==>pdata height
    */
    
    what=data[1]; 
//    alert(what);
    switch(what)
    {
        
        case "add_khoa":
        {
            title=data[0];           
            $(object+" p#ptitle").html(title);
            html="";
            html+='<table class="info" id="add_khoa">'+
                    '<tr><td>Mã Khoa</td><td><input id="makhoa" type="text" /></td></tr>'+
                    '<tr><td>Tên Khoa</td><td><input id="tenkhoa" type="text" /></td></tr>'+
                 '</table>'+
                 '<table class="error">'+
                 '<tr><td>*</td></tr>'+
                 '<tr><td>*</td></tr>'+
                 '</table>';
            break;
        }//end case add_khoa
        
        case "add_k":
        {
            title=data[0];           
            $(object+" p#ptitle").html(title);
            html="";
            html+='<table class="info" id="add_k">'+
                    '<tr><td>Mã Khóa</td><td><input id="mak" type="text" /></td></tr>'+
                    '<tr><td>Tên Khóa</td><td><input id="tenk" type="text" /></td></tr>'+
                 '</table>'+
                 '<table class="error">'+
                 '<tr><td>*</td></tr>'+
                 '<tr><td>*</td></tr>'+
                 '</table>';
            break;
        }//end case add_k
    }
    $(object+" #pdata").html(html);
    $(object).show();    
    $(".overflow").show();
    reset_all();
}

function open_popup_edit(object,data)
{    
    //alert(object);
    /*
        data[0]==>title
        data[1]==>pdata height
    */
    what=data[1];
    switch(what)
    {
       
        case "edit_khoa":
        {
           title=data[0];
       
            makhoa=data[2];
            tenkhoa=data[3];    
            $(object+" p#ptitle").html(title);
            html="";
            html+='<table class="info" id="edit_khoa">'+
                    '<tr><td>Mã Khoa</td><td><input id="makhoa" title="'+makhoa+'" type="text" value="'+makhoa+'" /></td></tr>'+
                    '<tr><td>Tên Khoa</td><td><input id="tenkhoa" type="text" value="'+tenkhoa+'" /></td></tr>'+
                 '</table>'+
                 '<table class="error" id="'+makhoa+'">'+
                 '<tr><td></td></tr>'+
                 '<tr><td></td></tr>'+
                 '</table>';
            break;
        }
        
        case "edit_k":
        {
            
             title=data[0];
            what=data[1];
            mak=data[2];
            tenk=data[3];    
            $(object+" p#ptitle").html(title);
            html="";
            html+='<table class="info" id="edit_k">'+
                    '<tr><td>Mã Khóa</td><td><input id="mak" title="'+mak+'" type="text" value="'+mak+'" /></td></tr>'+
                    '<tr><td>Tên Khóa</td><td><input id="tenk" type="text" value="'+tenk+'" /></td></tr>'+
                 '</table>'+
                 '<table class="error">'+
                 '<tr><td></td></tr>'+
                 '<tr><td></td></tr>'+
                 '</table>';
            break;
        }
    }//end switch
    $(object+" #pdata").html(html);
    $(object).show();    
    $(".overflow").show();
    reset_all();
}
function close_popup()
{
    $(".popup_detail").hide();
    $(".overflow").hide();
}
function reset_all()
{
    $(".popup_detail #pfooter img#save,.popup_detail #pfooter img#process,.popup_detail #pfooter h4").hide();
}

