$(document).ready(function()
{
    
    
   
//alert("jcauhinh.js");

   
    $("#thoigian table .datepicker").datepick({
                                   minDate: new Date(),
		                          dateFormat:'dd/mm/yyyy',
                                  onSelect: function(dates)
                                  { 
                                     box=$(this).parents(".data_box");        
                                     button=box.children("button");
                                     button.fadeIn(500);
                                  } 
   });
    
//======SU KIEN BUTTON CLICK BOX THOI GIAN DKHP==============================================================    
    $("#thoigian button").click(function()
    {
        time_start=$("#thoigian table input#start").val();
        time_end=$("#thoigian table input#end").val();
        
        //alert(time_start+" "+time_end);
        if(time_start>time_end) alert("Ngày bắt đầu lớn hơn ngày kết thúc");
        else
        {
            $.ajax(
            {
                url:"/cauhinh/update_thoigian",
                type:"POST",  
                data:{time_start:time_start,time_end:time_end},
                timeout: 5000,//5s
                beforeSend: function()
                    {
                        $("#thoigian button").fadeOut(600);
                        //$("#thoigian img#loading").show();
                        
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Quá trình lưu thất bại. Thử lại sau");
                   
                },             
                success:function(result)
                {                
                   if(result=="success")
                   {    
                       // alert(result);
                    
                        $("#thoigian button").fadeOut(600);
                        
                   }
                   else
                   {
                    alert(result);
                        
                        $("#thoigian button").show();
                        alert("Quá trình lưu thất bại. Lỗi có thể do kiểu dữ liệu, dữ liệu trống");
                   }
                }
            });
        }
       
    });//#thoigian button click
    
    
    
//======SU KIEN BUTTON CLICK BOX SO TIN CHI DKHP==============================================================   
    $("#sotc button").click(function()
    {
        tctd=$("#sotc table input#tctd").val();
        tctt=$("#sotc table input#tctt").val();
        
        
        $.ajax(
        {
            url:"/cauhinh/update_tinchi",
            type:"POST",  
            data:{tctd:tctd,tctt:tctt},
            timeout: 5000,//5s
            beforeSend: function()
                {
                    $("#sotc button").fadeOut(600);
                    
                    
                },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Quá trình lưu thất bại. Thử lại sau");
               
            },             
            success:function(result)
            {                
               if(result=="success")
               {    
                   // alert(result);                
                    $("#sotc button").fadeOut(600);
               }
               else
               {
                //alert(result);
                    
                    
                    alert("Quá trình lưu thất bại. Lỗi có thể do dữ liệu trống hoặc kiểu dữ liệu(phải là số nguyên dương 1,2,3..30) hoặc chứa kí tự đặc biệt");
                    $("#sotc button").show();
               }
            }
        });
       
    });
//======SU KIEN IPUT KEY UP==============================================================   
   
    $(".data_box table input").keyup(function()
    {
        
        box=$(this).parents(".data_box");        
        button=box.children("button");
        //alert(button.html());
       // alert(new_value);
       button.fadeIn(600);
    });
//==============QUY DINH VE KHOA=======================================================
    //Them khoa moi
    $(".data_box#khoa img.add").click(function()
    {
         
         arr=new Array();
        arr[0]="Thao tác thêm khoa mới";
        arr[1]="add_khoa";
        open_popup_add(".popup_detail#add",arr);
    });
    //Them k moi
    $(".data_box#k img.add").click(function()
    {
        //alert("me");
        arr=new Array();
        arr[0]="Thao tác thêm khóa mới";
        arr[1]="add_k";
        open_popup_add(".popup_detail#add",arr);
    });
    //sua doi thong tin khoa
    $(".data_box#khoa img.edit").click(function()
    {   
        current_row=$(this).parents("tr");
        makhoa=current_row.children("td#makhoa").html();
        tenkhoa=current_row.children("td#tenkhoa").html();
        
        
         arr=new Array();
        arr[0]="Thao tác sửa đổi thông tin khoa";
        arr[1]="edit_khoa";
        arr[2]=makhoa;
        arr[3]=tenkhoa;
       // alert(makhoa+" "+tenkhoa);
        open_popup_edit(".popup_detail#edit",arr);
    });
    
    //sua doi thong tin khoa
    $(".data_box#k img.edit").click(function()
    {   
        current_row=$(this).parents("tr");
        mak=current_row.children("td#mak").html();
        tenk=current_row.children("td#tenk").html();

        
         arr=new Array();
        arr[0]="Thao tác sửa đổi thông tin khoá";
        arr[1]="edit_k";
        arr[2]=mak;
        arr[3]=tenk;
       // alert(makhoa+" "+tenkhoa);
        open_popup_edit(".popup_detail#edit",arr);
    });
    //thao tac tao moi
    $(".popup_detail#add img#save").click(function()
    {
        what=$(".popup_detail#add table").attr("id");
        
       switch(what)
       {
         case "add_khoa":
         {
            makhoa=$(".popup_detail#add input#makhoa").val();
            tenkhoa=$(".popup_detail#add  input#tenkhoa").val();        
            //alert(what+" "+makhoa+" "+tenkhoa);
            $.ajax(
             {
                url:"/cauhinh/ajax_insert_khoa",
                type:"POST",
                data:{makhoa:makhoa,tenkhoa:tenkhoa},
                timeout: 10000,//10s
                beforeSend: function()
                    {
                        enable_footer(0,0);                	
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    enable_footer(1,1);  
                    alert("Thao tác thất bại. Thử lại sau.");
                    
                    },   
                success:function(result)
                {   
                    //alert(result);
                    if(result=="success")
                    {
                       window.location.reload(true);
                    }
                    else
                    { 
                      $(".popup_detail#add table.error").html(result);
                      enable_footer(1,1);  
                    }
                }//end success                
            }); //end ajax             
            break;
         }//end case 
         
         case "add_k":
         {
            
            mak=$(".popup_detail#add input#mak").val();
            tenk=$(".popup_detail#add  input#tenk").val();        
            //alert(what+" "+makhoa+" "+tenkhoa);
            $.ajax(
             {
                url:"/cauhinh/ajax_insert_k",
                type:"POST",
                data:{mak:mak,tenk:tenk},
                timeout: 10000,//10s
                beforeSend: function()
                    {
                        enable_footer(0,0);                	
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    enable_footer(1,1);  
                    alert("Thao tác thất bại. Thử lại sau.");
                    
                    },   
                success:function(result)
                {   
                    //alert(result);
                    if(result=="success")
                    {
                       window.location.reload(true);
                    }
                    else
                    { 
                      $(".popup_detail#add table.error").html(result);
                      enable_footer(1,1);  
                    }
                }//end success                
            }); //end ajax             
            break;
         }//end case add_k
         
       }//end switch
          
    });//end save  click to add khoa
    
    
    //thao tac luu sua doi
    $(".popup_detail#edit img#save").click(function()
    {
        what=$(".popup_detail#edit table").attr("id");
        
       switch(what)
       {
        
         case "edit_khoa":
         {  
            key=$(".popup_detail#edit input#makhoa").attr("title");
            makhoa=$(".popup_detail#edit input#makhoa").val();
            tenkhoa=$(".popup_detail#edit  input#tenkhoa").val();
            // alert(what+" "+makhoa+" "+tenkhoa+" "+key);  
            $.ajax(
             {
                url:"/cauhinh/ajax_edit_khoa",
                type:"POST",
                data:{key:key,makhoa:makhoa,tenkhoa:tenkhoa},
                timeout: 10000,//10s
                beforeSend: function()
                    {
                        enable_footer(0,0);                	
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    enable_footer(1,1);  
                    alert("Thao tác thất bại. Thử lại sau.");
                    
                    },   
                success:function(result)
                {   
                    //alert(result);
                    if(result=="success")
                    {
                       window.location.reload(true);
                    }
                    else
                    { 
                      $(".popup_detail#edit table.error").html(result);
                      enable_footer(1,1);  
                    }
                }//end success                
            }); //end ajax             
            break;
         }//end case add_khoa
         
         case "edit_k":
         {
            
            key=$(".popup_detail#edit input#mak").attr("title");
            mak=$(".popup_detail#edit input#mak").val();
            tenk=$(".popup_detail#edit  input#tenk").val();
            //alert(what+" "+mak+" "+tenk+" "+key);  
            $.ajax(
             {
                url:"/cauhinh/ajax_edit_k",
                type:"POST",
                data:{key:key,mak:mak,tenk:tenk},
                timeout: 10000,//10s
                beforeSend: function()
                    {
                        enable_footer(0,0);                	
                    },
                error: function (xhr, ajaxOptions, thrownError) {
                    enable_footer(1,1);  
                    alert("Thao tác thất bại. Thử lại sau.");
                    
                    },   
                success:function(result)
                {   
                    //alert(result);
                    if(result=="success")
                    {
                       window.location.reload(true);
                    }
                    else
                    { 
                      $(".popup_detail#edit table.error").html(result);
                      enable_footer(1,1);  
                    }
                }//end success                
            }); //end ajax             
            break;
         }//end case add_k
         
       }//end switch
          
        
         
            
      
    });//end save  click to add khoa
    
    $(".data_box#khoa img.del").click(function()
    {
        
         makhoa=$(this).attr("id");
         cur_row=$(this).parents("tr");
         
         
         conf=confirm("Nhắc nhở: Khi xóa dữ liệu từ khoa sẽ có ảnh hưởng đến toàn bộ hệ thống. Bạn có thật sự muốn xóa "+makhoa+" này không?  " ); 
          
        if(conf) 
        {            
            $.ajax({
                        url:"/cauhinh/ajax_delete_khoa",
                        type:"POST",
                        data:{makhoa:makhoa},
                        success: function(result)
                        {
                            if(result=="success")
                            {
                               cur_row.hide();
                               sokhoa=$(".data_box#khoa table th#sokhoa").html()-1;
                               $(".data_box#khoa table th#sokhoa").html(sokhoa);
                               
                               //window.location.reload(true);
                            }
                            else alert(result);
                            //window.location.reload(true);
                        }
                    });//end ajax_del
        }//end confirm
    });
    
     $(".data_box#k img.del").click(function()
    {
        
         mak=$(this).attr("id");
         cur_row=$(this).parents("tr");
         
         
         conf=confirm("Nhắc nhở: Khi xóa dữ liệu từ khóa sẽ có ảnh hưởng đến toàn bộ hệ thống. Bạn có thật sự muốn xóa "+mak+" này không?  " ); 
          
        if(conf) 
        {            
            $.ajax({
                        url:"/cauhinh/ajax_delete_k",
                        type:"POST",
                        data:{mak:mak},
                        success: function(result)
                        {
                            if(result=="success")
                            {
                               cur_row.hide();
                               sok=$(".data_box#k table th#sok").html()-1;
                               $(".data_box#k table th#sok").html(sok);
                               
                               //window.location.reload(true);
                            }
                            else alert(result);
                            //window.location.reload(true);
                        }
                    });//end ajax_del
        }//end confirm
    });
    
    $(".popup_detail #pdata table.info select").live("change",function()
    {
        enable_footer(1,0);
    });
      
    $(".popup_detail #pdata table.info input").live("keyup",function()
    {
        enable_footer(1,0);
    });
});
function enable_footer(save,h4)
{
        
    if(save==0)
    {     
      $(".popup_detail #pfooter img#save").hide();     
      $(".popup_detail #pfooter img#process").show();  
    }
    else  
    {
        $(".popup_detail #pfooter img#process").hide();
        $(".popup_detail #pfooter img#save").show();     
       
    }
    
    if(h4==0) 
    {
     $(".popup_detail #pdata table.error").hide();   
     $(".popup_detail #pfooter h4").hide();   
    }    
    else
    {
        $(".popup_detail #pdata table.error").show();
       $(".popup_detail #pfooter h4").show();  
    }     
    
}





















