$(document).ready(function()
{	
    
    $("#info a").click(function()
    {
        var link=$(this).attr("href");
        if(link!="")
        {
            var info= $(link).offset();
    		$(link).focus();
    		
    		
    		$('html, body').animate({scrollTop: info.top}, 800);    
        }
        
    
    });
    
    	$('#camera_wrap').camera({height: '480px',
											pagination: false,
											thumbnails: true,
											});
                                            
                                                
	$("#linklogin p,a#login").click(function(e)
	{
	   e.preventDefault();
		openPopup();
	});
	
	$("#popup").click(function(e)
	{
		closePopup();
	});
    $("#formlogin img#close").click(function(e)
	{
		closePopup();
	});
	
	 $('*').keyup(function(e){

        if(e.keyCode=='27')
		{
			closePopup();
        }         

    });
	
	function openPopup()
	{
		$("#popup").show();
		$("#formlogin").show();
	}
	
	function closePopup()
	{
		$("#popup").hide();
		$("#formlogin").hide();
	}
    
     $("#wrapper #top").SetScroller({	velocity: 	 30,
											direction: 	 'horizontal',
											startfrom: 	 'left',
											loop:		 'infinite',
											movetype: 	 'pingpong',
											onmouseover: 'pause',
											onmouseout:  'play',
											onstartup: 	 'play',
											cursor: 	 'pointer'
										});	
                                        
    $(this).scroll(function()
    {
        if($(this).scrollTop()!=0)
        {
            $('#ontop').fadeIn();
        }
        else
        {
            $('#ontop').fadeOut();
        }    
    });                                    
    
    $('#ontop').click(function()
    {
        $('body,html').animate({scrollTop:0},800);
    }
    );
                                 
});