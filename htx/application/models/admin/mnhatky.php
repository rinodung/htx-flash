<?php

class Mnhatky extends CI_Model 
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
	function get_log($type,$start,$limit,$date=0)
    {
        
        $result=null;
        if($type=="online")
        {   
                        
            if($date!=0)
            {
                
                $this->db->where("time_start >=",$date);
                $this->db->where("time_start <",$date+86400);
            }
            $this->db->limit($limit,$start);
            $query=$this->db->get("log_room");
            $result=$query->result_object();
            
        }
        return $result;
    }
    
    //lay du lieu khi search
    function get_search_log($search,$type,$start,$limit)
    {
        $result=null;
        if($type=="online")
        {                
            
            $str="SELECT l.id,l.MaMH,l.MaLop,l.MaGV, l.time_start,l.time_end,l.total FROM log_room as l,giaovien,monhoc 
                  WHERE l.MaMH=monhoc.MaMH 
                  and   l.MaGV=giaovien.MaGV 
                  and   (monhoc.TenMH like '".$search."'
                  or   giaovien.TenGV like '".$search."')
                  LIMIT $start,$limit";
            
            $query=$this->db->query($str);
            $result=$query->result_object();
            
        }
        return $result;
    }
    //lay tong so hang
    function get_num_rows($search,$type,$date=0)
    {
        $num=0;
        if($type=="online")
        {   if($search!="")
            {
                $str="SELECT l.id,l.MaMH,l.MaLop,l.MaGV, l.time_start,l.time_end,l.total FROM log_room as l,giaovien,monhoc 
                  WHERE l.MaMH=monhoc.MaMH 
                  and   l.MaGV=giaovien.MaGV 
                  and   (monhoc.TenMH like '".$search."'
                  or   giaovien.TenGV like '".$search."') ";
                  
                
                
                $query=$this->db->query($str);
                echo $str;
                $num=$query->num_rows();
              
            }
            else
            {
                if($date!=0)
                {
                    
                    $this->db->where("time_start >=",$date);
                    $this->db->where("time_start <",$date+86400);
                }
                $num=$this->db->count_all_results("log_room");
            }
                
        }
        return $num;
    }
    function get_date_log($type="online")
    {
        $result=NULL;
        if($type=="online")
        {
            $this->db->select("time_start");
            $query=$this->db->get("log_room");
            $result=$query->result_object();
            
        }
        return $result;
    }

	
}

?>