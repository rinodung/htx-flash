<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cauhinh extends CI_Controller 
{
    
     function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
       $this->load->library("form_validation");        
        $this->load->model("admin/mcauhinh");
        $this->load->library('session');
    }
    public function index()
    {
        
        
        $data["time_start"]=$this->mcauhinh->get_time_start();
        $data["time_end"]=$this->mcauhinh->get_time_end();
        $data["TCTD"]=$this->mcauhinh->get_tctd();
        $data["TCTT"]=$this->mcauhinh->get_tctt();       
        
        $khoa_result=$this->mcauhinh->get_khoa();
        $data["khoa_result"]=$khoa_result;
        $data["num_khoa"]=count($khoa_result);
        
        $k_result=$this->mcauhinh->get_K();
        $data["k_result"]=$k_result;
        $data["num_k"]=count($k_result);
        
        
        $phong_result=$this->mcauhinh->get_phong();
        $data["phong_result"]=$phong_result;
        $data["num_phong"]=count($phong_result);
        
        $thu_result=$this->mcauhinh->get_thu();
        $data["thu_result"]=$thu_result;
        $data["num_thu"]=count($thu_result);
        
        $ca_result=$this->mcauhinh->get_ca();
        $data["ca_result"]=$ca_result;
        $data["num_ca"]=count($ca_result);
        
        $data["title"]="Trang cấu hình hệ thống";
        $this->load->view("admin/vcauhinh.php",$data);
    }
    
    
    //==========LUU THOI GIAN DKHP=========================================================================================    
    public function update_thoigian()
    {
            $this->load->library("form_validation");     
            $this->form_validation->set_rules('time_start','Thời gian bắt đầu',"required|exact_length[10]");//kiem tra khoa chinh
            $this->form_validation->set_rules('time_end','Thời gian kết thúc',"required|exact_length[10]");
            
            if($this->form_validation->run() ==false)
            {
              //  echo validation_errors();
                
                echo form_error("time_start");
                echo form_error("time_end");
            } 
            else 
            {
               
               $data["GiaTri"]=trim($this->input->post("time_start"));
               
               $this->db->update("cauhinh",$data,array("DK"=>"time_start"));
               
               $data2["GiaTri"]=trim($this->input->post("time_end"));               
               $this->db->update("cauhinh",$data2,array("DK"=>"time_end"));
               
               echo "success";
            }
    }
	
    
    
    
    
    
    
    
//==========LUA CAU HINH SO TIN CHI TOI DA, SO TIN CHI TOI THIEU=========================================================================================    
    public function update_tinchi()
    {
            $this->load->library("form_validation");     
            $this->form_validation->set_rules('tctd', 'Số tín chỉ tối đa', "required|is_natural_no_zero");//kiem tra khoa chinh
            $this->form_validation->set_rules('tctt', 'Số tín chỉ tối thiểu', "required|is_natural_no_zero");            
            
            
            
            if($this->form_validation->run() ==false)
            {
                //echo validation_errors();
                
                echo form_error("tctd");
                echo form_error("tctt");
            } 
            else 
            {
               
               $data["GiaTri"]=trim($this->input->post("tctd"));
               
               $this->db->update("cauhinh",$data,array("DK"=>"tctd"));
               
               $data2["GiaTri"]=trim($this->input->post("tctt"));               
               $this->db->update("cauhinh",$data2,array("DK"=>"tctt"));
               echo "success";
            }
    }
    
    
    
    //==========THEM KHOA MOI=========================================================================================    
    public function ajax_insert_khoa()
    {
            $key="";
            $this->load->library("form_validation");     
            $this->form_validation->set_rules('makhoa', 'Mã Khoa', "required|max_length[4]|callback_check_makhoa[$key]");//kiem tra khoa chinh
            $this->form_validation->set_rules('tenkhoa', 'Tên Khoa', "required");            
            
            
            
            if($this->form_validation->run() ==false)
            {
                //echo validation_errors();
                
                echo "<tr><td>".form_error("makhoa","<span title='Thông báo lỗi'>","</span>")."</td></tr>";
                echo "<tr><td>".form_error("tenkhoa","<span title='Thông báo lỗi'>","</span>")."</td></tr>";
            } 
            else 
            {
               
               $data["MaKhoa"]=strtoupper(trim($this->input->post("makhoa")));
               $data["TenKhoa"]=strtoupper(trim($this->input->post("tenkhoa")));
               $this->db->insert("khoa",$data);
               echo "success";
            }
    }
    //==========THEM K MOI=========================================================================================
    public function ajax_insert_k()
    {
            $key="";
            $this->load->library("form_validation");     
            $this->form_validation->set_rules('mak', 'Mã Khóa', "required|is_natural_no_zero|callback_check_mak[$key]");//kiem tra khoa chinh
            $this->form_validation->set_rules('tenk', 'Tên Khóa', "required");            
            
            
            
            if($this->form_validation->run() ==false)
            {
                //echo validation_errors();
                
                echo "<tr><td>".form_error("mak","<span title='Thông báo lỗi'>","</span>")."</td></tr>";
                echo "<tr><td>".form_error("tenk","<span title='Thông báo lỗi'>","</span>")."</td></tr>";
            } 
            else 
            {
               
               $data["MaK"]=strtoupper(trim($this->input->post("mak")));
               $data["TenK"]=strtoupper(trim($this->input->post("tenk")));
               $this->db->insert("k",$data);
               echo "success";
            }
    }
    
    //==========LUA SUA DOI KHOA=========================================================================================    
    public function ajax_edit_khoa()
    {
            $key=$this->input->post("key");
            $this->load->library("form_validation");     
            $this->form_validation->set_rules('makhoa', 'Mã Khoa', "required|max_length[4]|callback_check_makhoa[$key]");//kiem tra khoa chinh
            $this->form_validation->set_rules('tenkhoa', 'Tên Khoa', "required");            
            
            
            
            if($this->form_validation->run() ==false)
            {
                //echo validation_errors();
                
                echo "<tr><td>".form_error("makhoa","<span title='Thông báo lỗi'>","</span>")."</td></tr>";
                echo "<tr><td>".form_error("tenkhoa","<span title='Thông báo lỗi'>","</span>")."</td></tr>";
            } 
            else 
            {
               
               $data["MaKhoa"]=strtoupper(trim($this->input->post("makhoa")));
               $data["TenKhoa"]=trim($this->input->post("tenkhoa"));
               $this->db->update("khoa",$data,array("MaKhoa"=>$key));
               echo "success";
            }
    }
    
    //==========LUA SUA DOI K=========================================================================================    
    public function ajax_edit_k()
    {
            $key=$this->input->post("key");
            $this->load->library("form_validation");     
            $this->form_validation->set_rules('mak', 'Mã Khoa', "required|is_natural_no_zero|callback_check_mak[$key]");//kiem tra khoa chinh
            $this->form_validation->set_rules('tenk', 'Tên Khoa', "required");            
            
            
            
            if($this->form_validation->run() ==false)
            {
                //echo validation_errors();
                
                echo "<tr><td>".form_error("mak","<span title='Thông báo lỗi'>","</span>")."</td></tr>";
                echo "<tr><td>".form_error("tenk","<span title='Thông báo lỗi'>","</span>")."</td></tr>";
            } 
            else 
            {
               
               $data["MaK"]=strtoupper(trim($this->input->post("mak")));
               $data["TenK"]=strtoupper(trim($this->input->post("tenk")));
               $this->db->update("k",$data,array("MaK"=>$key));
               echo "success";
            }
    }
    public function ajax_delete_khoa()
    {
            $makhoa=$this->input->post("makhoa");
            //echo $makhoa;
            $this->db->delete("khoa",array("MaKhoa"=>$makhoa));
            echo "success";
    }
    public function ajax_delete_k()
    {
            $mak=$this->input->post("mak");
            //echo $makhoa;
            $this->db->delete("k",array("MaK"=>$mak));
            echo "success";
    }
    
	














































    function check_makhoa($new_makhoa,$old_makhoa)
       {
            
            if($new_makhoa!=$old_makhoa)
            {
                if($this->mcauhinh->makhoa_exist($new_makhoa)) 
                {
                    $this->form_validation->set_message("check_makhoa","<span title='Thông báo lỗi'><b style='color:red'>".$new_makhoa."</b> đã tồn tại.</span>");
                    return false;   
                }
                else return true;
            }
            else return true;
       }
       
    function check_mak($new_mak,$old_mak)
       {
            
            if($new_mak!=$old_mak)
            {
                if($this->mcauhinh->mak_exist($new_mak)) 
                {
                    $this->form_validation->set_message("check_mak","<span title='Thông báo lỗi'><b style='color:red'>".$new_mak."</b> đã tồn tại.</span>");
                    return false;   
                }
                else return true;
            }
            else return true;
       }
    
    function check_valid_date($date)
    {
        $date_format = 'dd/mm/yyyy'; /* use dashes - dd/mm/yyyy */
    
        $date = trim($date);
        /* UK dates and strtotime() don't work with slashes, 
        so just do a quick replace */
        $date = str_replace('/', '-', $date); 
    
    
        $time = strtotime($date);
    
        $is_valid = date($date_format, $time) == $date;
    
        if($is_valid)
        {        
            return true;
        }
        else
        {
            $this->form_validation->set_message("check_valid_date","Kiểu ngày tháng không hợp lệ");
             return false;
        }
    
        /* not a valid date..return false */
       
    }

}//end class