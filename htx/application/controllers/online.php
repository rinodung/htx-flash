<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Online extends CI_Controller {

	
    public function getTime()
    {
        $res["index"] = $_POST["i"];
        $res["bd"] = $_POST["bd"];
        $res["kt"] = $_POST["kt"];
        $res["sec"] = time();
        echo json_encode($res);
    }
	 
	public function index()
	{  
		
		$this->load->helper("url");
		$this->load->library('session');
		$this->load->model('index/mlogin');
		$this->load->model('admin/mcauhinh');
		$Type = $this->session->userdata('Type');
		$data['MSSV'] = $MSSV = $this->session->userdata('name');
		$data['khoa'] = $khoa = $this->session->userdata('khoa');
		$data['K'] = $K = $this->session->userdata('K');
		$data['TKB'] = $this->mlogin->getTKB($MSSV, $khoa);
		$data['MonDK'] = $this->mlogin->getMonDK($MSSV, $khoa);
		$data['TCTD'] = $this->mlogin->TCTD();		
                $data['learn'] = $learn = $this->mcauhinh->time4learn();
		$temp = $this->mlogin->getLopMoSvByInt(time() + 7*3600, $MSSV, $khoa);
		$lops = array();
                if($learn)
                {
                    foreach ($temp->result() as $row)
                    {
                            $lops[$row->MaLop]["malop"] = $row->MaLop;
                            $lops[$row->MaLop]["tenmh"] = $row->TenMH;
                            $lops[$row->MaLop]["tengv"] = $row->TenGV;
                            $lops[$row->MaLop]["BD"] = $row->BatDau;
                            $lops[$row->MaLop]["KT"] = $row->KetThuc;
                    }
                }
		$data['lops'] = $lops;
		switch($khoa)
		{
			case "mmt":
				$data['TenSV'] = $this->mlogin->getNameMMT($MSSV);
				$data['sotc'] = $this->mlogin->soTC($MSSV, $khoa);
				break;
			case "cnpm":
				$data['TenSV'] = $this->mlogin->getNameCNPM($MSSV);
				$data['sotc'] = $this->mlogin->soTC($MSSV, $khoa);
				break;
			case "khmt":
				$data['TenSV'] = $this->mlogin->getNameKHMT($MSSV);
				$data['sotc'] = $this->mlogin->soTC($MSSV, $khoa);
				break;
			case "ktmt":
				$data['TenSV'] = $this->mlogin->getNameKTMT($MSSV);
				$data['sotc'] = $this->mlogin->soTC($MSSV, $khoa);
				break;
			case "httt":
				$data['TenSV'] = $this->mlogin->getNameHTTT($MSSV);
				$data['sotc'] = $this->mlogin->soTC($MSSV, $khoa);
				break;
		}
		$this->load->view('index/vlopsv', $data);
        
	  
	}
	public function log()
	{
		
		 $this->load->database(); //connect to database     
		$action=$this->input->post("action");
        date_default_timezone_set('Asia/Ho_Chi_Minh');	
		switch($action)
		{
			case "add":
			{
                $data["id"]=NULL;
				$data["MaLop"]=$this->input->post("malop");
				$data["MaMH"]=$this->input->post("mamh");
                $data["MaGV"]=$this->input->post("magv");
				$data["time_start"]=time();
                $data["time_end"]=time()+3600;
                $data["total"]=rand(10, 50);;
				$this->db->insert("log_room",$data);
				echo "added|".$data["MaLop"]." ".$data["MaMH"]." ".$data["MaGV"]." successfully";
				break;
			}
			case "getlist":
			{
				
				$query=$this->db->get("log_room");
				$result=$query->result_object();
				$data=$action."|";
				foreach($result as $row)
				{
					$data.=$row->id."&".$row->MaMH."&".$row->MaLop."&".$row->MaGV."&".$row->total;
					$data.="|";
					
					
				}
				
				echo $data;
				break;
			}
		}
	}
	
}
?>
