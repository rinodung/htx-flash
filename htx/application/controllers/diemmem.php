<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diemmem extends CI_Controller 
{
    function __construct()
    {
	parent::__construct();
	$this->load->helper(array('form', 'url'));
		
        $this->load->library('session');
        $this->load->model("admin/mdiemmem");
    }
	
    public function get4iframe($malop="NT101.C11")
    {
        $data['res'] = $this->mdiemmem->getLopAll($malop);
        $data['malop'] = $malop;
        
        $this->load->view('admin/vdiemmem',$data);  
    }
    
    public function ghichu()
    {
        $malop = $_POST['malop'];
        $masv = $_POST['masv'];
        $nd = $_POST['nd'];
        $this->mdiemmem->update($masv, $malop, "GhiChu", $nd);
    }
    
    public function diemdanh()
    {
        $malop = $_POST['malop'];
        $masv = $_POST['masv'];
        $this->mdiemmem->diemdanh($malop, $masv);
    }
    
    public function chamdiem()
    {
        $malop = $_POST['malop'];
        $masv = $_POST['masv'];
        $cot = $_POST['cot'];
        $diem = $_POST['d'];
        if($cot==1)
            $this->mdiemmem->update($masv, $malop, "GiuaKy", $diem);
        else
            $this->mdiemmem->update($masv, $malop, "CuoiKy", $diem);
    }
    
    public function update()
    {
        $malop = $_POST['malop'];
        $masv = $_POST['masv'];
        $col = $_POST['col'];
        $data = $_POST['data'];
        if($data=="" && $col!="GhiChu" && $col!="DiemDanh")
            echo $data = 'NULL';
        $this->mdiemmem->update($masv, $malop, $col, $data);
    }
    
}