<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller 
{
	function _construct()
	{
		parent::_construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
	}
	
	public function index()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
		//d? có session
		$name = $this->session->userdata('name');
		if($name!= false)
		{
			$Type = $this->session->userdata('Type');
			if($Type == 0)
			{
				header('Location: '.base_url().'trangchu');
				return;
			}
			elseif($Type == 1)
			{
				header('Location: '.base_url().'lich-giang-day');
				return;
			}
			header('Location: '.base_url().'thoi-khoa-bieu');
			return;
		}
		$this->load->model('index/mlogin');
		$data['cap'] = $this->mlogin->getCaptcha(33, 200, 3000);
		$this->load->view('index/vlogin', $data);
	}
}