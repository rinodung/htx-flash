<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of cauhoi
 *
 * @author map
 */
class cauhoi extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        $this->load->library("form_validation");        
        $this->load->model("admin/mcauhoi");
        $this->load->library('session');
    }
    
    public function get_all_questions($magv, $mamh)
    {
        $questions = $this->mcauhoi->statistic($magv, $mamh);
        echo json_encode($questions);
    }
    
    //$magv,$mamh,$nd,$a,$b,$c,$d,$ans,$tg,$file
    public function addques()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST')
            return;
        $magv=$_POST['magv'];$mamh=$_POST['mamh'];$nd=$_POST['nd'];$a=$_POST['a'];$b=$_POST['b'];
        $c=$_POST['c'];$d=$_POST['d'];$ans=$_POST['ans'];$tg=$_POST['tg'];$ext="";
        
        if (!isset($_FILES["file"]) || $_FILES["file"]["error"] > 0)
        {
        }
        else
        {
            $img = true;
            $filename = $_FILES["file"]["name"];
            $name = substr($filename, 0, strrpos($filename, "."));
            $ext = substr($filename, strrpos($filename, ".") + 1);
            
            /*
            echo "Upload: " . $_FILES["file"]["name"] . "<br>";
            echo "Type: " . $_FILES["file"]["type"] . "<br>";
            echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
            echo "Stored in: " . $_FILES["file"]["tmp_name"];
            */
        }
        
        
        $id = $this->mcauhoi->add($magv,$mamh,$nd,$a,$b,$c,$d,$ans,$tg,$ext);
        var_dump($_FILES);
        var_dump($ext);
        if(strcmp($ext, "") != 0)
            // Save file
            move_uploaded_file($_FILES["file"]["tmp_name"], "application/uploads/img_question/$id.$ext");
        echo  $id;
    }
    
    
    //$id, $nd,$a,$b,$c,$d,$ans,$tg,$img
    public function editques()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST')
            return;
        $id=$_POST['id'];$nd=$_POST['nd'];$a=$_POST['a'];$b=$_POST['b'];
        $c=$_POST['c'];$d=$_POST['d'];$ans=$_POST['ans'];$tg=$_POST['tg'];$ext="";
        
        if (!isset($_FILES["file"]) || $_FILES["file"]["error"] > 0)
        {
        }
        else
        {
            $img = true;
            $filename = $_FILES["file"]["name"];
            $name = substr($filename, 0, strrpos($filename, "."));
            $ext = substr($filename, strrpos($filename, ".") + 1);
            
            /*
            echo "Upload: " . $_FILES["file"]["name"] . "<br>";
            echo "Type: " . $_FILES["file"]["type"] . "<br>";
            echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
            echo "Stored in: " . $_FILES["file"]["tmp_name"];
            */
        }
        
        
        $this->mcauhoi->edit($id,$nd,$a,$b,$c,$d,$ans,$tg,$ext);
        if(strcmp($ext, "") != 0)
            // Save file
            move_uploaded_file($_FILES["file"]["tmp_name"], "application/uploads/img_question/$id.$ext");
        echo  $id;
        var_dump($_FILES);
        var_dump($ext);
    }
    
    public function delques($id)
    {
        $this->mcauhoi->delete($id);
    }


    //-------------------------------------------G?i c�c Iframe------------------------
    public function add_ques_iframe($magv, $mamh)
    {
        $data['magv'] = $magv;
        $data['mamh'] = $mamh;
        $this->load->view("admin/vcauhoi_iframe", $data);
    }
    
    public function edit_ques_iframe($mach)
    {
        $data['mach'] = $mach;
        $cauhoi = $this->mcauhoi->get_cauhoi($mach);
        //var_dump($cauhoi);
        $data['id'] = $mach;
        $data['nd'] = $cauhoi->ND;
        $data['a'] = $cauhoi->A;
        $data['b'] = $cauhoi->B;
        $data['c'] = $cauhoi->C;
        $data['d'] = $cauhoi->D;
        $data['ans'] = $cauhoi->Ans;
        $data['img'] = $cauhoi->Img;
        $temp = $cauhoi->ThoiGian;
        $arr = explode("m", $temp);
        $data["min"] = $arr[0];
        $data["sec"] = 0;
		if(isset($arr[1]))
			$data["sec"] = $arr[1];
		$data["edit"] = TRUE;
        $this->load->view("admin/vcauhoi_iframe", $data);
    }
    
}

?>
