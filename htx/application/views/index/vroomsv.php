<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
	<meta name="author" content="rinodung" />

	<title>Phòng học trực tuyến</title>
	
	
	<link href="<?php echo static_url();?>/css/menu/menu2.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo static_url();?>/css/index/room.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?php echo static_url();?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo static_url();?>/js/jquery-scroller.js"></script>
	<script type="text/javascript" src="<?php echo static_url();?>/js/jroom.js"></script>
    <script type="text/javascript" src="<?php echo static_url();?>/js/index/last_active.js"></script>    
	
	<script type="text/javascript">	
		var user = "<?php echo $MSSV; ?>";
		var KT = new Date();
		KT.setHours(<?php echo $GioKT; ?>);
		KT.setMinutes(<?php echo $PhutKT; ?>);
		
		function move()
		{
			var cur = new Date();
			if(cur.getTime() > KT.getTime())
			{
				alert("Lớp học đã kết thúc.");
				window.location.assign("<?php echo base_url()."hoc-truc-tuyen"; ?>");
			}
		}
		
		$(document).ready(function()
		{	
			setInterval(move, 10000);
		});
	</script>
	

</head>
<body>
    <div id="wrapper">
		<div id="header">
        
            
            <img src="<?php echo static_url();?>/images/index/logo.png" />
            <div id="banner">
                <p>Đại Học Quốc Gia Thành Phố Hồ Chí Minh</p>
                <p>Trường Đại Học Công Nghệ Thông Tin</p>
                <p>Hệ Thống Đăng Ký Học Phần</p>
            </div><!-- end #banner -->       
            
            
             <div id="contact">
            <ul>
                <li>
                
                <p><b>Email:</b>  online_citd@gmail.com</p>
                </li>
                <li>
                
                <p><b>Fax:</b>  08 8818818</p>
                </li>
                <li>
                
                <p><b>Phone:</b> 84 01699938919</p>
                </li>
                
               
            </ul>
            </div>
        
        </div><!-- end #header -->
        
        <?php include_once("vmenu2.php");  ?>
		
			
        
            <div id="flashcontent">                      
                                  
                <object type="application/x-shockwave-flash" data="<?php echo base_url(); ?>/application/static/flashs/sv/sinhvien.swf" width="1280" height="680" id="demo">
    					<param name="movie" value="sinhvien.swf" />
    					<param name="quality" value="high" />
    					<param name="bgcolor" value="#ffffff" />
    					<param name="play" value="true" />
    					<param name="loop" value="true" />
    					<param name="wmode" value="window" />
    					<param name="scale" value="showall" />
    					<param name="menu" value="true" />
    					<param name="devicefont" value="false" />
    					<param name="salign" value="" />
    					<param name="allowScriptAccess" value="sameDomain" />
    					<param name="FlashVars" value="<?php echo $flashvar_str; ?>" />
    					
    				
    				</object>
            
            </div><!--end #flashcontent -->
        
        <span id="info" style="display:none"  ><?php echo $name ?></span>
        <span id="username" style="display:none" ><?php echo $name ?></span>
      
      <div id="gioithieu">
            <!-- 
                <div id="linklogin">
                <p>Đăng nhập</p>
                </div>
                -->                
                
                <div id="quyche1" class='box'>
                    <h4>Giới thiệu hệ thống giảng dạy trực tuyến đa tương tác UIT-ONLINE</h4>
                    <div id="box_data">
                    <p>Hệ thống giảng dạy trực tuyến đa tương tác UIT-ONLINE là một giải pháp được đưa ra để đáp ứng nhu cầu học trực tuyến, hệ đào tạo từ xa ngày càng trở nên phổ biến
                     trong hệ thống giáo dục ngày này. Hệ thống này có thể đáp ứng được tất cả các môn học ở phổ thông như: anh văn, toán, lý, hóa cũng như những môn 
                     chuyên nghành ở các trường đại học cao đẳng như: Tin học đại cương, Toán cao cấp, Mạng máy tính cơ bản... </p>
                     
                     <img src="<?php echo static_url(); ?>/images/index/giaovien_demo.jpg" />
                     
                     <p>Hệ thống đăng ký học phần đơn giản và hiệu quả giúp cho học viên dễ dàng đăng ký môn học trước khi tham gia vào khóa học của mình.
                     Giao diện đăng ký học phần hệ thống giảng dạy trực tuyến đa tương tác UIT-ONLINE
                     </p>
                     <img src="<?php echo static_url(); ?>/images/index/dkhp_demo.png" />
                     
                     <p>Giao diện đăng ký học phần hệ thống giảng dạy trực tuyến đa tương tác UIT-ONLINE</p>                     
                     <img src="<?php echo static_url(); ?>/images/index/sinhvien_demo.jpg" />
                     
                     <p>Hệ thống đăng ký học phần đơn giản và hiệu quả giúp cho học viên dễ dàng đăng ký môn học trước khi tham gia vào khóa học của mình.
                     </p>
                     <img src="<?php echo static_url(); ?>/images/index/presentation_demo.jpg" />
                     
                     <p>Giáo viên chia sẽ màn hình cho học viên quan sát và thực hành theo
                     </p>
                     <img src="<?php echo static_url(); ?>/images/index/screenshare_demo.jpg" />
                     
                    <p>Hệ thống giảng dạy trực tuyến đa tương tác UIT-ONLINE sử dụng công nghệ truyền dữ liệu theo thời gian thực giúp cho học viên có thể quan sát hình ảnh và nghe được giọng nói của
                    giáo viên đang đứng lớp, hệ thống cung cấp các chức năng giúp cho học viên có một môi trường học trực tuyến giống với môi trường học truyền thống thông thường như bảng trắng
                    phấn đen, trình chiếu bài giảng, giơ tay phát biểu, thảo luận nhóm, tin nhắn trò chuyện. 
                    </p>
                    <p>Hệ thống xây dựng trên nền tảng thuần web tạo sự dễ dàng và tiện lợi trong quá trình sử dụng của học viên. Chỉ cần một hai cái nhấp chuột trên trình
                    duyệt thân thuộc như Chrome, Firefox, IE là học viên có thể tham gia vào một buổi học trực tuyến ngay lập tức.</p>       
                    <p>Quy trình đăng ký sử dụng hệ thống như sau: </p>              
                    <ol>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    </ol>
                    </div>
                </div>
               
            </div><!--end gioi thieu -->
            
            <div id="huongdan">
                
                <div id="quyche1" class='box'>
                    <h4>Hướng dẫn sử dụng hệ thống UIT-ONLINE</h4>
                    <div id="box_data">
                    <p>Để quá trình đăng ký học phần được diễn ra nhanh chóng và hiệu quả. Phòng đào tạo đề nghị sinh viên nghiêm chỉnh chấp hành những quy định sau:</p>
                    <ol>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    </ol>
                    </div>
                </div>
                <div id="quyche2" class='box'>
                    <h4>Kế hoạch đăng ký học phần học kỳ 1 2012 - 2013</h4>
                    <div id="box_data">
                    <p>Để quá trình đăng ký học phần được diễn ra nhanh chóng và hiệu quả. Phòng đào tạo đề nghị sinh viên nghiêm chỉnh chấp hành những quy định sau:</p>
                    <ol>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    </ol>
                    </div>
                </div>
            </div><!--end gioi thieu -->
    </div><!-- end #wrapper -->


</body>
</html>