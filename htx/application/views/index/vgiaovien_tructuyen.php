<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="rinodung" />
       
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/giaovien_tructuyen.css" />
    <link href="<?php echo static_url();?>/css/menu/menu2.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/popup/popup_css.css" />
	<link href="<?php echo static_url();?>/css/index/lop.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/index/last_active.js"></script>
	<script type="text/javascript">
	
		var base_url = "<?php echo base_url(); ?>";
		var user = "<?php echo $magv; ?>";
		var lops = new Array();
		<?php
			$i = 0;
			foreach($lops as $lop)
			{
				$bd = explode('h', $lop['BD']);
				$kt = explode('h', $lop['KT']);
				echo "var temp = new Array();";
				echo "temp[0] = ".$bd[0].";";
				echo "temp[1] = ".$bd[1].";";
				echo "temp[2] = ".$kt[0].";";
				echo "temp[3] = ".$kt[1].";";
				echo "lops[".$i."] = temp;";
				$i++;
			}
		?>
			
		function kt()
		{
			var n = lops.length;
			for(var i=0; i<n; i++)
			{
				var BD = new Date();
				var KT = new Date();
				BD.setHours(parseInt(lops[i][0]));
				BD.setMinutes(parseInt(lops[i][1]));
				KT.setHours(parseInt(lops[i][2]));
				KT.setMinutes(parseInt(lops[i][3]));
				BD.setTime(BD.getTime() - 15*60000);
				KT.setTime(KT.getTime() + 15*60000);
                                $.ajax(
                                {
                                    type:"POST",
                                    url: base_url + "online/getTime",
                                    data:{i:i, bd:BD.getTime(), kt:KT.getTime()},
                                    timeout: 5000,
                                    success:function(res)
                                    {
                                        var data = $.parseJSON(res);
                                        var cur = new Date(data.sec * 1000);
                                        var bd1 = new Date(data.bd *1);
                                        var kt1 = new Date(data.kt *1);
                                        var index = data.index;
                                        if(cur.getTime() >= bd1.getTime() && cur.getTime() < kt1.getTime())
                                        {
                                                $("#lop"+index).attr("class", "roomopen");
                                        }
                                        else
                                        {
                                                $("#lop"+index).attr("class", "roomclose");
                                        }
                                    }
                                });
			}
		}
		
		$(document).ready(function()
		{			
			$("div.roomopen").live("click", function()
			{
				var malop = $(this).attr("malop");
				var tgkt = $(this).find("span.tgkt").html();
				$("#ketthuc").val(tgkt);
				$("#malop").val(malop);
				$("form#lop").submit();
			});
			kt();
			setInterval(kt, 10000);
		});
	</script>
    
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/admin/jgiaovien_tructuyen.js"></script>        
	<title><?php echo $title ?></title>
</head>

<body>
<div id="wrapper">
    <div id="header">
        
            
            <img src="<?php echo static_url();?>/images/index/logo.png" />
            <div id="banner">
                <p>Đại Học Quốc Gia Thành Phố Hồ Chí Minh</p>
                <p>Trường Đại Học Công Nghệ Thông Tin</p>
                <p>Hệ Thống Đăng Ký Học Phần</p>
            </div><!-- end #banner -->       
            
            
             <div id="contact">
            <ul>
                <li>
                
                <p><b>Email:</b>  online_citd@gmail.com</p>
                </li>
                <li>
                
                <p><b>Fax:</b>  08 8818818</p>
                </li>
                <li>
                
                <p><b>Phone:</b> 84 01699938919</p>
                </li>
                
               
            </ul>
            </div>
        
        </div><!-- end #header -->
    <div id="page">
    
    <!--div #main_menu -->    
    <?php include_once("vmenu2.php"); ?>
    <!--END div #main_menu --> 
       
    <div id="data">
            
            <div id="tool">
                <p id="data_title">&#187;&#187;Các lớp dạy hôm nay</p>
                <img id="mail" title="Gửi thời khóa biểu giảng dạy cho giáo viên" src="<?php echo static_url(); ?>/images/mail.png" alt="mail" />
                <img id="export" title="Xuất thời khóa biểu thành file(Excel,PDF)" src="<?php echo static_url(); ?>/images/export.png" alt="export" />
                <img id="print" title="In thời khóa biểu này(PDF)" src="<?php echo static_url(); ?>/images/print.png" alt="print" />
            </div><!--end #tool -->
            
            <div id="content">
			
                <div id="contenttable" >
                    <?php 							
						$i = 0;
						foreach ($lops as $malop=>$lop)
						{
							echo "<div id='lop$i' kieu='divlop' malop='$malop'>";
							echo "<p class='time'>"; echo $lop["BD"]; echo " - "; echo "<span class='tgkt'>".$lop["KT"]."</span>"; echo "</p>";
							echo "<p class='malop'>"; echo $lop["malop"]; echo "</p>";
							echo "<p class='tenmh'>"; echo $lop["tenmh"]; echo "</p>";
							echo "</div>";
							$i++;
						}	
						
					?>
					<form id="lop" method="POST" action="<?php echo base_url(); ?>room/gv" >
						<input type='hidden' name="ketthuc" id="ketthuc" />
						<input type='hidden' name="malop" id="malop" />
					</form>
				</div>
            </div><!--end #content -->
            
    </div><!--end #data -->
    </div><!--#page -->
</div><!--end #wrapper -->

</body>
</html>