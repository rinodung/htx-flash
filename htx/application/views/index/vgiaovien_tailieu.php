<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="rinodung" />
     
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/giaovien_tailieu.css" />
    <link href="<?php echo static_url();?>/css/menu/menu2.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/popup/popup_css.css" />
    <link href="<?php echo static_url();?>/css/index/lop.css" rel="stylesheet" type="text/css"/>    
    
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.form.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/index/last_active.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/index/giaovien_tailieu.js"></script>
    <script type="text/javascript">
	
		var base_url = "<?php echo base_url(); ?>";
		var user = "<?php echo $magv; ?>";
        var totalSize = 0;
        var formdata = new FormData();
        
	$(document).ready(function()
        {   
            
            $("#fileuploads input:file").live("change", function(e)
            {
                var fileName = $(this).val();
                var e = isOK(fileName);
                if(!e)
                {
                    alert("File không hợp lệ.");
                    $(this).replaceWith($(this).clone());
                }
            });
            
            $("div.addfile").live("click", function()
            {
                $("div#fileuploads").append("<div class='input'><input type='file' name='fileupload' class='inputbox autowidth' /><div class='addfile' title='Thêm file'></div><div class='removefile' title='Bỏ file'></div></div>");
            });
            
            $("div.removefile").live("click", function()
            {
               $(this).parent("div.input").remove(); 
            });
  
            $("div.del").live("click", function()
            {
                var cf = confirm("Bạn có chắc chắn muốn xóa file này?")
                if(!cf)
                    return;
                var type = $(this).attr("type");
                var divtl = $(this).parent("div.tailieu");
                var name = divtl.attr("name");
                $.ajax(
                {
                    url: "<?php echo base_url()."giaovien/xoatailieu"; ?>",
                    data: {name:name,type:type},
                    type: "POST",
                    success:function(data)
                    {
                        alert("thành công: " + data);
                        if(data=="OK")
                        {
                            divtl.remove();
                        }
                        else if(data="error")
                        {
                            alert("Rất tiếc! Lỗi đã xảy ra trong quá trình xóa file " + name);
                        }
                    },
                    error:function(data)
                    {
                        alert(data);
                        alert("Rất tiếc! Lỗi đã xảy ra trong quá trình xóa file " + name);
                    }
                });
            });   
        });
        
        function getExtension(filename)
        {
            var parts = filename.split('.');
            return parts[parts.length - 1];
        }
        
        function isOK(filename)
        {
            var type = getExtension(filename);
            type = type.toLowerCase();
            var a = new Array("ppt","pptx","pdf","xls","xlsx","doc","docx","flv","mp3","txt","php","js","css","jpg","png");
            var t = a.indexOf(type);
            if(t>-1)
                return true;
            return false;
        }
        
        function getFileSize(size)
	{
            if(size < 1024)
                return size + " B"; 
            else if (size >= 1024 && size < 1048576) 
                return Math.ceil(size/1024) + " KB";
            else if (size >= 1048576 && size < 1073741824)
                return Math.ceil(size/1048576) + " MB";
            return Math.ceil(size/1073741824) + " MB";
	}
        
        function uploadFiles(ctrId)
        {
            totalSize = 0;
            formdata = new FormData();

            // builds a set of INPUT controls and tries to process any files
            var uploadControls = document.getElementById(ctrId).getElementsByTagName("INPUT");
            var len = uploadControls.length;
            if(len == 0)
                return;
            
            var height = document.body.clientHeight;

            for(var i = 0; i< uploadControls.length; i++)
            {
               processFiles(uploadControls[i], i);
            }
            
            
            $("#convert").css("display", "none");
            $(".uploading").fadeIn(1000,function(){});
            window.scroll(0, height);

            var ajax = new XMLHttpRequest();

            ajax.upload.addEventListener("progress", progressHandler, false);
            
            ajax.onreadystatechange = function()
            {
                if(ajax.readyState == 4)
                {location.reload();
                }
            }
            
            
            ajax.open("POST","<?php echo base_url()."giaovien/themtailieu"; ?>");
            ajax.send(formdata);        
        }
        
        function processFiles(ctrl, i) 
        {
            // if the control has files
            if(ctrl.files)
            {
                for(var j =0; j < ctrl.files.length; i++, j++) 
                {
                    var file = ctrl.files[j];
                    totalSize += file.size;
                    formdata.append("fileupload" + i, file);
                }
            }
        }
        
        function progressHandler(event)
        {
            //document.getElementById("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+totalSize;
            var percent = (event.loaded / totalSize) * 100;
            if(percent >= 100)
            {
                $("#convert").css("display", "block");
                $("#percent").html("converting");
                return;
            }
            percent = (Math.round(percent)).toString() + "%";
            //_("progressBar").value = Math.round(percent); 
            //_("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
            
            $("#bar").css("width", percent);
            $("#percent").html(percent);
        }
        
        
    </script>
    
    
    <title>Quản lý tài liệu</title>
</head>

<body>
<div id="wrapper">
      <div id="header">
        
            
            <img src="<?php echo static_url();?>/images/index/logo.png" />
            <div id="banner">
                <p>Đại Học Quốc Gia Thành Phố Hồ Chí Minh</p>
                <p>Trường Đại Học Công Nghệ Thông Tin</p>
                <p>Hệ Thống Đăng Ký Học Phần</p>
            </div><!-- end #banner -->       
            
            
             <div id="contact">
            <ul>
                <li>
                
                <p><b>Email:</b>  online_citd@gmail.com</p>
                </li>
                <li>
                
                <p><b>Fax:</b>  08 8818818</p>
                </li>
                <li>
                
                <p><b>Phone:</b> 84 01699938919</p>
                </li>
                
               
            </ul>
            </div>
        
        </div><!-- end #header -->
        
        
    <div id="page">
    
    <!--div #main_menu -->    
    <?php include_once("vmenu2.php"); ?>
    <!--END div #main_menu --> 
       
    <div id="data">
            
            <div id="tool">
                <p id="data_title">&#187;&#187; Các tài liệu đã upload:</p>
                <img id="mail" title="Gửi thời khóa biểu giảng dạy cho giáo viên" src="<?php echo static_url(); ?>/images/mail.png" alt="mail" />
                <img id="export" title="Xuất thời khóa biểu thành file(Excel,PDF)" src="<?php echo static_url(); ?>/images/export.png" alt="export" />
                <img id="print" title="In thời khóa biểu này(PDF)" src="<?php echo static_url(); ?>/images/print.png" alt="print" />
            </div><!--end #tool -->
            
            <div id="content">
                <ul id="tabs">
                    <li><a href="#" title="tab1">Slide</a></li>
                    <li><a href="#" title="tab2">Hình ảnh</a></li>
                    <li><a href="#" title="tab3">Media</a></li>
                </ul>

                <div id="tabcontent"> 
                    <div id="tab1" class="tab">
                        <?php
                            if(count($tailieu)>0)
                            {
                                $i = 1;
                                echo "<ol id='tailieus'>";
                                foreach ($tailieu as $var) 
                                {

                            ?>

                                <div class="tailieu" name="<?php echo $var['name']; ?>">
                                    <div class="stt"><li></li></div>
                                    <div class="tentailieu"><?php echo $var['name']; ?></div>
                                    <div class="dungluong"><?php echo $var['size']; ?></div>
                                    <div class="ngayupload"><?php echo $var['date']; ?></div>
                                    <div class="del" type="tailieu"></div>
                                </div>

                            <?php
                                }
                                echo "</ol>";
                            }
                            else 
                                echo "<span id='empty'>Chưa có tài liệu nào.</span>";
                            ?>
                    </div>
                    <div id="tab2" class="tab">
                        <?php
                            if(count($images)>0)
                            {
                                $i = 1;
                                echo "<ol id='images'>";
                                foreach ($images as $var) 
                                {

                            ?>

                                <div class="tailieu" name="<?php echo $var['name']; ?>">
                                    <div class="stt"><li></li></div>
                                    <div class="tentailieu"><?php echo $var['name']; ?></div>
                                    <div class="dungluong"><?php echo $var['size']; ?></div>
                                    <div class="ngayupload"><?php echo $var['date']; ?></div>
                                    <div class="del" type="images"></div>
                                </div>

                            <?php
                                }
                                echo "</ol>";
                            }
                            else 
                                echo "<span id='empty'>Chưa có tài liệu nào.</span>";
                            ?>
                    </div>
                    <div id="tab3" class="tab">
                        <?php
                            if(count($media)>0)
                            {
                                $i = 1;
                                echo "<ol id='media'>";
                                foreach ($media as $var) 
                                {

                            ?>

                                <div class="tailieu" name="<?php echo $var['name']; ?>">
                                    <div class="stt"><li></li></div>
                                    <div class="tentailieu"><?php echo $var['name']; ?></div>
                                    <div class="dungluong"><?php echo $var['size']; ?></div>
                                    <div class="ngayupload"><?php echo $var['date']; ?></div>
                                    <div class="del" type="media"></div>
                                </div>

                            <?php
                                }
                                echo "</ol>";
                            }
                            else 
                                echo "<span id='empty'>Chưa có tài liệu nào.</span>";
                            ?>
                    </div>
                </div>
                
                
                <div id="form" >
                    <div id="fileuploads">
                        <div class="input" name="inputfirst">
                            <input type="file" name="fileupload" id="fileupload0" value="" 
                                   class="inputbox autowidth" />
                            <div class="addfile" title="Thêm file"></div>
                        </div>
                    </div>
                    <div id="submit-upload" onclick="uploadFiles('fileuploads'); "></div>
                </div>
                                    
                <div class="uploading">
                        <div class="progress">
                            <div id="bar"></div>
                            <div id="convert"></div>
                        </div>
                        <div id="percent">0%</div>
                </div>
                
                <div id="res"></div>
                
            </div><!--end #content -->
            
    </div><!--end #data -->
    </div><!--#page -->
</div><!--end #wrapper -->

</body>
</html>