
<!DOCTYPE HTML>
<head>
	<meta name="author" content="danhkhh" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Trang Đăng Ký Học Phần</title>
	<link href="<?php echo static_url();?>/css/index/dkhp.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo static_url();?>/css/menu/menu2.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo static_url();?>/css/index/lop.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="<?php echo static_url();?>/js/jquery.min.js"></script>    
    <script type="text/javascript" src="<?php echo static_url();?>/js/jquery-scroller.js"></script>
	<script type="text/javascript" src="<?php echo static_url();?>/js/index/vindex.js"></script>    
	<script type="text/javascript" src="<?php echo static_url();?>/js/index/dkhp.js"></script>
    <!-- <script type="text/javascript" src="<?php echo static_url();?>/js/index/last_active.js"></script>    -->
	
	<script type="text/javascript">	
            var base_url = "<?php echo base_url(); ?>";
			var user = "<?php echo $MSSV; ?>";
			var lops = new Array();
			<?php
			$i = 0;
			foreach($lops as $lop)
			{
				$bd = explode('h', $lop['BD']);
				$kt = explode('h', $lop['KT']);
				echo "var temp = new Array();";
				echo "temp[0] = ".$bd[0].";";
				echo "temp[1] = ".$bd[1].";";
				echo "temp[2] = ".$kt[0].";";
				echo "temp[3] = ".$kt[1].";";
				echo "lops[".$i."] = temp;";
				$i++;
			}
			?>
			
		function kt()
		{
			var n = lops.length;
			for(var i=0; i<n; i++)
			{
				var BD = new Date();
				var KT = new Date();
				BD.setHours(parseInt(lops[i][0]));
				BD.setMinutes(parseInt(lops[i][1]));
				KT.setHours(parseInt(lops[i][2]));
				KT.setMinutes(parseInt(lops[i][3]));
				BD.setTime(BD.getTime() - 15*60000);
				KT.setTime(KT.getTime() + 15*60000);
                                $.ajax(
                                {
                                    type:"POST",
                                    url: base_url + "online/getTime",
                                    data:{i:i, bd:BD.getTime(), kt:KT.getTime()},
                                    timeout: 5000,
                                    success:function(res)
                                    {
                                        var data = $.parseJSON(res);
                                        var cur = new Date(data.sec * 1000);
                                        var bd1 = new Date(data.bd *1);
                                        var kt1 = new Date(data.kt *1);
                                        var index = data.index;
                                        if(cur.getTime() >= bd1.getTime() && cur.getTime() < kt1.getTime())
                                        {
                                                $("#lop"+index).attr("class", "roomopen");
                                        }
                                        else
                                        {
                                                $("#lop"+index).attr("class", "roomclose");
                                        }
                                    }
                                });
			}
		}
		
		$(document).ready(function()
		{			
			$("div.roomopen").live("click", function()
			{
				var malop = $(this).attr("malop");
				var tgkt = $(this).find("span.tgkt").html();
				$("#ketthuc").val(tgkt);
				$("#malop").val(malop);
				$("form#lop").submit();
			});
			kt();
			setInterval(kt, 10000);
		});
	</script>
</head>

<body>
	
	<div id="popup">
	</div><!-- end #popup -->
	<div id = "divchangepass">
		<div id="topchangepass" >Đổi mật khẩu</div>
		<button id="closechangepass" ></button>
		<div class="nottop">Mật khẩu cũ:<br> <input type="password" name="oldPassword" id="oldPassword" /><br></div>
		<div class="nottop">Mật khẩu mới:<br> <input type="password" name="password1" id="password1" /><br></div>
		<div class="nottop">Xác nhận mật khẩu mới:<br> <input type="password" name="password2" id="password2" /><br></div>
		<span id="errorOldPassword"></span>
		<span id="errorNewPassword"></span>
		<button id="btn_changepass" ></button>
		
	</div>

    <div id="wrapper">
        
       
        
        <div id="header">
        
            
            <img src="<?php echo static_url();?>/images/index/logo.png" />
            <div id="banner">
                <p>Đại Học Quốc Gia Thành Phố Hồ Chí Minh</p>
                <p>Trường Đại Học Công Nghệ Thông Tin</p>
                <p>Hệ Thống Đăng Ký Học Phần</p>
            </div><!-- end #banner -->            
             <div id="contact">
            <ul>
                <li>
                
                <p><b>Email:</b>  online_citd@gmail.com</p>
                </li>
                <li>
                
                <p><b>Fax:</b>  08 8818818</p>
                </li>
                <li>
                
                <p><b>Phone:</b> 84 01699938919</p>
                </li>
                
               
            </ul>
            </div>
        </div><!-- end #header -->
        
        <?php include_once("vmenu2.php");  ?>
        
           
        <div id="primary">
            <div id="left">
            <div id="content" class = "box">
                <div id="contentheader" class="box_header">
                    <?php if($learn)
                    {
                    ?>
                        <h3>Các lớp học hôm nay</h3>
                    <?php
                    }
                    else
                    {
                    ?>
                        <h3>Các lớp học chưa được mở</h3>
                    <?php
                    }
                    ?>
                    
                    
                </div><!-- end #contentheader -->
                
                <div id="contenttable" class="box_data">
                    <?php 							
						$i = 0;
						foreach ($lops as $malop=>$lop)
						{
							echo "<div id='lop$i' kieu='divlop' malop='$malop'>";
							echo "<p class='time'>"; echo $lop["BD"]; echo " - "; echo "<span class='tgkt'>".$lop["KT"]."</span>"; echo "</p>";
							echo "<p class='malop'>"; echo $lop["malop"]; echo "</p>";
							echo "<p class='tenmh'>"; echo $lop["tenmh"]; echo "</p>";
							echo "<p class='tengv'>"; echo $lop["tengv"]; echo "</p>";
							echo "</div>";
							$i++;
						}	
						
					?>
					<form id="lop" method="POST" action="<?php echo base_url(); ?>room/sv" >
						<input type='hidden' name="ketthuc" id="ketthuc" />
						<input type='hidden' name="malop" id="malop" />
					</form>
                </div><!-- end #contenttable -->
				
				<div id="contenttable1">
				</div><!-- end #contenttable1 -->
				
				
            </div><!-- end #content -->
            </div><!-- end #left -->
			
			<div id="right">
            <div id="taikhoan" class="box">
                <div class="box_header">
                    <h3>Thông tin cá nhân</h3>
                </div>
                <div id="taikhoancontent" class="box_data">
                    <p>MSSV: <strong><span id = "MSSV" style="color: red;"><?php echo $MSSV; ?></span></strong></p>
                    <p>Họ tên: <strong><?php echo $TenSV; ?></strong></p>
                    <p>Khoa: <strong><span id = "khoa" class = <?php echo $khoa; ?>><?php	
											switch($khoa)
											{
												case "mmt": 
													echo "Mạng máy tính và truyền thông";
													break;
												case "khmt": 
													echo "Khoa học máy tính";
													break;
												case "ktmt": 
													echo "Kỹ thuật máy tính";
													break;
												case "httt": 
													echo "Hệ thống thông tin";
													break;
												case "cnpm": 
													echo "Công nghệ phần mềm";
													break;
											}
										?></span></strong></p>
                    <p>Khóa: <strong><?php echo $K; ?></strong></p>
					<span id="MaCN" style="display: none;"><? echo $MaCN; ?></span>
                    
                    <p id="action"><a href="/logout" >Thoát</a> <a href="#" id="showchangepass" >Đổi mật khẩu</a></p>
                    
                </div>
            </div>
            
             <div id="TKB" class="box">
                <div class="box_header">
                    <h3>Thời khóa biểu</h3>
                </div><!-- end #content -->
                
                <div id="TKBcontent" class="box_data">
                    <table id="TKBtable" cellspacing="0">
                        <tr>
                            <th colspan="2"></th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                        </tr>
                        <tr>
                            <td rowspan="2">Sáng</td>
                            <td>ca 1</td>
							<?php
								for($thu = 2; $thu <= 7; $thu++)
								{
									$tempTKB = explode("|",$TKB[$thu - 2]);
									if($tempTKB != "")
									{
										echo "<td class='lich' id='1".$thu."' title='".$tempTKB[1]."' >".$tempTKB[0]."</td>";
									}
									else
									{
										echo "<td class='lich' id='1".$thu."' ></td>";
									}
								}
							?>
                        </tr>
                        <tr>
                            <td style="border-left: 1px solid #DCDDDE;">ca 2</td>
							<?php
								for($thu = 2; $thu <= 7; $thu++)
								{
									$tempTKB = explode("|",$TKB[$thu + 4]);
									if($tempTKB != "")
									{
										echo "<td class='lich' id='2".$thu."' title='".$tempTKB[1]."' >".$tempTKB[0]."</td>";
									}
									else
									{
										echo "<td class='lich' id='2".$thu."' ></td>";
									}
								}
							?>
                        </tr>
                        <tr>
                            <td rowspan="2">Chiều</td>
                            <td>ca 3</td>
							<?php
								for($thu = 2; $thu <= 7; $thu++)
								{
									$tempTKB = explode("|",$TKB[$thu + 10]);
									if($tempTKB != "")
									{
										echo "<td class='lich' id='3".$thu."' title='".$tempTKB[1]."' >".$tempTKB[0]."</td>";
									}
									else
									{
										echo "<td class='lich' id='3".$thu."' ></td>";
									}
								}
							?>
                        </tr>
                        <tr>
                            <td style="border-left: 1px solid #DCDDDE">ca 4</td>
							<?php
								for($thu = 2; $thu <= 7; $thu++)
								{
									$tempTKB = explode("|",$TKB[$thu + 16]);
									if($tempTKB != "")
									{
										echo "<td class='lich' id='4".$thu."' title='".$tempTKB[1]."' >".$tempTKB[0]."</td>";
									}
									else
									{
										echo "<td class='lich' id='4".$thu."' ></td>";
									}
								}
							?>
                        </tr>
                    </table>
                    <p id="tkb"><a id="showTKB" href="<?php echo base_url()."thoi-khoa-bieu"; ?>">Chi tiết ...</a></p>
                </div><!-- end #TKBcontent -->
            </div><!-- end #TKB -->
                
           
            
            <div id="nhacnho" class="box">
                    <div class="box_header">
                        <h3>Lưu ý</h3>
                    </div><!-- end #mondakdheader -->
                    <div class="box_data">
						<ul class='scrollingtext'>
                        <li>- Sinh viên tự chịu trách nhiệm về những môn mình đã đăng ký.</li>
                        <li>- Số tín chỉ tối đa có thể đăng ký là:<? echo $TCTD."."; ?> </li>                        
                        <li>- Đối với các môn chuyên ngành, sinh viên cần chọn đúng các môn theo khoa chỉ định</li>
                        <li>- Đối với các lớp <b> đề nghị mở</b>, mặc định sinh viên sẽ có tên trong danh sách lớpnếu phòng đào tạo mở lớp này.</li>
                        <li>- Thời gian đăng ký từ ngày <br /> 25/12/2012 --&gt; 02/02/2013 .</li>
                        <li>- Sau khi đăng ký xong sinh viên in phiếu, ký tên và nộp lại cho phòng đào tạo.</li>
                        <li>- Sinh viên nên cập nhật thông báo thường xuyên từ phòng đào tạo.</li>
                        <li>- Sinh viên xem hướng dẫn đăng ký học phần <a href="#">tại đây.</a> </li>
                        <li>- Sinh viên nên đăng ký ít nhất là 14 tín chỉ để có thể xét học bổng.</li>
                        </ul>
						
                    </div><!-- end #lietkedk -->
					
            </div><!-- end #mondadk -->
                
			</div><!-- end #right -->
				            
        </div><!-- end #primary -->
        
		
        <div id="footer">
            <div id="diachi">
                <p>Trường Đại Học Công Nghệ Thông Tin</p>
                <p>Đại Học Quốc Gia Thành Phố Hồ Chí Minh</p>
                <p>Khu phố 6, Phường Linh Trung Thủ Đức</p>
                <p>Mail: Admin.uit@gmail.com</p>
                <p>Fax: 016888898</p>
                <p>Phone: 01699938919</p>
            </div><!-- end #diachi -->
            <div id="link">
                <img src="<?php echo static_url();?>/images/index/uit.png"  alt="uit" />
                <img src="<?php echo static_url();?>/images/index/facebook.png" alt="facebook" />
                <img src="<?php echo static_url();?>/images/index/zing.png" title="zing"  />
            </div><!-- end #link -->
        </div><!-- end #footer -->
        
    </div><!-- end #wrapper -->

</body>
</html>