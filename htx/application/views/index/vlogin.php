<?php 
	$name = $this->session->userdata('name');
	if($name != false)
	{
		//echo $name;
		header('Location: '.base_url());
	}
?>
<!DOCTYPE HTML>
<head>
    <meta name="author" content="danhkhh" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Trang chủ</title>
    <link rel="stylesheet" type="text/css" href="<?php echo static_url();?>/css/index/login.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo static_url();?>/css/index/camera.css"/>
    
    <script type="text/javascript" src="<?php echo static_url();?>/js/jquery.min.js"></script>
    
    <script type="text/javascript" src="<?php echo static_url();?>/js/jquery-scroller.js"></script>
    <script type="text/javascript" src="<?php echo static_url();?>/js/camera.min.js"></script>
          
    <script type="text/javascript" src="<?php echo static_url();?>/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?php echo static_url();?>/js/index/vlogin.js"></script>	
	<script type="text/javascript">
		$(document).ready(function()
		{
			<?php
				if(isset($formeload))
				{
					echo '$("#popup").show(); $("#formlogin").show();';
				}
				else
				{
					echo '$("#popup").hide(); $("#formlogin").hide();';
				}
			?>
		});
	</script>
</head>

<body>
	<div id="popup">      
    </div>      
    <div id="formlogin">
        <img id="close" title="Đóng" src="<?php echo static_url(); ?>/images/close.png" />
		<form method="post" action="<?php echo base_url(); ?>index/login">
        <table>
        <tr><td>Mã số tài khoản</td></tr>
        <tr><td><input type="text" name="username" value="" id="username" /></td></tr>
        <tr><td>Mật khẩu</td></tr>
        <tr><td><input type="password" name="password"  id="password" /></td></tr>
        <tr><td>Mã xác nhận &nbsp;&nbsp;&nbsp;&nbsp;<?php echo $cap['image'];?> <input type="hidden" name="captchavalue" value="<?php echo $cap['word'] ?>" /></td>	</tr>
		<tr><td><input type="text" name="captcha" id="captcha" value="<?PHP echo $cap['word']; ?>" /></td></tr>
        <tr><td class="error"><?php 
					if(isset($accounterror)) 
						echo $accounterror;
					if(isset($captchaerror)) 
						echo "Mã xác nhận sai!";
				?>
			</td></tr>					
		<tr><td><input type="submit" name="submit" value="Đăng nhập" id="dangnhap" /></td></tr>	
        </table>
        </form>
    </div>

    <div id="wrapper">
        
        <div id="top">            
           <h4 class="scrollingtext">HỆ THỐNG GIẢNG DẠY TRỰC TUYẾN ĐA TƯƠNG TÁC UIT-ONLINE</h4>
            
        </div><!-- end #top -->
        
        <div id="header">                    
            <a href=""><img src="<?php echo static_url();?>/images/index/logo.png" /></a>
            <div id="banner">
                <p>ĐẠI HỌC QUỐC GIA TP.HỒ CHÍ MINH</p>
                <p>TRƯỜNG ĐẠI HỌC CÔNG NGHỆ THÔNG TIN</p>
                <h1>UIT-ONLINE</h1>
            </div><!-- end #banner -->
            
             <div id="contact">
            <ul>
                <li>
                
                <p><b>Email:</b>  online_citd@gmail.com</p>
                </li>
                <li>
                
                <p><b>Fax:</b>  08 8818818</p>
                </li>
                <li>
                
                <p><b>Phone:</b> 84 01699938919</p>
                </li>
                
               
            </ul>
            </div>
            <div id="info">
            <ul>
                <li>
                
                <a id="home" href="#gioithieu">Giới thiệu</a>
                </li>
                <li>
                
               
                
                <a id="policy" href="#quyche">Quy chế chung</a>
                </li>
                <li>
                
                 <a id="tutorial"  href="#huongdan">Hướng dẫn sử dụng</a>
                </li>
                <li>
                <a  id="login" href="">Đăng nhập</a>
                </li>
            </ul>
            </div>
        </div><!-- end #header -->
        
         <div class="separator" id="separator1">
         <img src="<?php echo static_url(); ?>/images/index/separator1.png" />
         </div>  
         
        <div id="slider">
        <!------------------------------------- THE CONTENT ------------------------------------------------->
        
		<div class="camera_wrap black_skin" id="camera_wrap">
        
            <div  data-src="<?php echo static_url(); ?>/images/index/slider/slider_4.jpg" data-thumb="<?php echo static_url(); ?>/images/index/slider/thumbs/thumbl_980x340_002.png"></div>
            <div data-src="<?php echo static_url(); ?>/images/index/slider/slider_12.jpg" data-thumb="<?php echo static_url(); ?>/images/index/slider/thumbs/thumbl_980x340_006.png"></div>           
            
            
            <div  data-src="<?php echo static_url(); ?>/images/index/slider/slider_14.jpg" data-thumb="<?php echo static_url(); ?>/images/index/slider/thumbs/thumbl_980x340_002.png"></div>
                
            
            
                
            
             <div data-src="<?php echo static_url(); ?>/images/index/slider/slider_8.jpg" data-thumb="<?php echo static_url(); ?>/images/index/slider/thumbs/thumbl_980x340_004.png">
                
            </div>
           
            <div  data-src="<?php echo static_url(); ?>/images/index/slider/slider_10.jpg" data-thumb="<?php echo static_url(); ?>/images/index/slider/thumbs/thumbl_980x340_002.png"></div>
                
            
            
             <div data-src="<?php echo static_url(); ?>/images/index/slider/slider_11.jpg" data-thumb="<?php echo static_url(); ?>/images/index/slider/thumbs/thumbl_980x340_004.png">
                
            </div>
            
        </div><!-- #camera_wrap_1 -->
      
    </div><!--end #slider -->
    <div class="clear" style="clear: both;"></div>
    <div class="separator" id="separator2">
         <img src="<?php echo static_url(); ?>/images/index/separator2.png" />
        </div>  
      
        <div class="clear" style="clear: both;"></div>
        <div id="primary">
                        
            
                <!--div #main_menu -->    
             
            <!--END div #main_menu --> 
            
            <div id="gioithieu">
            <!-- 
                <div id="linklogin">
                <p>Đăng nhập</p>
                </div>
                -->                
                
                <div id="quyche1" class='box'>
                    <h4>Giới thiệu hệ thống giảng dạy trực tuyến đa tương tác UIT-ONLINE</h4>
                    <div id="box_data">
                    <p>Hệ thống giảng dạy trực tuyến đa tương tác UIT-ONLINE là một giải pháp được đưa ra để đáp ứng nhu cầu học trực tuyến, hệ đào tạo từ xa ngày càng trở nên phổ biến
                     trong hệ thống giáo dục ngày này. Hệ thống này có thể đáp ứng được tất cả các môn học ở phổ thông như: anh văn, toán, lý, hóa cũng như những môn 
                     chuyên nghành ở các trường đại học cao đẳng như: Tin học đại cương, Toán cao cấp, Mạng máy tính cơ bản... </p>
                     
                     <img src="<?php echo static_url(); ?>/images/index/giaovien_demo.jpg" />
                     
                     <p>Hệ thống đăng ký học phần đơn giản và hiệu quả giúp cho học viên dễ dàng đăng ký môn học trước khi tham gia vào khóa học của mình.
                     Giao diện đăng ký học phần hệ thống giảng dạy trực tuyến đa tương tác UIT-ONLINE
                     </p>
                     <img src="<?php echo static_url(); ?>/images/index/dkhp_demo.png" />
                     
                     <p>Giao diện đăng ký học phần hệ thống giảng dạy trực tuyến đa tương tác UIT-ONLINE</p>                     
                     <img src="<?php echo static_url(); ?>/images/index/sinhvien_demo.jpg" />
                     
                     <p>Hệ thống đăng ký học phần đơn giản và hiệu quả giúp cho học viên dễ dàng đăng ký môn học trước khi tham gia vào khóa học của mình.
                     </p>
                     <img src="<?php echo static_url(); ?>/images/index/presentation_demo.jpg" />
                     
                     <p>Giáo viên chia sẽ màn hình cho học viên quan sát và thực hành theo
                     </p>
                     <img src="<?php echo static_url(); ?>/images/index/screenshare_demo.jpg" />
                     
                    <p>Hệ thống giảng dạy trực tuyến đa tương tác UIT-ONLINE sử dụng công nghệ truyền dữ liệu theo thời gian thực giúp cho học viên có thể quan sát hình ảnh và nghe được giọng nói của
                    giáo viên đang đứng lớp, hệ thống cung cấp các chức năng giúp cho học viên có một môi trường học trực tuyến giống với môi trường học truyền thống thông thường như bảng trắng
                    phấn đen, trình chiếu bài giảng, giơ tay phát biểu, thảo luận nhóm, tin nhắn trò chuyện. 
                    </p>
                    <p>Hệ thống xây dựng trên nền tảng thuần web tạo sự dễ dàng và tiện lợi trong quá trình sử dụng của học viên. Chỉ cần một hai cái nhấp chuột trên trình
                    duyệt thân thuộc như Chrome, Firefox, IE là học viên có thể tham gia vào một buổi học trực tuyến ngay lập tức.</p>       
                    <p>Quy trình đăng ký sử dụng hệ thống như sau: </p>              
                    <ol>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    </ol>
                    </div>
                </div>
               
            </div><!--end gioi thieu -->
            
            <div id="huongdan">
                
                <div id="quyche1" class='box'>
                    <h4>Hướng dẫn sử dụng hệ thống UIT-ONLINE</h4>
                    <div id="box_data">
                    <p>Để quá trình đăng ký học phần được diễn ra nhanh chóng và hiệu quả. Phòng đào tạo đề nghị sinh viên nghiêm chỉnh chấp hành những quy định sau:</p>
                    <ol>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    </ol>
                    </div>
                </div>
                <div id="quyche2" class='box'>
                    <h4>Kế hoạch đăng ký học phần học kỳ 1 2012 - 2013</h4>
                    <div id="box_data">
                    <p>Để quá trình đăng ký học phần được diễn ra nhanh chóng và hiệu quả. Phòng đào tạo đề nghị sinh viên nghiêm chỉnh chấp hành những quy định sau:</p>
                    <ol>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    </ol>
                    </div>
                </div>
            </div><!--end gioi thieu -->
            
            <div id="quyche">
                
                <br style="clear:both" />
                <div id="quyche1" class='box'>
                    <h4>Quy chế đăng ký học phần học kỳ 1 2012 - 2013</h4>
                    <div id="box_data">
                    <p>Để quá trình đăng ký học phần được diễn ra nhanh chóng và hiệu quả. Phòng đào tạo đề nghị sinh viên nghiêm chỉnh chấp hành những quy định sau:</p>
                    <ol>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    </ol>
                    </div>
                </div>
                <div id="quyche2" class='box'>
                    <h4>Kế hoạch đăng ký học phần học kỳ 1 2012 - 2013</h4>
                    <div id="box_data">
                    <p>Để quá trình đăng ký học phần được diễn ra nhanh chóng và hiệu quả. Phòng đào tạo đề nghị sinh viên nghiêm chỉnh chấp hành những quy định sau:</p>
                    <ol>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    <li>Hệ thống đăng ký gồm 2 phần: phần môn học chung (những môn đại cương) và phần những môn chuyên ngành</li>
                    <li>Sinh viên chọn lớp để đăng ký cho môn học của mình (1 môn có thể có nhiều lớp)</li>
                    <li>Sinh viên tự sắp xếp thời khóa biểu của mình để tránh trùng giờ.</li>
                    <li>Số tín chỉ tối đa sinh viên có thể đăng ký là 26 tín chỉ và tối thiểu là 10 tín chỉ. Nếu vi phạm phòng đào tạo sẽ làm việc riêng.</li>
                    <li>Sau khi đăng ký xong sinh viên in phiếu và đóng tiền học phí tại phòng tài chính.</li>
                    </ol>
                    </div>
                </div>
            </div><!--end quy che -->
            
        </div><!-- end #primary1 -->
        
    </div><!-- end #wrapper -->
    <div id="ontop" title="Lên đầu trang">
    <p>top</p>
    </div>

</body>
</html>