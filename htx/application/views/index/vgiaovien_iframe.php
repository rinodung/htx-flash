<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="rinodung" /> 
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/giaovien_iframe.css" />
    
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.form.js"></script>
    <script type="text/javascript">
	
		var base_url = "<?php echo base_url(); ?>";
		var user = "<?php echo $magv; ?>";
        var totalSize = 0;
        var formdata = new FormData();
        
	$(document).ready(function()
        {   
            
            
            
            var obj = $("#form");
            obj.live('dragenter', function (e) 
            {
                    e.stopPropagation();
                    e.preventDefault();
                    $(this).css('border', '1px solid #ccc');
            });
            obj.live('dragover', function (e) 
            {
                     e.stopPropagation();
                     e.preventDefault();
            });
            obj.live('drop', function (e) 
            {

                     $(this).css('border', 'none');
                     e.preventDefault();
                     var files = e.originalEvent.dataTransfer.files;

                    
                    var x = $("div#fileuploads div.input:last-child input");
                    if(x[0].files.length > 0)
                    {
                        $("div#fileuploads").append("<div class='input'><input type='file' name='fileupload' class='inputbox autowidth' /><div class='addfile' title='Thêm file'></div><div class='removefile' title='Bỏ file'></div></div>");
                        var x = $("div#fileuploads div.input:last-child input");
                    }
                    x[0].files = files;
            });
            
            
            
            
            $("#fileuploads input:file").live("change", function(e)
            {
                var fileName = $(this).val();
                var e = isOK(fileName);
                if(!e)
                {
                    alert("File không hợp lệ.");
                    $(this).replaceWith($(this).clone());
                }
            });
            
            $("div.addfile").live("click", function()
            {
                $("div#fileuploads").append("<div class='input'><input type='file' name='fileupload' class='inputbox autowidth' /><div class='addfile' title='Thêm file'></div><div class='removefile' title='Bỏ file'></div></div>");
            });
            
            $("div.removefile").live("click", function()
            {
               $(this).parent("div.input").remove(); 
            });
  
            $("div.del").live("click", function()
            {
                var cf = confirm("Bạn có chắc chắn muốn xóa file này?")
                if(!cf)
                    return;
                var divtl = $(this).parent("div.tailieu");
                var name = divtl.attr("name");
                $.ajax(
                {
                    url: "<?php echo base_url()."giaovien/xoatailieu"; ?>",
                    data: {name:name},
                    type: "POST",
                    success:function(data)
                    {
                        alert("thành công: " + data);
                        if(data=="OK")
                        {
                            divtl.remove();
                        }
                        else if(data="error")
                        {
                            alert("Rất tiếc! Lỗi đã xảy ra trong quá trình xóa file " + name);
                        }
                    },
                    error:function(data)
                    {
                        alert(data);
                        alert("Rất tiếc! Lỗi đã xảy ra trong quá trình xóa file " + name);
                    }
                });
            });   
        });
        
        function getExtension(filename)
        {
            var parts = filename.split('.');
            return parts[parts.length - 1];
        }
        
        function isOK(filename)
        {
            var type = getExtension(filename);
            type = type.toLowerCase();
            var a = new Array("ppt","pptx","pdf","xls","xlsx","doc","docx","flv","mp3","txt","php","js","css","jpg","png");
            var t = a.indexOf(type);
            if(t>-1)
                return true;
            return false;
        }
        
        function getFileSize(size)
	{
            if(size < 1024)
                return size + " B"; 
            else if (size >= 1024 && size < 1048576) 
                return Math.ceil(size/1024) + " KB";
            else if (size >= 1048576 && size < 1073741824)
                return Math.ceil(size/1048576) + " MB";
            return Math.ceil(size/1073741824) + " MB";
	}
        
        function uploadFiles(ctrId)
        {
            totalSize = 0;
            formdata = new FormData();
			formdata.append("name", user);

            // builds a set of INPUT controls and tries to process any files
            var uploadControls = document.getElementById(ctrId).getElementsByTagName("INPUT");
            var len = uploadControls.length;
            if(len == 0)
                return;
            
            var height = document.body.clientHeight;

            for(var i = 0; i< uploadControls.length; i++)
            {
               processFiles(uploadControls[i], i);
            }
            
            
            $("#convert").css("display", "none");
            $(".uploading").fadeIn(1000,function(){});
            window.scroll(0, height);

            var ajax = new XMLHttpRequest();

            ajax.upload.addEventListener("progress", progressHandler, false);
            
            ajax.onreadystatechange = function()
            {
                if(ajax.readyState == 4)
                {
                    $("#ok").fadeIn(2700).fadeOut(500);
                    var d=new Date();
                    var today = d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
                        for(var i = 0; i< uploadControls.length; i++)
                        {
                            if(!uploadControls[i].files)
                                continue;
                            $("#tailieus").append("<div class='tailieu' name='"+ uploadControls[i].files[0].name +"'><div class='stt'><li></li></div><div class='tentailieu'>"+ uploadControls[i].files[0].name +"</div><div class='dungluong'>"+ getFileSize(uploadControls[i].files[0].size) +"</div><div class='ngayupload'>"+ today +"</div><div class='del'></div></div>");
                            //$("#tailieus").append(getFileSize(uploadControls[i].files[0].size));
                        }
                        $("#res").html(ajax.responseText);
                        $(".uploading").fadeOut(2000,function()
                        {
                            var inpFirst = $("#fileupload0");
                            $("#fileupload0").replaceWith(inpFirst.clone());
                        });
                        $("#"+ctrId).children("div.input[name!=inputfirst]").remove();
                }
            }
            
            
            ajax.open("POST","<?php echo base_url()."giaovien/themtailieu1"; ?>");
            ajax.send(formdata);        
        }
        
        function processFiles(ctrl, i) 
        {
            // if the control has files
            if(ctrl.files)
            {
                for(var j =0; j < ctrl.files.length; i++, j++) 
                {
                    var file = ctrl.files[j];
                    totalSize += file.size;
                    formdata.append("fileupload" + i, file);
                }
            }
        }
        
        function progressHandler(event)
        {
            //document.getElementById("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+totalSize;
            var percent = (event.loaded / totalSize) * 100;
            if(percent >= 100)
            {
                $("#convert").css("display", "block");
                $("#percent").html("converting");
                return;
            }
            percent = (Math.round(percent)).toString() + "%";
            //_("progressBar").value = Math.round(percent); 
            //_("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
            
            $("#bar").css("width", percent);
            $("#percent").html(percent);
        }
        
        
    </script>
    
    
    <title>Quản lý tài liệu</title>
</head>

<body>
<div id="wrapper">
            
            <div id="tool" style="display: none;">
                <p id="data_title">&#187;&#187; Các tài liệu đã upload:</p>
            </div><!--end #tool -->
            
            <div id="content" style="display: none;">
                <?php
                if(count($tailieu)>0)
                {
                    $i = 1;
                    echo "<ol id='tailieus'>";
                    foreach ($tailieu as $var) 
                    {
                        
                ?>
                    
                    <div class="tailieu" name="<?php echo $var['name']; ?>">
                        <div class="stt"><li></li></div>
                        <div class="tentailieu"><?php echo $var['name']; ?></div>
                        <div class="dungluong"><?php echo $var['size']; ?></div>
                        <div class="ngayupload"><?php echo $var['date']; ?></div>
                        <div class="del"></div>
                    </div>
                    
                <?php
                    }
                    echo "</ol>";
                }
                else 
                    echo "<span id='empty'>Chưa có tài liệu nào.</span>";
                ?>
                
            </div><!--end #content -->
                
                <div id="form" >
                    <div id="fileuploads">
                        <div class="input" name="inputfirst">
                            <input type="file" name="fileupload" id="fileupload0" value="" 
                                   class="inputbox autowidth" />
                            <div class="addfile" title="Thêm file"></div>
                        </div>
                    </div>
                    <div id="submit-upload" onclick="uploadFiles('fileuploads'); "></div>
                </div>
                                    
                <div class="uploading">
                        <div class="progress">
                            <div id="bar"></div>
                            <div id="convert"></div>
                        </div>
                        <div id="percent">0%</div>
                </div>
                
                <div id="res" style="display: none;"></div>
                <div id="ok">Upload file thành công</div>
                <div id="er">Upload file thất bại</div>
            
</div><!--end #wrapper -->

</body>
</html>