<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="rinodung" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/giaovien_tkb.css" />
    <link href="<?php echo static_url();?>/css/menu/menu2.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/popup/popup_css.css" />
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jpopup.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/admin/jgiaovien_tkb.js"></script>        
	<title><?php echo $title ?></title>
</head>

<body>
<div id="wrapper">
    <div id="header">
        
            
            <img src="<?php echo static_url();?>/images/index/logo.png" />
            <div id="banner">
                <p>Đại Học Quốc Gia Thành Phố Hồ Chí Minh</p>
                <p>Trường Đại Học Công Nghệ Thông Tin</p>
                <p>Hệ Thống Đăng Ký Học Phần</p>
            </div><!-- end #banner -->       
            
            
             <div id="contact">
            <ul>
                <li>
                
                <p><b>Email:</b>  online_citd@gmail.com</p>
                </li>
                <li>
                
                <p><b>Fax:</b>  08 8818818</p>
                </li>
                <li>
                
                <p><b>Phone:</b> 84 01699938919</p>
                </li>
                
               
            </ul>
            </div>
        
        </div><!-- end #header -->
        
        
    <div id="page">
    
    <!--div #main_menu -->    
    <?php include_once("vmenu2.php"); ?>
       
    <div id="data">
        <div id="left">
            
            
            <div id="content">
                <?php 
                echo "<table>";                    
                    echo "<tr id='first'>";
                    echo "<th id='ca'>Ca</th>";
                    foreach($thu_result as $thu_row)
                    {
                        echo "<th>Thứ ".$thu_row->TenThu."</th>";
                    } 
                    echo "</tr>";
                    //Trong tung hang ca
                    foreach($ca_result as $ca_row)
                    {                      
                        echo "<tr>";
                        echo "<td class='ca' title='Ca $ca_row->TenCa'>$ca_row->TenCa</td>";
                        //trong tung cot thu
                        foreach($thu_result as $thu_row)
                        {
                            $id=$ca_row->TenCa.$thu_row->TenThu;
                            $lop_result=$this->mgiaovien->get_tkb($magv,$thu_row->TenThu,$ca_row->TenCa);
                            
                            echo "<td class='info'>";                           
                            foreach($lop_result as $lop_row)
                            {
                                $malop=$lop_row->MaLop;
                                $tenmh=$lop_row->TenMH;
                                $phong=$lop_row->Phong;
                                
                                $title="$tenmh \n$malop P.$phong";
                                $text="$tenmh<br \>$malop P.$phong";
                                echo "<p class='item' title='$title'>$text</p>";
                            }                                                   
                            echo "</td>";                      
                        } 
                        echo "</tr>";
                          
                    }
                    echo "</table>";//end table 
                    ?>
            </div><!--end #content -->
        </div><!--end #right -->        
    
		<div id="right">
			<div id="taikhoan" class="box">
				<div class="box_header">
                    <h3>Thông tin cá nhân</h3>
                </div>
                <div id="taikhoancontent" class="box_data">
                    <p>Họ tên: <strong><?php echo $tengv; ?></strong></p>
                    <p>Học vị: <strong><?php echo $HocVi; ?></strong></p>
                    <p>Chuyên môn: <strong><?php echo $ChuyenMon; ?></strong></p>
                    <p>Khoa: <strong><span id = "khoa" ><?php echo $Khoa; ?></span></strong></p>
                    
                    <p id="action"><a href="<?php echo base_url(); ?>logout" >Thoát</a> <a href="#" id="showchangepass" >Đổi mật khẩu</a></p>
                    
                </div>
			</div>
		</div>
	</div><!--end #data -->
    </div><!--#page -->
    
   
</div><!--end #wrapper -->   



</body>
</html>