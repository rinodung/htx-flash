<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="rinodung" />
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/common.css" />
           
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/menu/menu_css.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/trangchu.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/redmond.datepick.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/cauhinh.css" />     
    
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jpopup.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.datepick.pack.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.datepick-vi.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/admin/jcauhinh.js"></script>           
	<title><?php echo $title ?></title>
</head>

<body>
<div id="wrapper">
    <div id="header">
    <img src="<?php echo static_url(); ?>/images/logo.png" alt="logo.gif" id="logo" />     
    <img src="<?php echo static_url(); ?>/images/header_pic.png" alt="header_" id="header_pic" />
     <p>Trường Đại Học Công Nghệ Thông Tin<br />
       Trường Đại Học Quốc Gia Thành Phố Hồ Chí Minh<br />
       Hệ Thống Đăng Ký Học Phần
    </p>
    </div><!--#header -->
    <div id="page">
        <!--div #main_menu -->    
        <?php include_once("vcauhinh_menu.php");  ?>      
        <!--END div #main_menu --> 
    
        <div id="data">
            <div id="left">
            <h3>Cấu hình hệ thống</h3>
                <div id="thoigian" class="data_box">
                    <h4>Thời gian đăng ký học phần</h4>
                    
                    <table>
                    <tr>
                        <td style="width: 120px;">Thời gian bắt đầu</td>
                        <td><input type="text" name="time_start" id="start" class="datepicker" value="<?php echo $time_start ;?>" /></td>
                        <td id="time_start_error"></td>
                    </tr>
                    <tr>
                        <td>Thời gian kết thúc</td>
                        <td><input type="text" name="time_end" id="end"   class="datepicker"  value="<?php echo $time_end;?>"/></td>
                    </tr>
                    </table>
                    <button>Lưu</button>
                    <img id="loading" src="<?php echo static_url()."/images/process.gif"; ?>" />                    
                </div><!--end #ttlop -->
                
                <div id="sotc" class="data_box">
                    <h4>Quy định số tín chỉ</h4>
                    <table>
                    <tr><td style="width: 120px;">Số tín chỉ tối đa</td><td><input type="text" id="tctd" value="<?php echo $TCTD;?>" /></td></tr>
                    <tr><td>Số tín chỉ gợi ý</td><td><input type="text" id="tctt" value="<?php echo $TCTT?>"/></td></tr>
                    <button>Lưu</button>
                    </table>
                </div><!--end #ttlop -->
<!--=================================QUY DINH VE KHOA=========================================================== -->                  
                <div id="khoa" class="data_box">
                    <h4>Quy định Khoa đào tạo</h4>
                    <table>
                    <tr>
                        <th >Số khoa</th>
                        <th id="sokhoa"><?php echo $num_khoa; ?></th>
                        <th colspan="2" rowspan="2" style="text-align:center;">
                        <?php
                            $add_path=static_url()."/images/button-add.png";
                            echo "<img title='Thêm Khoa mới' class='add' src='$add_path' />" ;
                        ?>
                        </th>
                    </tr>
                    <tr><th >Mã Khoa</th>
                        <th>Tên Khoa</th>
                        
                    </tr>
                    <?php
                    foreach($khoa_result as $row)
                    {
                        $makhoa=$row->MaKhoa;
                        $tenkhoa=$row->TenKhoa;
                        $edit_path=static_url()."/images/edit.png";
                        $del_path=static_url()."/images/close.png";
                        echo "<tr>";
                        echo "<td style='width:120px;' id='makhoa'>$makhoa</td>
                              <td style='width:320px;' id='tenkhoa'>$tenkhoa</td>
                              <td><img title='Sửa đổi' id='$makhoa' class='edit' src='$edit_path' /></td>
                              <td><img title='Xóa' id='$makhoa' class='del' src='$del_path' /></td>";
                        echo "</tr>";
                    }
                    ?>                  
                    </table>
                </div><!--end #ttlop -->
<!--=================================QUY DINH VE KHOA=========================================================== -->                  
                 <div id="k" class="data_box">
                    <h4>Quy định Khóa đào tạo</h4>
                    <table>
                    <tr>
                        <th>Số khóa</th>
                        <th id="sok"><?php echo $num_k; ?></th>
                        <th colspan="2" rowspan="2" style="text-align:center;">
                        <?php
                            $add_path=static_url()."/images/button-add.png";
                            echo "<img title='Thêm khóa mới' class='add' src='$add_path' />" ;
                        ?>
                        </th>
                    </tr>
                    <tr><th >Mã Khóa</th>
                        <th >Tên Khóa</th>
                        
                    </tr>
                    <?php
                    foreach($k_result as $row)
                    {
                        $mak=$row->MaK;
                        $tenk=$row->TenK;
                        
                        $edit_path=static_url()."/images/edit.png";
                        $del_path=static_url()."/images/close.png";
                        echo "<tr>";
                        echo "<td style='width:120px;' id='mak'>$mak</td>
                              <td style='width:320px;' id='tenk'>$tenk</td>
                              <td><img title='Sửa đổi' id='$mak' class='edit' src='$edit_path' /></td>
                              <td><img title='Xóa' id='$mak' class='del' src='$del_path' /></td>";
                        echo "</tr>";
                    }
                     ?>
                   
                    </table>
                </div><!--end #k -->
            </div><!--end #left -->
            
            <div id="right">
                <h3>Tài nguyên</h3>
                
<!--=================================QUY DINH VE THU=========================================================== -->            
                <div  id="thu" class="data_box">
                    <h4>Quy định về ngày(thứ bố trí giảng dạy)</h4>
                   <table>
                    <tr>
                        <th>Số Ngày(thứ)</th>
                        <th><?php echo $num_thu; ?></th>
                        <th colspan="2" rowspan="2" style="text-align:center;">
                        <?php
                            $add_path=static_url()."/images/button-add.png";
                            echo "<img title='Thêm khóa mới'class='add' src='$add_path' />" ;
                        ?>
                        </th>
                    </tr>
                    <tr><th >STT</th>
                        <th >Tên Ngày(Thứ)</th>
                        
                    </tr>
                    <?php
                    $STT=1;
                    foreach($thu_result as $row)
                    {
                        
                        $thu=$row->TenThu;
                        
                        $edit_path=static_url()."/images/edit.png";
                        $del_path=static_url()."/images/close.png";
                        echo "<tr>";
                        echo "<td style='width:120px;'>$STT</td>
                              <td style='width:320px;'>$thu</td>
                              <td><img title='Sửa đổi' id='$thu 'class='edit' src='$edit_path' /></td>
                              <td><img title='Xóa' id='$thu ' class='del' src='$del_path' /></td>";
                        echo "</tr>";
                        $STT++;
                    }
                     ?>
                   
                    </table>
                </div><!--# thu -->
                
<!--=================================QUY DINH VE CA=========================================================== -->                
                <div  class="data_box">
                    <h4>Quy định về ca(bố trí giảng dạy)</h4>
                    <table>
                        <tr>
                            <th>Số Ca</th>
                            <th colspan="2"><?php echo $num_ca; ?></th>
                            
                            <th colspan="2" rowspan="2" style="text-align:center;">
                            <?php
                                $add_path=static_url()."/images/button-add.png";
                                echo "<img title='Thêm khóa mới'class='add' src='$add_path' />" ;
                            ?>
                            </th>
                        </tr>
                        <tr><th >STT</th>
                            <th >Tên Ca(Thứ)</th>
                            <th >Thời gian</th>
                        </tr>
                        <?php
                        $STT=1;
                        foreach($ca_result as $row)
                        {
                            
                            $ca=$row->TenCa;
                            $thoigian=$row->ThoiGian;
                            
                            $edit_path=static_url()."/images/edit.png";
                            $del_path=static_url()."/images/close.png";
                            echo "<tr>";
                            echo "<td style='width:120px;'>$STT</td>
                                  <td style='width:160px;'>$ca</td>
                                  <td style='width:160px;'>$thoigian</td>
                                  <td><img title='Sửa đổi' id='$ca 'class='edit' src='$edit_path' /></td>
                                  <td><img title='Xóa' id='$ca ' class='del' src='$del_path' /></td>";
                            echo "</tr>";
                            $STT++;
                        }
                         ?>
                   
                    </table>
                </div><!--# CA -->
<!--=================================QUY DINH VE PHONG=========================================================== -->                
                <div id="phong" class="data_box">
                    <h4>Phòng học</h4>
                    <table>
                    <tr>
                        <th>Số Phòng</th>
                        <th><?php echo $num_phong; ?></th>
                        <th colspan="2" rowspan="2" style="text-align:center;">
                        <?php
                            $add_path=static_url()."/images/button-add.png";
                            echo "<img title='Thêm khóa mới'class='add' src='$add_path' />" ;
                        ?>
                        </th>
                    </tr>
                    <tr><th >STT</th>
                        <th >Tên Phòng</th>
                        
                    </tr>
                    <?php
                    $STT=1;
                    foreach($phong_result as $row)
                    {
                        
                        $tenphong=$row->TenPhong;
                        
                        $edit_path=static_url()."/images/edit.png";
                        $del_path=static_url()."/images/close.png";
                        echo "<tr>";
                        echo "<td style='width:120px;'>$STT</td>
                              <td style='width:320px;'>$tenphong</td>
                              <td><img title='Sửa đổi' id='$tenphong 'class='edit' src='$edit_path' /></td>
                              <td><img title='Xóa' id='$tenphong ' class='del' src='$del_path' /></td>";
                        echo "</tr>";
                        $STT++;
                    }
                     ?>
                   
                    </table>
                </div><!--# phong -->
                
            </div><!--end #right -->            
        </div><!--end #data -->
    </div><!--#page -->
    
    
     <!-- #footer -->
    <?php include_once("vfooter.php"); ?>
    <!-- End #footer-->
</div><!--end #wrapper --> 




<!--popup div -->
<div class="overflow"></div>
<!--=======VIEW POPUP============================================================================================== -->
<div class="popup_detail" id="add">
    <div id="pheader">
        <p id="ptitle"></p>
        <img id="pclose" title="Đóng" src="<?php echo static_url(); ?>/images/close.png" />        
    </div>
    <div id="pdata">
    
    </div>
    <div id="pfooter">
        <h4 title="Phát hiện lỗi"><img src="<?php echo static_url(); ?>/images/error.png" />Phát hiện lỗi trong quá trình kiểm tra dữ liệu.Thao tác chỉ thành công khi không còn lỗi</h4>
        <img id="save" title="Lưu" src="<?php echo static_url(); ?>/images/accept.png" />
        <img id="process" title="Đang kiểm tra" src="<?php echo static_url(); ?>/images/process.gif" />
    </div>    
</div>


<!--=======EXPORT POPUP============================================================================================== -->
<div class="popup_detail" id="edit">
    
        <div id="pheader">
            <p id="ptitle">Thao tác xuất dữ liệu</p>
            <img id="pclose" title="Đóng" src="<?php echo static_url(); ?>/images/close.png" />        
        </div>
        
        <div id="pdata">            
                
           
        </div>
        <div id="pfooter">
        <h4 title="Phát hiện lỗi"><img src="<?php echo static_url(); ?>/images/error.png" />Phát hiện lỗi trong quá trình kiểm tra dữ liệu.Thao tác chỉ thành công khi không còn lỗi</h4>
        <img id="save" title="Lưu" src="<?php echo static_url(); ?>/images/accept.png" />
        <img id="process" title="Đang kiểm tra" src="<?php echo static_url(); ?>/images/process.gif" />
        
     
    
    </div>
</div>
<!-- end popup div -->  

</body>
</html>