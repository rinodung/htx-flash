
<!DOCTYPE HTML>
<head>
	<meta name="author" content="danhkhh" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Trang Kết Quả Học Tập</title>
	<link href="<?php echo static_url();?>/css/index/diemmem.css" rel="stylesheet" type="text/css"/>

	<script type="text/javascript" src="<?php echo static_url();?>/js/jquery.min.js"></script> 
    
	<script type="text/javascript">
                    var base_url = "<?php echo base_url(); ?>";
                    var malop = "<?php echo $malop; ?>";
	</script>	
	<script type="text/javascript" src="<?php echo static_url();?>/js/admin/jdiemmem.js"></script>    
</head>

<body>
    <div id="wrapper">
        
        <div id="malop">Bảng điểm mềm lớp <?php echo $malop; ?></div>
        
        <div id='diem'>

            <table border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <th class="stt"><div style="width: 35px;" >STT</div></th>
                    <th class="mssv"><div style="width: 75px;" >MSSV</div></th>
                    <th class="tensv"><div style="width: 140px;" >Tên sinh viên</div></th>
                    <th class="giuaky"><div style="width: 60px;">Giữa kỳ</div></th>
                    <th class="cuoiky"><div style="width: 60px;">Cuối kỳ</div></th>
                    <th class="tongket"><div style="width: 60px;">Tổng kết</div></th>
                    <th class="ghichu"><div style="width: 300px;">Ghi chú</div></th>
                    <th class="diemdanh"><div style="width: 120px;">Điểm danh</div></th>
                </tr>
                <?php
                $i=0;
                foreach ($res as $val)
                {
                ?>
                
                <tr id="<?php echo $val->MaSV; ?>">
                    <td><?php echo ++$i; ?></td>
                    <td class='mssv'><?php echo $val->MaSV; ?></td>
                    <td class='tensv'><?php echo $val->TenSV; ?></td>
                    <td class='GiuaKy'><input class="data" step="0.05" type="number" value="<?php echo $val->GiuaKy; ?>" /></td>
                    <td class='CuoiKy'><input class="data" step="0.05" type="number" value="<?php echo $val->CuoiKy; ?>" /></td>
                    <td class='TongKet'><input class="data" step="0.05" type="number" value="<?php echo $val->TongKet; ?>" /></td>
                    <td class='GhiChu'><textarea class="data"><?php echo $val->GhiChu; ?></textarea></td>
                    <td class='DiemDanh'><textarea class="data"><?php echo $val->DiemDanh; ?></textarea></td>
                </tr>
                
                <?php
                }
                ?>
            </table>
            
       </div>
    </div><!-- end #wrapper -->

</body>
</html>