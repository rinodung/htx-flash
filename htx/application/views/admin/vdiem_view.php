
<!DOCTYPE HTML>
<head>
	<meta name="author" content="danhkhh" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Trang Kết Quả Học Tập</title>
	<link href="<?php echo static_url();?>/css/index/diem.css" rel="stylesheet" type="text/css"/>

	<script type="text/javascript" src="<?php echo static_url();?>/js/jquery.min.js"></script>    
    <script type="text/javascript" src="<?php echo static_url();?>/js/jquery-scroller.js"></script>
	<script type="text/javascript" src="<?php echo static_url();?>/js/index/vindex.js"></script>    
	<script type="text/javascript" src="<?php echo static_url();?>/js/index/dkhp.js"></script>
    
	<script type="text/javascript">
		$(document).ready(function()
		{		
			$("#btn_changepass").click(function(e)
			{
				if($("#oldPassword").val() == "")
				{
					alert("Bạn chưa nhập mật khẩu cũ!");
					return;					
				}
				pass1 = $("#password1").val();
				pass2 = $("#password2").val();
				if(pass1 != pass2)
				{
					$("#errorNewPassword").html("* Password mới không trùng!");
					alert("Password mới không trùng!");
				}
				else
				{
					if(pass1 == "")
					{
						alert("Bạn chưa nhập mật khẩu mới!");
						return;
					}
					var MSSV = $("#MSSV").html();
					var oldPass = $("#oldPassword").val();
					var newPass = $("#password1").val();
					$.ajax(
					{
						url: "<?php echo base_url()."index/changePass"; ?>",
						data: "MSSV="+MSSV+"&oldPass="+oldPass+"&newPass="+newPass,
						type: "POST",
						success:function(res) 
						{
							if(res=="OK")
							{
								window.location.assign("<?php echo base_url()."index/logout"; ?>");
							}
							else
								if(res == "error")
								{
									$("#errorOldPassword").fadeTo(0, 1);
									$("#errorOldPassword").html("* Password cũ sai!");
									$("#errorOldPassword").fadeTo(2000, 0);
								}
						}
					});
				}
			});
            
            
             
		});//end ready function
	</script>	
</head>

<body>
	
	<div id="popup">
	</div><!-- end #popup -->
	<div id="monnhom">
		<div id="monnhomajax">
			<div id='mntop' >
				<div id="topQTPT" class="mnshow">Quản trị & phát triển ứng dụng mạng</div>
				<div id="topTTAN" class="mnnormal">Truyền thông & an ninh mạng</div>
				<div class="mnhide">Truyền thông & an ninh mạng</div>
			</div>
			<div id='mncontent' >
				<div id="contentQTPT" class="divmnshow">
					<table class='th' cellspacing='0'>
						<tr>
							<th>Mã lớp</th>
							<th>Tên môn học</th>
							<th>Tên giáo viên</th>
							<th>Thứ</th>
							<th>Ca</th>
							<th>Phòng</th>
							<th>Min</th>
							<th>Max</th>
							<th>SLHT</th>
							<td></td>
						</tr>
						<tr>
							<td>dđ</td>
						</tr>
					</table>
				</div>
				<div id="contentTTAN" class="divmnnormal">Truyền thông& an ninh mạng</div>
				<div class="divmnhide">Truyền thông& an ninh mạng</div>
			</div>
		</div>
		<div class="selectMN">
		</div>
		<div id='mnbottom' >
			<p>*Chú ý: chọn 1 trong các lớp do phòng đào tạo mở</p>
			<p>*Đối với môn có tín chỉ thực hành thì phải đăng ký kèm theo tín  chỉ thực hành</p>
		</div>
	</div>
	<?php
		foreach ($loplt->result() as $row)
		{
			echo "<div id='div"; echo $row->MaMH; echo "' title='"; echo $row->TenMH; echo "' class='popupdetail'></div>";
		}
	?>
	<div id = "divchangepass">
		<div id="topchangepass" >Đổi mật khẩu</div>
		<button id="closechangepass" ></button>
		<div class="nottop">Mật khẩu cũ:<br> <input type="password" name="oldPassword" id="oldPassword" /><br></div>
		<div class="nottop">Mật khẩu mới:<br> <input type="password" name="password1" id="password1" /><br></div>
		<div class="nottop">Xác nhận mật khẩu mới:<br> <input type="password" name="password2" id="password2" /><br></div>
		<span id="errorOldPassword"></span>
		<span id="errorNewPassword"></span>
		<button id="btn_changepass" ></button>
		
	</div>

    <div id="wrapper">
        
       
        
        <div id="header">
        
            
            <img src="<?php echo static_url();?>/images/index/logo.png" />
            <div id="banner">
                <p>Đại Học Quốc Gia Thành Phố Hồ Chí Minh</p>
                <p>Trường Đại Học Công Nghệ Thông Tin</p>
                <p>Hệ Thống Đăng Ký Học Phần</p>
            </div><!-- end #banner -->            
        
        </div><!-- end #header -->
        
        <?php include(dirname(__FILE__)."/../index/vmenu.php");  ?>
        
           
        <div id="primary">
            <div id="left">
            <div id="content" class = "box">
                <div id="contentheader" class="box_header">
                    <h3>Bảng Điểm</h3>
                </div><!-- end #contentheader -->
                
                <div id="contenttable" class="box_data">
                    <table>
                        <tr id="first">
                            <th id="hk">Học Kỳ</th>
                            <th id="mamh">Mã Môn Học</th>
                            <th id="tenmh">Tên Môn Học</th>
                            <th id="sotc">Số TC</th>
                            <th id="tclt">TCLT</th>
                            <th id="tcth">TCTH</th>
                            <th id="diem">Điểm</th>
                            <th id="ghichu">Ghi Chú</th>
                            
                        </tr>
						<?php
							$lopdk = "";
                            $tempt_hk=1;
                            $tongtc=0;
                            $tongdiem=0;
                            

                            
							foreach ($ctdt->result() as $row)
							{
							     $hk=$row->HK;
                                 $mamh=$row->MaMH;
                                 $tenmh=$row->TenMH;
                                 $sotc=$row->SoTC;
                                 $tclt=$row->TCLT;
                                 $tcth=$row->TCTH;
                                 $diem=$row->Diem;
								
                                //thongke
                                if($hk!=$tempt_hk)
                                {
                                    
                                    
                                    echo "<tr>";
                                    echo "<td colspan='3'>Tổng cộng</td>";
                                    echo "<td >$tongtc</td>";
                                    echo "<td></td>";
                                   echo "<td></td>";
                                   
                                   echo "<td>$tongdiem</td>";
                                   echo "<td></td>";
                                    echo "</tr>";
                                    $tempt_hk=$hk;
                                    $tongdiem=0;
                                    $tongtc=0;
                                }  
                                 $tongtc+=$sotc;
                                if($diem=="") $diem=0;
                                $tongdiem+=$diem;                             
								
								if($hk %2==0)
								{	 
										echo "<tr class='even'><td>";
								}
								else
								{
								    echo "<tr class='odd'><td>";
								}
								echo $hk;
								echo "</td><td>";
								echo $mamh;
								echo "</td><td align = 'left'>";
								echo $tenmh;
								echo "</td><td class='SoTC'>";
								echo $sotc;
								echo "</td><td>";
								echo $tclt;
								echo "</td><td>";
								echo $tcth;
								echo "</td><td>";
								echo $diem;
								echo "</td>
                                <td></td>
                                </tr>";
                                
                        }
                        echo "<tr>";
                                    echo "<td colspan='3'>Tổng cộng</td>";
                                    echo "<td >$tongtc</td>";
                                    echo "<td></td>";
                                   echo "<td></td>";
                                   echo "<td>$tongdiem</td>";
                                   echo "<td></td>";
                                    echo "</tr>";
						?>
                    </table>
                </div><!-- end #contenttable -->
				
				<div id="contenttable1">
				</div><!-- end #contenttable1 -->
				
				<div id="form">				
					<form method="POST" action="<?php echo base_url()."index/register"; ?>" >
						<input type="hidden" name="khoa" value="<?php echo $khoa; ?>" />
						<input type="hidden" name="MSSV" value="<?php echo $MSSV; ?>" />
						<input type="hidden" id="denghi" name="denghi" value="" />
						<input type="hidden" id="DKHP" name="DKHP" value="<?php echo $lopdk; ?>" />
						<a id="dangky" href="<?php echo base_url()."ket-qua"; ?>">Kết quả</a>
					</form>
				</div><!-- end #form -->
				
            </div><!-- end #content -->
            </div><!-- end #left -->
			
			<div id="right">
            <div id="taikhoan" class="box">
                <div class="box_header">
                    <h3>Thông tin cá nhân</h3>
                </div>
                <div id="taikhoancontent" class="box_data">
                    <p>MSSV: <strong><span id = "MSSV" style="color: red;"><?php echo $MSSV; ?></span></strong></p>
                    <p>Họ tên: <strong><?php echo $TenSV; ?></strong></p>
                    <p>Khoa: <strong><span id = "khoa" class = <?php echo $khoa; ?>><?php	
											switch($khoa)
											{
												case "mmt": 
													echo "Mạng máy tính và truyền thông";
													break;
												case "khmt": 
													echo "Khoa học máy tính";
													break;
												case "ktmt": 
													echo "Kỹ thuật máy tính";
													break;
												case "httt": 
													echo "Hệ thống thông tin";
													break;
												case "cnpm": 
													echo "Công nghệ phần mềm";
													break;
											}
										?></span></strong></p>
                    <p>Khóa: <strong><?php echo $K; ?></strong></p>
					<span id="MaCN" style="display: none;"><? echo $MaCN; ?></span>
                    
                    <p id="action"><a href="<?php echo base_url(); ?>logout" >Thoát</a> <a href="#" id="showchangepass" >Đổi mật khẩu</a></p>
                    
                </div>
            </div>
            
             <div id="TKB" class="box">
                <div class="box_header">
                    <h3>Thời khóa biểu</h3>
                </div><!-- end #content -->
                
                <div id="TKBcontent" class="box_data">
                    <table id="TKBtable" cellspacing="0">
                        <tr>
                            <th colspan="2"></th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                        </tr>
                        <tr>
                            <td rowspan="2">Sáng</td>
                            <td>ca 1</td>
							<?php
								for($thu = 2; $thu <= 7; $thu++)
								{
									$tempTKB = explode("|",$TKB[$thu - 2]);
									if($tempTKB != "")
									{
										echo "<td class='lich' id='1".$thu."' title='".$tempTKB[1]."' >".$tempTKB[0]."</td>";
									}
									else
									{
										echo "<td class='lich' id='1".$thu."' ></td>";
									}
								}
							?>
                        </tr>
                        <tr>
                            <td style="border-left: 1px solid #DCDDDE;">ca 2</td>
							<?php
								for($thu = 2; $thu <= 7; $thu++)
								{
									$tempTKB = explode("|",$TKB[$thu + 4]);
									if($tempTKB != "")
									{
										echo "<td class='lich' id='2".$thu."' title='".$tempTKB[1]."' >".$tempTKB[0]."</td>";
									}
									else
									{
										echo "<td class='lich' id='2".$thu."' ></td>";
									}
								}
							?>
                        </tr>
                        <tr>
                            <td rowspan="2">Chiều</td>
                            <td>ca 3</td>
							<?php
								for($thu = 2; $thu <= 7; $thu++)
								{
									$tempTKB = explode("|",$TKB[$thu + 10]);
									if($tempTKB != "")
									{
										echo "<td class='lich' id='3".$thu."' title='".$tempTKB[1]."' >".$tempTKB[0]."</td>";
									}
									else
									{
										echo "<td class='lich' id='3".$thu."' ></td>";
									}
								}
							?>
                        </tr>
                        <tr>
                            <td style="border-left: 1px solid #DCDDDE">ca 4</td>
							<?php
								for($thu = 2; $thu <= 7; $thu++)
								{
									$tempTKB = explode("|",$TKB[$thu + 16]);
									if($tempTKB != "")
									{
										echo "<td class='lich' id='4".$thu."' title='".$tempTKB[1]."' >".$tempTKB[0]."</td>";
									}
									else
									{
										echo "<td class='lich' id='4".$thu."' ></td>";
									}
								}
							?>
                        </tr>
                    </table>
                    <p id="tkb"><a id="showTKB" href="<?php echo base_url()."thoi-khoa-bieu"; ?>">Chi tiết ...</a></p>
                </div><!-- end #TKBcontent -->
            </div><!-- end #TKB -->
            
            
            <div id="nhacnho" class="box">
                    <div class="box_header">
                        <h3>Lưu ý</h3>
                    </div><!-- end #mondakdheader -->
                    <div class="box_data">
						<ul class='scrollingtext'>
                        <li>- Sinh viên tự chịu trách nhiệm về những môn mình đã đăng ký.</li>
                        <li>- Số tín chỉ tối đa có thể đăng ký là:<? echo $TCTD."."; ?> </li>                        
                        <li>- Đối với các môn chuyên ngành, sinh viên cần chọn đúng các môn theo khoa chỉ định</li>
                        <li>- Đối với các lớp <b> đề nghị mở</b>, mặc định sinh viên sẽ có tên trong danh sách lớpnếu phòng đào tạo mở lớp này.</li>
                        <li>- Thời gian đăng ký từ ngày <br /> 25/12/2012 --&gt; 02/02/2013 .</li>
                        <li>- Sau khi đăng ký xong sinh viên in phiếu, ký tên và nộp lại cho phòng đào tạo.</li>
                        <li>- Sinh viên nên cập nhật thông báo thường xuyên từ phòng đào tạo.</li>
                        <li>- Sinh viên xem hướng dẫn đăng ký học phần <a href="#">tại đây.</a> </li>
                        <li>- Sinh viên nên đăng ký ít nhất là 14 tín chỉ để có thể xét học bổng.</li>
                        </ul>
						
                    </div><!-- end #lietkedk -->
					
            </div><!-- end #mondadk -->
                
			</div><!-- end #right -->
				            
        </div><!-- end #primary -->
        
		
        <div id="footer">
            <div id="diachi">
                <p>Trường Đại Học Công Nghệ Thông Tin</p>
                <p>Đại Học Quốc Gia Thành Phố Hồ Chí Minh</p>
                <p>Khu phố 6, Phường Linh Trung Thủ Đức</p>
                <p>Mail: Admin.uit@gmail.com</p>
                <p>Fax: 016888898</p>
                <p>Phone: 01699938919</p>
            </div><!-- end #diachi -->
            <div id="link">
                <img src="<?php echo static_url();?>/images/index/uit.png"  alt="uit" />
                <img src="<?php echo static_url();?>/images/index/facebook.png" alt="facebook" />
                <img src="<?php echo static_url();?>/images/index/zing.png" title="zing"  />
            </div><!-- end #link -->
        </div><!-- end #footer -->
        
    </div><!-- end #wrapper -->

</body>
</html>