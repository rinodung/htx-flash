<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/cauhoi_iframe.css" />    

        <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.min.js"></script>
        <script>
            
            $(document).ready(function()
            {
                $("#form").submit(function()
                {
                    var tg = $("#min").val() + "m" + $("#sec").val();
                    $("#tg").val(tg);
                });
            });
            
        </script>

    </head>
    
    <body>
        
        <div id="wrapper">
		
		<?php 
		
		$action = "cauhoi/addques";
		if(isset($edit) && $edit)
			$action = "cauhoi/editques";
		
		?>
		
            <form id="form" method="post" enctype="multipart/form-data" action="<?php echo base_url().$action; ?>"> 
                <div id="nd">
                    <label>Nội dung câu hỏi:</label>
                    <textarea type="text" name="nd"><?php if(isset($nd)) echo $nd; ?></textarea>
                </div>
                <div id="choice">
                    <div id="a">
                        <label>A.</label>
                        <input type="text" name="a" value="<?php if(isset($a)) echo $a; ?>"/>
                    </div>
                    
                    <div id="b">
                        <label>B.</label>
                        <input type="text" name="b" id="b" value="<?php if(isset($b)) echo $b; ?>"/>
                    </div>
                    
                    <div id="c">
                        <label>C.</label>
                        <input type="text" name="c" id="c" value="<?php if(isset($c)) echo $c; ?>"/>
                    </div>
                    
                    <div id="d">
                        <label>D.</label>
                        <input type="text" name="d" id="d" value="<?php if(isset($d)) echo $d; ?>"/>
                    </div>
                </div>
                <div id="ans">
                    <label>Đáp án:</label>
                    <select name="ans">
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                    </select>
                    <label>&#09;Thời gian:</label>
                    <input type="number" id="min" value="<?php if(isset($min)) echo $min; ?>" />m
                    <input type="number" id="sec" value="<?php if(isset($sec)) echo $sec; ?>" />s
                </div>
                <div id="img">
                    <label>Chọn hình:</label>
                    <input type="file" name="file"/>
                    <?php 
                    if(isset($img)&&$img!="")
                    {
                    ?>
                        <img id="display" width="160" height="160" id="view" src="<?php echo base_url()."application/uploads/img_question/$img" ;?>" />
                    <?php
                    }    
                    ?>
                </div>
                
                <div id="submit">
                    <input type="submit"  title="Tạo câu hỏi này" value=""/>
                </div>
                <input type="hidden" value="<?php if(isset($magv)) echo $magv; ?>" name="magv"/>
                <input type="hidden" value="<?php if(isset($mamh)) echo $mamh; ?>" name="mamh"/>
                <input type="hidden" value="<?php if(isset($tg)) echo $tg; ?>" name="tg" id="tg"/>
                <input type="hidden" value="<?php if(isset($mach)) echo $mach; ?>" name="id"/>
            </form>
        </div>
        
    </body>
    
</html>