<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="rinodung" />
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/common.css" />    
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/diem_detail.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/menu/menu_css.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/popup/popup_css.css" />
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jpopup.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/admin/jdiem_detail.js"></script>        
	<title><?php echo $title ?></title>
</head>

<body>
<div id="wrapper">
    <div id="header">
    <img src="<?php echo static_url(); ?>/images/logo.png" alt="logo.gif" id="logo" />     
    <img src="<?php echo static_url(); ?>/images/header_pic.png" alt="header_" id="header_pic" />
     <p>Trường Đại Học Công Nghệ Thông Tin<br />
       Trường Đại Học Quốc Gia Thành Phố Hồ Chí Minh<br />
       Hệ Thống Đăng Ký Học Phần
    </p>
    </div><!--#header -->
    <div id="page">
    
    <!--div #main_menu -->    
    <?php include_once("vmain_menu.php"); ?>
    <!--END div #main_menu --> 
       
    <div id="data">
        <div id="left">
        <h3>Quản lý điểm</h3>
        <ul>
            <li><a href="<?php echo base_url(); ?>quanly/sinhvien">Danh sách sinh viên</a>
            
            <?php
            echo "<ul>";                       
            foreach($khoa_result as $row)
            {   
                $num_sinhvien=$this->msinhvien->get_num_rows("",$row->MaKhoa);
                
               echo "<li id='".$row->MaKhoa."' title='".$row->TenKhoa."'><a href='".base_url()."quanly/diem/".$row->MaKhoa."'> Khoa ".$row->MaKhoa."(".$num_sinhvien.")</a></li>"; 
            }
                echo "</ul>";
            ?>
             
            </li>
            <li><a href="<?php echo base_url(); ?>quanly/sinhvien/them-sinh-vien">Thêm sinh viên</a></li>
            <li><a href="<?php echo base_url(); ?>quanly/sinhvien/nhap-du-lieu">Nhập dữ liệu</a></li>
            <li><a href="<?php echo base_url(); ?>quanly/sinhvien/thongke">Thống kê</a></li>
            
            
        </ul>
        </div><!--end #left -->
        
        <div id="right">
            <div id="tool">
                <p id="data_title">&#187;&#187;<?php echo $data_title; ?></p>                
                <p id="info"><?php echo "Mã SV: <span id='masv'>$masv</span>Họ tên: <span id='tensv'>$tensv</span>Lớp: <span id='lop'>$lop</span>" ?></p>
                <div id="action">
                    
                    <img id="del" title="Xóa" src="<?php echo static_url(); ?>/images/bin.png" alt="bin" />
                    <a href="/quanly/sinhvien/them-sinh-vien/<?php echo $khoa ?>"><img title="Thêm sinh viên" src="<?php echo static_url(); ?>/images/sv_add.png" alt="export" /></a>
                    <a href="/quanly/sinhvien/nhap-du-lieu/<?php echo $khoa ?>"><img title="Nhập dữ liệu từ tập tin" src="<?php echo static_url(); ?>/images/import.png" alt="export" /></a>
                    <img id="export" title="Xuất dữ liệu" src="<?php echo static_url(); ?>/images/export.png" alt="export" />
                    
                </div>
                <div id="search" title="Tìm kiếm">
                    
                    <form action="post" action="">
                    <input  type="text" title="Nhập MSSV hoặc họ tên" />
                    <input id="submit" type="image" title="tìm kiếm" src="<?php echo static_url(); ?>/images/search_icon.png"/>
                    </form>
                </div>
                
            </div><!--end #tool -->
            
            <div id="content">
                
                <table id="tempt">
                <tr>
                    <th id="textbox"><input id="all" type="checkbox" title="Chọn tất cả/ hủy tất cả"/></th>
                     <th id="hk">Học Kỳ</th>
                     <th id="mamh">Mã Môn Học</th>
                     <th id="tenmh">Tên Môn Học</th>
                     <th id="sotc">Số TC</th>
                     <th id="tclt">TCLT</th>
                     <th id="tcth">TCTH</th>
                     <th id="diem">Điểm</th>
                     <th id="thaotac">Thao tác</th>
                    
                </tr>            
                </table><!--end tempt -->
                
                <div id="message">
                <img alt="ok" src="<?php echo static_url() ?>/images/ok.png"/>                
                </div>
                
                <div id="change_data">
                <?php //pagination
                    
                    ?>
                    <div id="scroll">
                        <table id="table_data">
                        <tr id="first">
                            <td class="checkbox"></td>
                             <td id="hk">Học Kỳ</td>
                             <td id="mamh">Mã Môn Học</td>
                             <td id="tenmh">Tên Môn Học</td>
                             <td id="sotc">Số TC</td>
                             <td id="tclt">TCLT</td>
                             <td id="tcth">TCTH</td>
                             <td id="diem">Điểm</td>
                             <td id="thaotac">Thao tác</td>
                            
                        </tr>
                        <?php
                            
                                $tempt_hk=1;
                                $tongtc=0;
                                $tongdiem=0;
                                
    
                                
    							foreach ($ctdt->result() as $row)
    							{
    							     $hk=$row->HK;
                                     $mamh=$row->MaMH;
                                     $tenmh=$row->TenMH;
                                     $sotc=$row->SoTC;
                                     $tclt=$row->TCLT;
                                     $tcth=$row->TCTH;
                                     $diem=$row->Diem;
    								
                                    //thongke
                                    if($hk!=$tempt_hk)
                                    {
                                        
                                        
                                        echo "<tr class='sum'>";
                                        echo "<td colspan='4'>Tổng cộng</td>";
                                        echo "<td >$tongtc</td>";
                                        echo "<td></td>";
                                       echo "<td></td>";
                                       
                                       echo "<td>$tongdiem</td>";
                                       echo "<td></td>";
                                        echo "</tr>";
                                        $tempt_hk=$hk;
                                        $tongdiem=0;
                                        $tongtc=0;
                                    }  
                                     $tongtc+=$sotc;
                                    if($diem=="") $diem=0;
                                    $tongdiem+=$diem;                             
    								
    								if($hk %2==0)
    								{	 
    										echo "<tr class='even'>";
    								}
    								else
    								{
    								    echo "<tr class='odd'>";
    								}
                                    echo "<td class='checkbox'><input id='$mamh' class='checkbox_row' type='checkbox' /></td>";
                                    echo "<td class='hk'>$hk</td>";
                                    echo "<td class='mamh'>$mamh</td>";
                                    echo "<td class='tenmh' style='text-align:left;'>$tenmh</td>";
                                    echo "<td class='sotc'>$sotc</td>";
                                    echo "<td class='tclt'>$tclt</td>";
                                    echo "<td class='tcth'>$tcth</td>";
                                    echo "<td class='diem'>$diem</td>";
    								 echo "<td class='thaotac'><button id='$mamh' title='Sửa điểm môn này'>Sửa đổi</button></td>";
                                    echo "</tr>";
                                    
                                    
                            }
                            echo "<tr class='sum'>";
                                        echo "<td colspan='4'>Tổng cộng</td>";
                                        echo "<td >$tongtc</td>";
                                        echo "<td></td>";
                                       echo "<td></td>";
                                       echo "<td>$tongdiem</td>";
                                       echo "<td></td>";
                                        echo "</tr>";
                         ?>                  
                        </table>                    
                    </div><!-- end #scroll -->     
                </div><!--end #change_data -->
            </div><!--end #content -->
        </div><!--end #right -->        
    </div><!--end #data -->
    </div><!--#page -->
    
    <!-- #footer -->
    <?php include_once("vfooter.php"); ?>
    <!-- End #footer-->
   
</div><!--end #wrapper -->   

<?php include_once("vpopup.php"); ?>

</body>
</html>