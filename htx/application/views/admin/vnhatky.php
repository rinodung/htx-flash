<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="rinodung" />
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/common.css" />    
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/admin/nhatky.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/menu/menu_css.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo static_url(); ?>/css/popup/popup_css.css" />
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/jpopup.js"></script>
    <script type="text/javascript" src="<?php echo static_url(); ?>/js/admin/jnhatky.js"></script>        
	<title><?php echo $title ?></title>
</head>

<body>
<div id="wrapper">
    <div id="header">
    <img src="<?php echo static_url(); ?>/images/logo.png" alt="logo.gif" id="logo" />     
    <img src="<?php echo static_url(); ?>/images/header_pic.png" alt="header_" id="header_pic" />
     <p>Trường Đại Học Công Nghệ Thông Tin<br />
       Trường Đại Học Quốc Gia Thành Phố Hồ Chí Minh<br />
       Hệ Thống Đăng Ký Học Phần
    </p>
    </div><!--#header -->
    <div id="page">
    
    <!--div #main_menu -->    
    <?php include_once("vmain_menu.php"); ?>
    <!--END div #main_menu --> 
       
    <div id="data">
        <div id="left">
        <h3>Quản lý ghi chú</h3>
        <ul>
            <li><a id="online" class="active" href="/quanly/nhat-ky">Lớp học online</a></li>
            <li><a href="/quanly/sinhvien/them-sinh-vien">Ghi chú 2</a></li>
            <li><a href="/quanly/sinhvien/nhap-du-lieu">Ghi chú 3</a></li>
            
            
            
        </ul>
        </div><!--end #left -->
        
        <div id="right">
            <div id="tool">
                <p id="data_title">&#187;&#187;<?php echo $data_title; ?></p>                
                <select id='view_num' title="Số lượng hàng">
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="50">50</option>
                <option value="0">Tất cả</option>                    
                </select>
                <div id="action">
                    <img id="del" title="Xóa" src="<?php echo static_url(); ?>/images/bin.png" alt="bin" />                    
                  
                    <img id="export" title="Xuất dữ liệu" src="<?php echo static_url(); ?>/images/export.png" alt="export" />
                    
                </div>
                <div id="search" title="Tìm kiếm">
                    <form action="post" action="">
                    <input  type="text" title="Nhập MSSV hoặc họ tên" />
                    <input id="submit" type="image" title="tìm kiếm" src="<?php echo static_url(); ?>/images/search_icon.png"/>
                    </form>
                </div>
                <select id="date" title="Chọn ngày">          
                <option value="0">Tất cả</option>  
                <?php
                $unique=array();
                date_default_timezone_set('Asia/Ho_Chi_Minh');	
                foreach($date_log_result as $row)
                {
                    $label=date("d-m-Y",$row->time_start);
                    if(in_array($label,$unique)==FALSE)
                    {
                     
                      $val=strtotime($label);
                      
                     echo "<option value='$val' title='$val'>$label</option>";
                     $unique[]=$label;   
                    }
                   
                    
                } 
                ?>
                </select>
            </div><!--end #tool -->
            
            <div id="content">
                
                <table id="tempt">
                <tr>
                    <th id="checkbox"><input id="all" type="checkbox" title="Chọn tất cả/ hủy tất cả"/></th>
                    <th id="malop">Mã Lớp</th>
                    <th id="mamh">Tên Môn Học</th>                    
                    <th id="magv">Tên Giáo Viên</th>
                    <th id="start">Thời gian bắt đầu</th>
                    <th id="end">Thời gian kết thúc</th>
                    <th id="total">Số người tham dự</th>                    
                    <th id="thaotac">Thao tác</th>
                    
                </tr>            
                </table><!--end tempt -->
                
                <div id="message">
                <img alt="ok" src="<?php echo static_url() ?>/images/ok.png"/>                
                </div>
                
                <div id="change_data">
                <?php //pagination                    
                                         
            			echo "<div id='pagination' class='".$total_rows."'>";
                        echo $pagination;
            			echo "</div>";	
                    	
                    
                    ?>
                    <div id="scroll">
                        <table id="table_data">
                        <tr id="first">
                            <td class="checkbox"></td>
                            <td class="malop">Mã Lớp</td>
                            <td class="mamh">Tên Môn học</td>                            
                            <td class="magv">Tên Giáo Viên</td>
                            <td class="start">Thời gian bắt đầu</td>
                            <td class="end">Thời gian kết thúc</td>
                            <td class="total">Số người tham dự</td>
                            <td class="thactac">Thao tác</td>
                            
                        </tr>
                        <?php
                        date_default_timezone_set('Asia/Ho_Chi_Minh');	
                         foreach($log_result as $row)
                         {
                            $id=$row->id;
                            $malop=$row->MaLop;
                            $mamh=$row->MaMH;
                            $tenmh=$this->mmonhoc->get_tenmh_by_mamh($mamh);
                            $magv=$row->MaGV;
                            $tengv=$this->mgiaovien->get_tengv_by_magv($magv);
                            
                            $time_start=date("H:i:s d-m-Y",$row->time_start);
                            $time_end=date("H:i:s d-m-Y",$row->time_end);
                            $total=$row->total;
                            echo "<tr >";
                            echo "<td class='checkbox'><input id='$id' class='checkbox_row' type='checkbox' /></td>";
                            echo "<td class='malop' title='Xem chi tiết'>$malop</td>";
                            echo "<td class='mamh' style='text-align:left' >$tenmh</td>";                            
                            echo "<td class='magv'>$tengv</td>";
                            echo "<td class='start'>$time_start</td>";
                            echo "<td class='end'>$time_end</td>";
                            echo "<td class='total'>$total</td>";
                            echo "<td class='thaotac'><a href='#'><button title='Xem chi tiết'>Xem</button></a></td>";
                            echo "</tr>";
                         }
                         ?>                  
                        </table>                    
                    </div><!-- end #scroll -->     
                </div><!--end #change_data -->
            </div><!--end #content -->
        </div><!--end #right -->        
    </div><!--end #data -->
    </div><!--#page -->
    
    <!-- #footer -->
    <?php include_once("vfooter.php"); ?>
    <!-- End #footer-->
   
</div><!--end #wrapper -->   

<?php include_once("vpopup.php"); ?>

</body>
</html>