<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Profiler Sections
| -------------------------------------------------------------------------
| This file lets you determine whether or not various sections of Profiler
| data are displayed when the Profiler is enabled.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/profiling.html
|
*/


/*

	ach section of Profiler data can be enabled or disabled by setting a corresponding config variable to TRUE or FALSE. This can be done one of two ways. First, you can set application wide defaults with the application/config/profiler.php config file.

		$config['config']          = FALSE;
		$config['queries']         = FALSE;
		
		
		In your controllers, you can override the defaults and config file values by calling the set_profiler_sections() method of the Output class:

		$sections = array(
			'config'  => TRUE,
			'queries' => TRUE
			);

		$this->output->set_profiler_sections($sections);

	benchmarks	Elapsed time of Benchmark points and total execution time	TRUE
	config	CodeIgniter Config variables	TRUE
	controller_info	The Controller class and method requested	TRUE
	get	Any GET data passed in the request	TRUE
	http_headers	The HTTP headers for the current request	TRUE
	memory_usage	Amount of memory consumed by the current request, in bytes	TRUE
	post	Any POST data passed in the request	TRUE
	queries	Listing of all database queries executed, including execution time	TRUE
	uri_string	The URI of the current request	TRUE
	query_toggle_count	The number of queries after which the query block will default to hidden.	25
*/
/* End of file profiler.php */
/* Location: ./application/config/profiler.php */