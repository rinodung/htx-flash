-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2014 at 03:30 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `htx`
--

-- --------------------------------------------------------

--
-- Table structure for table `ca`
--

CREATE TABLE IF NOT EXISTS `ca` (
  `TenCa` int(1) NOT NULL,
  `ThoiGian` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `TietBD` int(11) NOT NULL,
  `TietKT` int(11) NOT NULL,
  PRIMARY KEY (`TenCa`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ca`
--

INSERT INTO `ca` (`TenCa`, `ThoiGian`, `TietBD`, `TietKT`) VALUES
(1, '', 1, 3),
(2, '', 4, 5),
(3, '', 6, 8),
(4, '', 9, 10);

-- --------------------------------------------------------

--
-- Table structure for table `cauhinh`
--

CREATE TABLE IF NOT EXISTS `cauhinh` (
  `DK` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `GiaTri` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`DK`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cauhinh`
--

INSERT INTO `cauhinh` (`DK`, `GiaTri`) VALUES
('TCTD', '24'),
('TCTT', '15'),
('time_end', '2014-12-30'),
('time_start', '2013-12-01'),
('learn', '1'),
('HK_CaiThien', '2');

-- --------------------------------------------------------

--
-- Table structure for table `cauhoi`
--

CREATE TABLE IF NOT EXISTS `cauhoi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MaGV` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `MaMH` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `MaCH` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ND` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `A` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `B` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `C` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `D` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Ans` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `ThoiGian` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cauhoi`
--

INSERT INTO `cauhoi` (`ID`, `MaGV`, `MaMH`, `MaCH`, `ND`, `A`, `B`, `C`, `D`, `Ans`, `ThoiGian`, `Img`) VALUES
(1, 'haidam', 'NT303', '', 'Thầy nào dạy VoIP?', 'Thầy Hải', 'Thầy Tuấn', 'Cả 2 thầy đều đúng.', 'Không phải 2 thầy.', 'A', '3m30', '');

-- --------------------------------------------------------

--
-- Table structure for table `chuyennganh`
--

CREATE TABLE IF NOT EXISTS `chuyennganh` (
  `MaCN` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `MaKhoa` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `MaK` int(2) NOT NULL,
  `TenCN` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaCN`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chuyennganh`
--

INSERT INTO `chuyennganh` (`MaCN`, `MaKhoa`, `MaK`, `TenCN`) VALUES
('CNPM_4_1', 'CNPM', 4, 'Lập trình và quản trị'),
('CNPM_4_2', 'CNPM', 4, 'An ninh thông tin'),
('HTTT_4_1', 'HTTT', 4, 'Lập trình và quản trị'),
('HTTT_4_2', 'HTTT', 4, 'An ninh mạng và bảo mật'),
('KHMT_4_1', 'KHMT', 4, 'Phát triển ứng dụng'),
('KHMT_4_2', 'KHMT', 4, 'Truyền thông thông tin và bảo mật'),
('MMT_4_1', 'MMT', 4, 'Lập trình và quản trị'),
('MMT_4_2', 'MMT', 4, 'An ninh mạng và bảo mật'),
('MMT_6_2', 'MMT', 6, 'An ninh mạng và bảo mật'),
('MMT_6_1', 'MMT', 6, 'Lập trình và quản trị'),
('MMT_5_2', 'MMT', 5, 'An ninh mạng và bảo mật'),
('MMT_5_1', 'MMT', 5, 'Lập trình và quản trị');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ctdt`
--

CREATE TABLE IF NOT EXISTS `ctdt` (
  `MaKhoa` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `MaK` int(2) NOT NULL,
  `SoHK` int(2) NOT NULL,
  PRIMARY KEY (`MaKhoa`,`MaK`),
  KEY `MaK` (`MaK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ctdt`
--

INSERT INTO `ctdt` (`MaKhoa`, `MaK`, `SoHK`) VALUES
('CNPM', 4, 9),
('CNPM', 5, 9),
('HTTT', 4, 9),
('KHMT', 4, 9),
('MMT', 4, 9),
('MMT', 6, 9),
('CNPM', 9, 9),
('MMT', 5, 9);

-- --------------------------------------------------------

--
-- Table structure for table `ctdt_cnpm`
--

CREATE TABLE IF NOT EXISTS `ctdt_cnpm` (
  `ID` int(3) NOT NULL,
  `HK` int(1) NOT NULL DEFAULT '0',
  `K` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`,`K`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ctdt_cnpm`
--

INSERT INTO `ctdt_cnpm` (`ID`, `HK`, `K`) VALUES
(6, 9, 4),
(6, 9, 5),
(7, 7, 4),
(7, 7, 5),
(8, 7, 4),
(8, 7, 5),
(9, 8, 4),
(9, 8, 5),
(10, 9, 4),
(10, 9, 5),
(15, 9, 4),
(15, 9, 5),
(16, 9, 4),
(16, 9, 5),
(27, 8, 4),
(27, 8, 5),
(28, 8, 4),
(28, 8, 5),
(29, 8, 4),
(29, 8, 5),
(37, 7, 4),
(37, 7, 5),
(38, 7, 4),
(38, 7, 5),
(39, 6, 4),
(39, 6, 5),
(40, 8, 4),
(40, 8, 5),
(41, 7, 4),
(41, 7, 5),
(42, 5, 4),
(42, 5, 5),
(43, 5, 4),
(43, 5, 5),
(44, 6, 4),
(44, 6, 5),
(45, 5, 4),
(45, 5, 5),
(46, 8, 4),
(46, 8, 5),
(47, 6, 4),
(47, 6, 5),
(48, 6, 4),
(48, 6, 5),
(49, 8, 4),
(49, 8, 5),
(50, 5, 4),
(50, 5, 5),
(51, 5, 4),
(51, 5, 5),
(52, 6, 4),
(52, 6, 5),
(53, 1, 4),
(53, 1, 5),
(54, 2, 4),
(54, 2, 5),
(55, 3, 4),
(55, 3, 5),
(56, 4, 4),
(56, 4, 5),
(57, 2, 4),
(57, 2, 5),
(58, 4, 4),
(58, 4, 5),
(59, 3, 4),
(59, 3, 5),
(60, 1, 4),
(60, 1, 5),
(61, 3, 4),
(61, 3, 5),
(62, 1, 4),
(62, 1, 5),
(63, 1, 4),
(63, 1, 5),
(64, 2, 4),
(64, 2, 5),
(65, 3, 4),
(65, 3, 5),
(66, 2, 4),
(66, 2, 5),
(67, 3, 4),
(67, 3, 5),
(68, 4, 4),
(68, 4, 5),
(69, 4, 4),
(69, 4, 5),
(70, 4, 4),
(70, 4, 5),
(71, 3, 4),
(71, 3, 5),
(72, 2, 4),
(72, 2, 5),
(73, 7, 4),
(73, 7, 5),
(74, 1, 4),
(74, 1, 5),
(75, 1, 4),
(75, 1, 5),
(76, 2, 4),
(76, 2, 5),
(77, 4, 4),
(77, 4, 5),
(79, 2, 4),
(79, 2, 5),
(74, 1, 9),
(53, 1, 9),
(60, 1, 9),
(75, 1, 9),
(62, 1, 9),
(63, 1, 9),
(66, 2, 9),
(57, 2, 9),
(54, 2, 9),
(76, 2, 9),
(64, 2, 9),
(72, 2, 9),
(79, 2, 9),
(59, 3, 9),
(55, 3, 9),
(71, 3, 9),
(67, 3, 9),
(65, 3, 9),
(61, 3, 9),
(69, 4, 9),
(56, 4, 9),
(77, 4, 9),
(70, 4, 9),
(58, 4, 9),
(68, 4, 9),
(42, 5, 9),
(45, 5, 9),
(51, 5, 9),
(43, 5, 9),
(50, 5, 9),
(39, 6, 9),
(52, 6, 9),
(44, 6, 9),
(48, 6, 9),
(47, 6, 9),
(37, 7, 9),
(38, 7, 9),
(41, 7, 9),
(7, 7, 9),
(8, 7, 9),
(73, 7, 9),
(46, 8, 9),
(49, 8, 9),
(40, 8, 9),
(9, 8, 9),
(28, 8, 9),
(29, 8, 9),
(27, 8, 9),
(15, 9, 9),
(16, 9, 9),
(6, 9, 9),
(10, 9, 9);

-- --------------------------------------------------------

--
-- Table structure for table `ctdt_httt`
--

CREATE TABLE IF NOT EXISTS `ctdt_httt` (
  `ID` int(3) NOT NULL,
  `HK` int(1) NOT NULL DEFAULT '0',
  `K` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`,`K`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ctdt_httt`
--

INSERT INTO `ctdt_httt` (`ID`, `HK`, `K`) VALUES
(6, 9, 4),
(7, 7, 4),
(8, 7, 4),
(9, 8, 4),
(10, 9, 4),
(15, 9, 4),
(16, 9, 4),
(27, 8, 4),
(28, 8, 4),
(29, 8, 4),
(37, 7, 4),
(38, 7, 4),
(39, 6, 4),
(40, 8, 4),
(41, 7, 4),
(42, 5, 4),
(43, 5, 4),
(44, 6, 4),
(45, 5, 4),
(46, 8, 4),
(47, 6, 4),
(48, 6, 4),
(49, 8, 4),
(50, 5, 4),
(51, 5, 4),
(52, 6, 4),
(53, 1, 4),
(54, 2, 4),
(55, 3, 4),
(56, 4, 4),
(57, 2, 4),
(58, 4, 4),
(59, 3, 4),
(60, 1, 4),
(61, 3, 4),
(62, 1, 4),
(63, 1, 4),
(64, 2, 4),
(65, 3, 4),
(66, 2, 4),
(67, 3, 4),
(68, 4, 4),
(69, 4, 4),
(70, 4, 4),
(71, 3, 4),
(72, 2, 4),
(73, 7, 4),
(74, 1, 4),
(75, 1, 4),
(76, 2, 4),
(77, 4, 4),
(78, 1, 4),
(79, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ctdt_khmt`
--

CREATE TABLE IF NOT EXISTS `ctdt_khmt` (
  `ID` int(3) NOT NULL,
  `HK` int(1) NOT NULL DEFAULT '0',
  `K` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`,`K`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ctdt_khmt`
--

INSERT INTO `ctdt_khmt` (`ID`, `HK`, `K`) VALUES
(6, 9, 4),
(7, 7, 4),
(8, 7, 4),
(9, 8, 4),
(10, 9, 4),
(15, 9, 4),
(16, 9, 4),
(27, 8, 4),
(28, 8, 4),
(29, 8, 4),
(37, 7, 4),
(38, 7, 4),
(39, 6, 4),
(40, 8, 4),
(41, 7, 4),
(42, 5, 4),
(43, 5, 4),
(44, 6, 4),
(45, 5, 4),
(46, 8, 4),
(47, 6, 4),
(48, 6, 4),
(49, 8, 4),
(50, 5, 4),
(51, 5, 4),
(52, 6, 4),
(53, 1, 4),
(54, 2, 4),
(55, 3, 4),
(56, 4, 4),
(57, 2, 4),
(58, 4, 4),
(59, 3, 4),
(60, 1, 4),
(61, 3, 4),
(62, 1, 4),
(63, 1, 4),
(64, 2, 4),
(65, 3, 4),
(66, 2, 4),
(67, 3, 4),
(68, 4, 4),
(69, 4, 4),
(70, 4, 4),
(71, 3, 4),
(72, 2, 4),
(73, 7, 4),
(74, 1, 4),
(75, 1, 4),
(76, 2, 4),
(77, 4, 4),
(78, 1, 4),
(79, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ctdt_ktmt`
--

CREATE TABLE IF NOT EXISTS `ctdt_ktmt` (
  `ID` int(3) NOT NULL,
  `HK` int(1) NOT NULL DEFAULT '0',
  `K` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`,`K`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ctdt_mmt`
--

CREATE TABLE IF NOT EXISTS `ctdt_mmt` (
  `ID` int(3) NOT NULL,
  `HK` int(1) NOT NULL DEFAULT '0',
  `K` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`,`K`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ctdt_mmt`
--

INSERT INTO `ctdt_mmt` (`ID`, `HK`, `K`) VALUES
(6, 9, 4),
(10, 9, 6),
(7, 7, 4),
(6, 9, 6),
(8, 7, 4),
(16, 9, 6),
(9, 8, 4),
(15, 9, 6),
(10, 9, 4),
(27, 8, 6),
(15, 9, 4),
(29, 8, 6),
(16, 9, 4),
(9, 8, 6),
(27, 8, 4),
(40, 8, 6),
(28, 7, 4),
(49, 8, 6),
(29, 8, 4),
(46, 8, 6),
(37, 7, 4),
(73, 7, 6),
(38, 7, 4),
(28, 7, 6),
(39, 6, 4),
(7, 7, 6),
(40, 8, 4),
(8, 7, 6),
(41, 7, 4),
(41, 7, 6),
(42, 5, 4),
(38, 7, 6),
(43, 5, 4),
(37, 7, 6),
(44, 6, 4),
(47, 6, 6),
(45, 5, 4),
(48, 6, 6),
(46, 8, 4),
(44, 6, 6),
(47, 6, 4),
(52, 6, 6),
(48, 6, 4),
(39, 6, 6),
(49, 8, 4),
(50, 5, 6),
(50, 5, 4),
(43, 5, 6),
(51, 5, 4),
(51, 5, 6),
(52, 6, 4),
(45, 5, 6),
(53, 1, 4),
(42, 5, 6),
(54, 2, 4),
(68, 4, 6),
(55, 3, 4),
(58, 4, 6),
(56, 4, 4),
(70, 4, 6),
(57, 2, 4),
(77, 4, 6),
(58, 4, 4),
(56, 4, 6),
(59, 3, 4),
(69, 4, 6),
(60, 1, 4),
(61, 3, 6),
(61, 3, 4),
(65, 3, 6),
(62, 1, 4),
(67, 3, 6),
(63, 1, 4),
(71, 3, 6),
(64, 2, 4),
(55, 3, 6),
(65, 3, 4),
(59, 3, 6),
(66, 2, 4),
(79, 2, 6),
(67, 3, 4),
(72, 2, 6),
(68, 4, 4),
(64, 2, 6),
(69, 4, 4),
(76, 2, 6),
(70, 4, 4),
(54, 2, 6),
(71, 3, 4),
(57, 2, 6),
(72, 2, 4),
(66, 2, 6),
(73, 7, 4),
(78, 1, 6),
(74, 1, 4),
(63, 1, 6),
(75, 1, 4),
(62, 1, 6),
(76, 2, 4),
(75, 1, 6),
(77, 4, 4),
(60, 1, 6),
(78, 1, 4),
(53, 1, 6),
(79, 2, 4),
(74, 1, 6),
(10, 9, 5),
(6, 9, 5),
(16, 9, 5),
(15, 9, 5),
(27, 8, 5),
(29, 8, 5),
(9, 8, 5),
(40, 8, 5),
(49, 8, 5),
(46, 8, 5),
(73, 7, 5),
(28, 7, 5),
(7, 7, 5),
(8, 7, 5),
(41, 7, 5),
(38, 7, 5),
(37, 7, 5),
(47, 6, 5),
(48, 6, 5),
(44, 6, 5),
(52, 6, 5),
(39, 6, 5),
(50, 5, 5),
(43, 5, 5),
(51, 5, 5),
(45, 5, 5),
(42, 5, 5),
(68, 4, 5),
(58, 4, 5),
(70, 4, 5),
(77, 4, 5),
(56, 4, 5),
(69, 4, 5),
(61, 3, 5),
(65, 3, 5),
(67, 3, 5),
(71, 3, 5),
(55, 3, 5),
(59, 3, 5),
(79, 2, 5),
(72, 2, 5),
(64, 2, 5),
(76, 2, 5),
(54, 2, 5),
(57, 2, 5),
(66, 2, 5),
(78, 1, 5),
(63, 1, 5),
(62, 1, 5),
(75, 1, 5),
(60, 1, 5),
(53, 1, 5),
(74, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `dangky_cnpm`
--

CREATE TABLE IF NOT EXISTS `dangky_cnpm` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MaLop` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `GioDK` datetime DEFAULT NULL,
  PRIMARY KEY (`MaSV`,`MaLop`),
  KEY `MaLop` (`MaLop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dangky_httt`
--

CREATE TABLE IF NOT EXISTS `dangky_httt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MaLop` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `GioDK` datetime DEFAULT NULL,
  PRIMARY KEY (`MaSV`,`MaLop`),
  KEY `MaLop` (`MaLop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dangky_khmt`
--

CREATE TABLE IF NOT EXISTS `dangky_khmt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MaLop` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `GioDK` datetime DEFAULT NULL,
  PRIMARY KEY (`MaSV`,`MaLop`),
  KEY `MaLop` (`MaLop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dangky_ktmt`
--

CREATE TABLE IF NOT EXISTS `dangky_ktmt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MaLop` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `GioDK` datetime DEFAULT NULL,
  PRIMARY KEY (`MaSV`,`MaLop`),
  KEY `MaLop` (`MaLop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dangky_ktmt`
--

INSERT INTO `dangky_ktmt` (`MaSV`, `MaLop`, `GioDK`) VALUES
('09520032', 'VCPL1.C11', '2012-11-30 23:35:25');

-- --------------------------------------------------------

--
-- Table structure for table `dangky_mmt`
--

CREATE TABLE IF NOT EXISTS `dangky_mmt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MaLop` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `GioDK` datetime DEFAULT NULL,
  PRIMARY KEY (`MaSV`,`MaLop`),
  KEY `MaLop` (`MaLop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dangky_mmt`
--

INSERT INTO `dangky_mmt` (`MaSV`, `MaLop`, `GioDK`) VALUES
('09520044', 'NT101.C11', '2013-08-22 19:18:12'),
('09520044', 'NT301.C12', '2013-08-23 08:42:52'),
('09520044', 'NT305.C11', '2013-08-23 08:41:42'),
('09520032', 'NT301.C21', '2014-01-29 15:35:36'),
('09520044', 'NT303.C11', '2013-08-22 19:25:24'),
('09520001', 'NT101.C11', '2013-08-22 19:18:12'),
('09520001', 'NT301.C12', '2013-08-23 08:42:52'),
('09520001', 'NT305.C11', '2013-08-23 08:41:42'),
('09520001', 'NT303.C11', '2013-08-22 19:25:24'),
('09520003', 'NT101.C11', '2013-08-22 19:18:12'),
('09520003', 'NT301.C12', '2013-08-23 08:42:52'),
('09520003', 'NT305.C11', '2013-08-23 08:41:42'),
('09520003', 'NT303.C11', '2013-08-22 19:25:24'),
('09520004', 'NT101.C11', '2013-08-22 19:18:12'),
('09520004', 'NT301.C12', '2013-08-23 08:42:52'),
('09520004', 'NT305.C11', '2013-08-23 08:41:42'),
('09520004', 'NT303.C11', '2013-08-22 19:25:24'),
('09520005', 'NT101.C11', '2013-08-22 19:18:12'),
('09520005', 'NT301.C12', '2013-08-23 08:42:52'),
('09520005', 'NT305.C11', '2013-08-23 08:41:42'),
('09520005', 'NT303.C11', '2013-08-22 19:25:24'),
('09520006', 'NT101.C11', '2013-08-22 19:18:12'),
('09520006', 'NT301.C12', '2013-08-23 08:42:52'),
('09520006', 'NT305.C11', '2013-08-23 08:41:42'),
('09520006', 'NT303.C11', '2013-08-22 19:25:24'),
('09520007', 'NT101.C11', '2013-08-22 19:18:12'),
('09520007', 'NT301.C12', '2013-08-23 08:42:52'),
('09520007', 'NT305.C11', '2013-08-23 08:41:42'),
('09520007', 'NT303.C11', '2013-08-22 19:25:24'),
('09520008', 'NT101.C11', '2013-08-22 19:18:12'),
('09520008', 'NT301.C12', '2013-08-23 08:42:52'),
('09520008', 'NT305.C11', '2013-08-23 08:41:42'),
('09520008', 'NT303.C11', '2013-08-22 19:25:24'),
('09520002', 'NT101.C11', '2013-08-22 19:18:12'),
('09520002', 'NT301.C12', '2013-08-23 08:42:52'),
('09520002', 'NT305.C11', '2013-08-23 08:41:42'),
('09520002', 'NT303.C11', '2013-08-22 19:25:24'),
('09520032', 'NT303.C21', '2014-02-12 08:10:57');

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE IF NOT EXISTS `data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `name`, `city`) VALUES
(1, 'dũng', 'Thủ đức'),
(2, 'Danh', 'Tp.HCM'),
(3, 'Hà', 'Bồ'),
(4, 'hehe', 'ssss'),
(5, 'dong dung', 'dd'),
(6, 'Đồng', 'Dũng'),
(7, 'Đồng', 'Dũng2'),
(8, 'what the heil', 'sss'),
(9, 'we', 'aa'),
(10, 'aaa', 'bbbb'),
(11, 'đồng dũng', 'Thủ đức'),
(12, 'what the hell', 'zing.vn'),
(13, 'what the hell', 'zing.vn'),
(14, 'user1', 'user2'),
(15, 'Bác ba phi', 'Nam phi');

-- --------------------------------------------------------

--
-- Table structure for table `denghi`
--

CREATE TABLE IF NOT EXISTS `denghi` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MaMH` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `GioDK` datetime DEFAULT NULL,
  PRIMARY KEY (`MaSV`,`MaMH`),
  KEY `MaLop` (`MaMH`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diem`
--

CREATE TABLE IF NOT EXISTS `diem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `masv` varchar(10) NOT NULL,
  `mamh` varchar(20) NOT NULL,
  `malop` varchar(20) DEFAULT NULL,
  `sotc` smallint(5) unsigned NOT NULL,
  `namhoc` smallint(5) unsigned NOT NULL,
  `hocky` smallint(5) unsigned NOT NULL,
  `diem1` double DEFAULT '0' COMMENT 'Điểm QT',
  `diem2` double DEFAULT '0' COMMENT 'Điểm TH',
  `diem3` double DEFAULT '0' COMMENT 'Điểm GK',
  `diem4` double DEFAULT '0' COMMENT 'Điểm CK',
  `diem` double DEFAULT '0' COMMENT 'Điểm HP',
  `thoigiancapnhat` datetime DEFAULT NULL,
  `trangthai` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '0: hủy; 1: bình thường; 2: trả nợ; 3: cải thiện; 4: khác',
  `heso1` int(10) unsigned NOT NULL DEFAULT '0',
  `heso2` float NOT NULL DEFAULT '0',
  `heso3` float NOT NULL DEFAULT '0',
  `heso4` float NOT NULL DEFAULT '0',
  `ghichu` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `masv` (`masv`,`mamh`,`namhoc`,`hocky`),
  KEY `mamh` (`mamh`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `diem`
--

INSERT INTO `diem` (`id`, `masv`, `mamh`, `malop`, `sotc`, `namhoc`, `hocky`, `diem1`, `diem2`, `diem3`, `diem4`, `diem`, `thoigiancapnhat`, `trangthai`, `heso1`, `heso2`, `heso3`, `heso4`, `ghichu`) VALUES
(1, '09520032', 'DBSS1', 'DBSS1.D15', 4, 2012, 1, 0, 7.57, 7, 4.5, 6, '2013-06-29 10:56:58', 1, 0, 0, 0, 0, ''),
(2, '09520032', 'CARC1', 'CARC1.D15', 4, 2012, 1, 0, 6.11, 6, 4.75, 5.5, '2013-06-29 10:56:58', 1, 0, 0, 0, 0, ''),
(3, '09520032', 'NT301', 'NT301.D12', 4, 2012, 1, 0, 2.82, 5.5, 4, 4, '2013-06-29 10:56:58', 1, 0, 0, 0, 0, ''),
(4, '09520032', 'PH002', 'PH002.D21', 3, 2012, 2, 0, 3.5, 4.5, 1, 2.5, '2013-07-17 11:38:13', 1, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `diem_cnpm`
--

CREATE TABLE IF NOT EXISTS `diem_cnpm` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ID` int(3) NOT NULL,
  `Diem` int(2) DEFAULT NULL,
  PRIMARY KEY (`MaSV`,`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diem_httt`
--

CREATE TABLE IF NOT EXISTS `diem_httt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ID` int(3) NOT NULL,
  `Diem` int(2) DEFAULT NULL,
  PRIMARY KEY (`MaSV`,`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diem_khmt`
--

CREATE TABLE IF NOT EXISTS `diem_khmt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ID` int(3) NOT NULL,
  `Diem` int(2) DEFAULT NULL,
  PRIMARY KEY (`MaSV`,`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diem_ktmt`
--

CREATE TABLE IF NOT EXISTS `diem_ktmt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ID` int(3) NOT NULL,
  `Diem` int(2) DEFAULT NULL,
  PRIMARY KEY (`MaSV`,`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `diem_mem`
--

CREATE TABLE IF NOT EXISTS `diem_mem` (
  `MaLop` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `MaSV` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `TenSV` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `GiuaKy` float DEFAULT NULL,
  `CuoiKy` float DEFAULT NULL,
  `TongKet` float DEFAULT NULL,
  `GhiChu` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DiemDanh` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diem_mem`
--

INSERT INTO `diem_mem` (`MaLop`, `MaSV`, `TenSV`, `GiuaKy`, `CuoiKy`, `TongKet`, `GhiChu`, `DiemDanh`) VALUES
('NT101.C11', '09520032', 'Nguyễn Quý Danh', NULL, NULL, NULL, '', 'x'),
('NT101.C11', '10520001', 'Nguyễn Văn A', NULL, NULL, NULL, '', 'x'),
('NT301.C21', '09520032', 'Nguyễn Quý Danh', NULL, NULL, NULL, '', ''),
('NT303.C21', '09520032', 'Nguyễn Quý Danh', NULL, NULL, NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `diem_mmt`
--

CREATE TABLE IF NOT EXISTS `diem_mmt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Diem` int(2) DEFAULT NULL,
  `ID` int(3) NOT NULL,
  PRIMARY KEY (`MaSV`,`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `diem_mmt`
--

INSERT INTO `diem_mmt` (`MaSV`, `Diem`, `ID`) VALUES
('09520002', 9, 79),
('09520002', 7, 78),
('09520002', 7, 77),
('09520002', 9, 76),
('09520002', 7, 75),
('09520002', 8, 74),
('09520002', 8, 72),
('09520002', 8, 71),
('09520002', 8, 70),
('09520002', 9, 69),
('09520002', 7, 68),
('09520002', 8, 67),
('09520002', 10, 66),
('09520002', 7, 65),
('09520002', 7, 64),
('09520002', 7, 63),
('09520002', 7, 62),
('09520002', 8, 61),
('09520002', 9, 60),
('09520002', 6, 59),
('09520002', 8, 58),
('09520002', 7, 57),
('09520002', 8, 56),
('09520002', 6, 55),
('09520002', 9, 54),
('09520002', 7, 53),
('09520002', 8, 52),
('09520002', 9, 51),
('09520002', 8, 50),
('09520002', 7, 48),
('09520002', 9, 47),
('09520002', 7, 45),
('09520002', 8, 44),
('09520002', 8, 43),
('09520002', 9, 42),
('09520002', 7, 39),
('09520032', 9, 79),
('09520032', 7, 78),
('09520032', 7, 77),
('09520032', 9, 76),
('09520032', 7, 75),
('09520032', 8, 74),
('09520032', 8, 72),
('09520032', 8, 71),
('09520032', 8, 70),
('09520032', 9, 69),
('09520032', 7, 68),
('09520032', 8, 67),
('09520032', 10, 66),
('09520032', 7, 65),
('09520032', 7, 64),
('09520032', 7, 63),
('09520032', 7, 62),
('09520032', 8, 61),
('09520032', 9, 60),
('09520032', 6, 59),
('09520032', 8, 58),
('09520032', 7, 57),
('09520032', 8, 56),
('09520032', 6, 55),
('09520032', 9, 54),
('09520032', 7, 53),
('09520032', 8, 52),
('09520032', 9, 51),
('09520032', 8, 50),
('09520032', 7, 48),
('09520032', 9, 47),
('09520032', 7, 45),
('09520032', 8, 44),
('09520032', 8, 43),
('09520032', 9, 42),
('09520032', 4, 39);

-- --------------------------------------------------------

--
-- Table structure for table `dkcn`
--

CREATE TABLE IF NOT EXISTS `dkcn` (
  `MaSV` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `IDnhom` int(11) NOT NULL,
  `MaLop` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaSV`,`IDnhom`,`MaLop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dkcn`
--

INSERT INTO `dkcn` (`MaSV`, `IDnhom`, `MaLop`) VALUES
('09520032', 8, 'NT301.C21'),
('09520032', 28, 'NT303.C12'),
('09520032', 28, 'NT303.C21');

-- --------------------------------------------------------

--
-- Table structure for table `giaovien`
--

CREATE TABLE IF NOT EXISTS `giaovien` (
  `MaGV` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `TenGV` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `NgaySinh` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `NoiSinh` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `SoDT` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `HocVi` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `KinhNghiem` int(11) NOT NULL,
  `ChuyenMon` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Khoa` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `NoiGD` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaGV`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `giaovien`
--

INSERT INTO `giaovien` (`MaGV`, `TenGV`, `NgaySinh`, `NoiSinh`, `SoDT`, `email`, `HocVi`, `KinhNghiem`, `ChuyenMon`, `Khoa`, `NoiGD`) VALUES
('HAIDAM', 'Đàm Quang Hồng Hải', '11/01/1985', 'Hà Nội', '0168888991', 'damhai@uit.edu.vn', 'TS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('NAMNGUYEN', 'Nguyễn Tuấn Nam', '', '', '', 'namnguyen@uit.edu.vn', 'TS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('TUANNGUYEN', 'Nguyễn Anh Tuấn', '', '', '', '', 'TS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('NHIEMTB', 'Trần Bá Nhiệm', '', '', '', '', 'ThS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('DUYN', 'Nguyễn Duy', '', '', '', '', 'TS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('NGHILH', 'Lê Hồng Nghi', '26/01/1986', '', '', '', 'ThS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('MANHLE', 'Lê Mạnh', '', '', '', '', 'TS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('MINHLN', 'Lê Nhật Minh', '', '', '', '', 'TS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('HUNGTM', 'Trần Mạnh Hùng', '', '', '', '', 'TS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('DUNGVT', 'Vũ Trí Dũng', '', '', '', '', 'TS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('MINHNQ', 'Nguyễn Quang Minh', '', '', '', '', 'TS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('QUANGTNN', 'Tô Nguyễn Nhật Quang', '', '', '', '', 'TS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.'),
('HUNGLK', 'Lê Kim Hùng', '', '', '', '', 'KS', 30, 'Mạng Máy tính, truyền dữ liệu, VoIP, xử lý tín hiệu số...', 'Mạng Máy Tính & Truyền Thông', 'Trường Đại Học Công Nghệ Tin, Trung tâm phát triển Công Nghệ Thông Tin, Đại học Khoa Học Tự Nhiên.');

-- --------------------------------------------------------

--
-- Table structure for table `inphieu`
--

CREATE TABLE IF NOT EXISTS `inphieu` (
  `MaSV` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `SL` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MaSV`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inphieu`
--

INSERT INTO `inphieu` (`MaSV`, `SL`) VALUES
('09520001', 0),
('09520002', 0),
('09520005', 0),
('09520008', 0),
('09520009', 0),
('09520032', 2),
('09520044', 0);

-- --------------------------------------------------------

--
-- Table structure for table `k`
--

CREATE TABLE IF NOT EXISTS `k` (
  `MaK` int(2) NOT NULL,
  `TenK` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaK`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `k`
--

INSERT INTO `k` (`MaK`, `TenK`) VALUES
(4, 'K4 (2009)'),
(5, 'K5 (2010)'),
(6, 'K6 (2011)'),
(7, 'K7 (2012)'),
(8, 'K8(2013)'),
(9, 'K9(2014)'),
(10, 'K10(2015)');

-- --------------------------------------------------------

--
-- Table structure for table `khoa`
--

CREATE TABLE IF NOT EXISTS `khoa` (
  `MaKhoa` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TenKhoa` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaKhoa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `khoa`
--

INSERT INTO `khoa` (`MaKhoa`, `TenKhoa`) VALUES
('CNPM', 'Công Nghệ Phần Mềm'),
('HTTT', 'Hệ Thống Thông Tin'),
('KHMT', 'Khoa Học Máy Tính'),
('KTMT', 'Kỹ Thuật Máy Tính'),
('MMT', 'Mạng Máy Tính và Truyền Thông');

-- --------------------------------------------------------

--
-- Table structure for table `lich`
--

CREATE TABLE IF NOT EXISTS `lich` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Thu` smallint(6) NOT NULL,
  `TietBD` smallint(6) NOT NULL,
  `TietKT` smallint(6) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `lich`
--

INSERT INTO `lich` (`ID`, `Thu`, `TietBD`, `TietKT`) VALUES
(1, 2, 1, 3),
(2, 4, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `loai_monhoc`
--

CREATE TABLE IF NOT EXISTS `loai_monhoc` (
  `STT` int(1) NOT NULL,
  `MaLoai` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TenLoai` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaLoai`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loai_monhoc`
--

INSERT INTO `loai_monhoc` (`STT`, `MaLoai`, `TenLoai`) VALUES
(3, 'CN', 'Chuyên Nghành'),
(2, 'CSN', 'Cơ Sở Nghành'),
(1, 'DC', 'Đại Cương'),
(4, 'TC', 'Tự Chọn');

-- --------------------------------------------------------

--
-- Table structure for table `log_room`
--

CREATE TABLE IF NOT EXISTS `log_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MaLop` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mã lớp',
  `MaMH` varchar(5) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mã môn học',
  `MaGV` varchar(5) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mã Giáo Viên',
  `time_start` int(11) NOT NULL COMMENT 'Thời gian bắt đầu',
  `time_end` int(11) NOT NULL COMMENT 'Thời gian kết thúc',
  `total` int(11) NOT NULL COMMENT 'Số người tham gia',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `log_room`
--

INSERT INTO `log_room` (`id`, `MaLop`, `MaMH`, `MaGV`, `time_start`, `time_end`, `total`) VALUES
(7, 'CARC1.C11', 'CARC1', 'GV01', 1375870295, 1375873895, 34),
(8, 'CARC1.C11', 'CARC1', 'GV01', 1375870315, 1375873915, 43),
(9, 'DBSS1.C11', 'DBSS1', 'GV02', 1375870348, 1375873948, 33),
(10, 'CARC1.C11', 'CARC1', 'GV01', 1375888755, 1375892355, 39),
(11, 'CARC1.C11', 'CARC1', 'GV01', 1375888779, 1375892379, 19),
(12, 'CARC1.C11', 'CARC1', 'GV01', 1375888805, 1375892405, 24),
(13, 'CARC1.C11', 'CARC1', 'GV01', 1375888814, 1375892414, 10),
(14, 'CARC1.C11', 'CARC1', 'GV01', 1375944050, 1375947650, 17),
(15, 'DBSS1.C11', 'DBSS1', 'GV02', 1375944198, 1375947798, 28),
(16, 'CARC1.C11', 'DBSS1', 'GV02', 1375944200, 1375947800, 39),
(17, 'CARC1.C11', 'CARC1', 'GV02', 1375944202, 1375947802, 39),
(18, 'CARC1.C11', 'CARC1', 'GV01', 1375944204, 1375947804, 43),
(19, 'CARC1.C11', 'CARC1', 'GV01', 1375944215, 1375947815, 12),
(20, 'CARC1.C11', 'CARC1', 'GV01', 1375944216, 1375947816, 47),
(21, 'CARC1.C11', 'CARC1', 'GV01', 1375944217, 1375947817, 26),
(22, 'CARC1.C11', 'CARC1', 'GV01', 1375944217, 1375947817, 29),
(23, 'CARC1.C11', 'DBSS1', 'GV01', 1375946601, 1375950201, 23),
(24, 'CARC1.C11', 'DBSS1', 'GV02', 1375946603, 1375950203, 25),
(25, 'DBSS1.C11', 'DBSS1', 'GV02', 1375946605, 1375950205, 42),
(26, 'CARC1.C11', 'CARC1', 'GV02', 1375946609, 1375950209, 48),
(27, 'CARC1.C11', 'CARC1', 'GV02', 1375946610, 1375950210, 43),
(28, 'CARC1.C11', 'CARC1', 'GV02', 1375946610, 1375950210, 50),
(29, 'CARC1.C11', 'CARC1', 'GV02', 1375946611, 1375950211, 16),
(30, 'CARC1.C11', 'CARC1', 'GV02', 1375946611, 1375950211, 10),
(31, 'CARC1.C11', 'CARC1', 'GV02', 1375946611, 1375950211, 50),
(32, 'CARC1.C11', 'CARC1', 'GV02', 1375948818, 1375952418, 22),
(33, 'DBSS1.C11', 'CARC1', 'GV01', 1375948822, 1375952422, 27),
(34, 'DBSS1.C11', 'DBSS1', 'GV02', 1375948825, 1375952425, 21),
(35, 'CARC1.C11', 'DBSS1', 'GV02', 1375948827, 1375952427, 41);

-- --------------------------------------------------------

--
-- Table structure for table `lop`
--

CREATE TABLE IF NOT EXISTS `lop` (
  `MaLop` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MaGV` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `MaMH` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phong` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Thu` int(1) DEFAULT NULL,
  `Ca` int(1) DEFAULT NULL,
  `Min` int(2) DEFAULT NULL,
  `Max` int(3) DEFAULT NULL,
  `SLHT` int(3) DEFAULT NULL,
  PRIMARY KEY (`MaLop`),
  KEY `FK_lop_mh` (`MaMH`),
  KEY `FK_lop_gv` (`MaGV`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lop`
--

INSERT INTO `lop` (`MaLop`, `MaGV`, `MaMH`, `Phong`, `Thu`, `Ca`, `Min`, `Max`, `SLHT`) VALUES
('CARC1.C11', 'GV05', 'CARC1', 'PM', 6, 2, 30, 150, 149),
('DBSS1.C11', 'GV05', 'DBSS1', '102', 2, 3, 6, 7, 8),
('ENG02.C11', 'GV02', 'ENG02', '101', 3, 3, 6, 7, 8),
('ENG02.C122', 'GV02', 'ENG02', '101', 4, 2, 6, 7, 8),
('HCMT1.C11', 'GV05', 'HCMT1', '108', 2, 2, 32, 100, 0),
('ITEM1.C11', 'GV01', 'ITEM1', '308', 6, 3, 6, 7, 8),
('MAT22.C11', 'GV03', 'MAT22', 'PM', 4, 1, 30, 150, 150),
('NT101.C11', 'GV01', 'NT101', '101', 2, 1, 6, 7, 8),
('NT104.C11', 'GV03', 'NT104', '101', 5, 3, 6, 7, 8),
('NT104.C122', 'GV02', 'NT104', '101', 3, 4, 6, 7, 8),
('NT108.C11', 'GV02', 'NT108', '203', 3, 2, 6, 7, 8),
('NT109.C11', 'GV01', 'NT109', '302', 2, 4, 6, 7, 8),
('NT111.C11', 'GV03', 'NT111', '204', 5, 1, 50, 150, 54),
('NT201.C11', 'GV04', 'NT201', '205', 4, 4, 50, 150, 149),
('NT203.C11', 'GV02', 'NT203', '102', 4, 3, 6, 7, 8),
('OSYS1.C11', 'GV03', 'OSYS1', '208', 3, 1, 50, 150, 150),
('PEDU1.C11', 'GV04', 'PEDU1', '102', 5, 3, 6, 7, 8),
('PHY02.C11', 'GV04', 'PHY02', 'PM', 5, 2, 30, 150, 150),
('VCPL1.C11', 'GV05', 'VCPL1', '301', 6, 4, 50, 150, 47);

-- --------------------------------------------------------

--
-- Table structure for table `lophoc`
--

CREATE TABLE IF NOT EXISTS `lophoc` (
  `TenLop` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `MaKhoa` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `MaK` int(2) NOT NULL,
  PRIMARY KEY (`TenLop`),
  KEY `MaKhoa` (`MaKhoa`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lophoc`
--

INSERT INTO `lophoc` (`TenLop`, `MaKhoa`, `MaK`) VALUES
('CNPM01', 'CNPM', 1),
('CNPM02', 'CNPM', 2),
('CNPM03', 'CNPM', 3),
('CNPM04', 'CNPM', 4),
('CNPM05', 'CNPM', 5),
('CNPM06', 'CNPM', 6),
('CNPM07', 'CNPM', 7),
('HTTT01', 'HTTT', 1),
('HTTT02', 'HTTT', 2),
('HTTT03', 'HTTT', 3),
('HTTT04', 'HTTT', 4),
('HTTT05', 'HTTT', 5),
('HTTT06', 'HTTT', 6),
('HTTT07', 'HTTT', 7),
('KHMT01', 'KHMT', 1),
('KHMT02', 'KHMT', 2),
('KHMT03', 'KHMT', 3),
('KHMT04', 'KHMT', 4),
('KHMT05', 'KHMT', 5),
('KHMT06', 'KHMT', 6),
('KHMT07', 'KHMT', 7),
('KTMT01', 'KTMT', 1),
('KTMT02', 'KTMT', 2),
('KTMT03', 'KTMT', 3),
('KTMT04', 'KTMT', 4),
('KTMT05', 'KTMT', 5),
('KTMT06', 'KTMT', 6),
('KTMT07', 'KTMT', 7),
('MMT01', 'MMT', 1),
('MMT02', 'MMT', 2),
('MMT03', 'MMT', 3),
('MMT04', 'MMT', 4),
('MMT05', 'MMT', 5),
('MMT06', 'MMT', 6),
('MMT07', 'MMT', 7);

-- --------------------------------------------------------

--
-- Table structure for table `loplt`
--

CREATE TABLE IF NOT EXISTS `loplt` (
  `MaLop` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MaGV` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `MaMH` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phong` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Thu` int(1) DEFAULT NULL,
  `Ca` int(1) DEFAULT NULL,
  `Min` int(2) DEFAULT NULL,
  `Max` int(3) DEFAULT NULL,
  `SLHT` int(3) DEFAULT NULL,
  `ThoiLuong` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '15 tuần',
  `NgayBD` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '14-01-2014',
  `NgayKT` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '02-05-2014',
  PRIMARY KEY (`MaLop`),
  KEY `FK_lop_mh` (`MaMH`),
  KEY `FK_lop_gv` (`MaGV`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `loplt`
--

INSERT INTO `loplt` (`MaLop`, `MaGV`, `MaMH`, `Phong`, `Thu`, `Ca`, `Min`, `Max`, `SLHT`, `ThoiLuong`, `NgayBD`, `NgayKT`) VALUES
('WINP1.C21', 'MINHLN', 'WINP1', '101', 2, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('CNET1.C21', 'QUANGTNN', 'CNET1', '102', 2, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('CNET1.C22', 'HAIDAM', 'CNET1', '103', 2, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('CNET1.C23', 'QUANGTNN', 'CNET1', '101', 2, 2, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('ITEW1.C21', 'HUNGLK', 'ITEW1', '101', 3, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('ITEW1.C22', 'HUNGLK', 'ITEW1', '101', 3, 2, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('HCMT1.C21', 'HUNGLK', 'HCMT1', '207', 3, 3, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('HCMT1.C22', 'MINHLN', 'HCMT1', '101', 3, 3, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT106.C21', 'NHIEMTB', 'NT106', '208', 4, 2, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT106.C22', 'NGHILH', 'NT106', '101', 4, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT109.C21', 'HUNGLK', 'NT109', '102', 4, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT109.C22', 'MINHLN', 'NT109', '203', 5, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT201.C21', 'HUNGLK', 'NT201', '108', 6, 3, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT201.C22', 'MINHLN', 'NT201', '101', 7, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT111.C21', 'NGHILH', 'NT111', '101', 7, 3, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT111.C22', 'QUANGTNN', 'NT111', '208', 6, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT107.C21', 'HAIDAM', 'NT107', '101', 6, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT107.C22', 'NAMNGUYEN', 'NT107', '102', 6, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT102.C21', 'HUNGLK', 'NT102', '205', 5, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT102.C22', 'MINHLN', 'NT102', '201', 5, 4, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT107.C23', 'MINHLN', 'NT107', '101', 5, 2, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT108.C21', 'NHIEMTB', 'NT108', '103', 5, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT108.C22', 'NAMNGUYEN', 'NT108', '101', 5, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT113.C21', 'DUNGVT', 'NT113', '104', 2, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT113.C22', 'NGHILH', 'NT113', '101', 2, 3, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT501.C21', 'HAIDAM', 'NT501', '102', 2, 2, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT301.C21', 'DUNGVT', 'NT301', '102', 5, 2, 30, 100, 1, '15 tuần', '14-01-2014', '02-05-2014'),
('NT301.C22', 'NGHILH', 'NT301', '304', 5, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT305.C21', 'TUANNGUYEN', 'NT305', '101', 6, 2, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT305.C22', 'NGHILH', 'NT305', '103', 6, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT307.C21', 'MINHNQ', 'NT307', '105', 2, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT307.C22', 'MINHLN', 'NT307', '302', 4, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT302.C21', 'DUYN', 'NT302', '103', 2, 2, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT303.C21', 'HAIDAM', 'NT303', '101', 4, 2, 30, 100, 1, '15 tuần', '14-01-2014', '02-05-2014'),
('NT303.C22', 'MINHLN', 'NT303', '104', 6, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT304.C21', 'QUANGTNN', 'NT304', '103', 4, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014'),
('NT304.C22', 'NAMNGUYEN', 'NT304', '208', 4, 1, 30, 100, 0, '15 tuần', '14-01-2014', '02-05-2014');

-- --------------------------------------------------------

--
-- Table structure for table `lopth`
--

CREATE TABLE IF NOT EXISTS `lopth` (
  `MaLop` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MaGV` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `MaMH` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phong` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Thu` int(1) DEFAULT NULL,
  `Ca` int(1) DEFAULT NULL,
  `Min` int(2) DEFAULT NULL,
  `Max` int(3) DEFAULT NULL,
  `SLHT` int(3) DEFAULT NULL,
  PRIMARY KEY (`MaLop`),
  KEY `FK_lop_mh` (`MaMH`),
  KEY `FK_lop_gv` (`MaGV`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mhtuongduong`
--

CREATE TABLE IF NOT EXISTS `mhtuongduong` (
  `ID_OLD` int(3) NOT NULL,
  `ID_NEW` int(3) NOT NULL,
  PRIMARY KEY (`ID_OLD`,`ID_NEW`),
  KEY `MaMHMoi` (`ID_NEW`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mhtuongduong`
--

INSERT INTO `mhtuongduong` (`ID_OLD`, `ID_NEW`) VALUES
(58, 58),
(60, 61);

-- --------------------------------------------------------

--
-- Table structure for table `molop`
--

CREATE TABLE IF NOT EXISTS `molop` (
  `MaMH` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `NgayDK` datetime NOT NULL,
  `MSSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaMH`,`MSSV`),
  KEY `FK_ml_sv` (`MSSV`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `molop`
--

INSERT INTO `molop` (`MaMH`, `NgayDK`, `MSSV`) VALUES
('MAT22', '2012-05-29 21:43:15', '09520044');

-- --------------------------------------------------------

--
-- Table structure for table `moncn`
--

CREATE TABLE IF NOT EXISTS `moncn` (
  `ID` int(11) NOT NULL,
  `MaCN` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`,`MaCN`),
  KEY `MaCN` (`MaCN`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `moncn`
--

INSERT INTO `moncn` (`ID`, `MaCN`) VALUES
(2, 'CNPM_4_2'),
(2, 'HTTT_4_2'),
(2, 'MMT_4_2'),
(2, 'MMT_5_2'),
(2, 'MMT_6_2'),
(22, 'CNPM_4_1'),
(22, 'HTTT_4_1'),
(22, 'MMT_4_1'),
(22, 'MMT_5_1'),
(22, 'MMT_6_1'),
(26, 'CNPM_4_1'),
(26, 'HTTT_4_1'),
(26, 'MMT_4_1'),
(26, 'MMT_5_1'),
(26, 'MMT_6_1'),
(32, 'CNPM_4_1'),
(32, 'HTTT_4_1'),
(32, 'MMT_4_1'),
(32, 'MMT_5_1'),
(32, 'MMT_6_1'),
(33, 'CNPM_4_2'),
(33, 'HTTT_4_2'),
(33, 'MMT_4_2'),
(33, 'MMT_5_2'),
(33, 'MMT_6_2'),
(35, 'CNPM_4_2'),
(35, 'HTTT_4_2'),
(35, 'MMT_4_2'),
(35, 'MMT_5_2'),
(35, 'MMT_6_2');

-- --------------------------------------------------------

--
-- Table structure for table `monhoc`
--

CREATE TABLE IF NOT EXISTS `monhoc` (
  `ID` int(3) NOT NULL AUTO_INCREMENT,
  `MaMH` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TenMH` varchar(62) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SoTC` int(2) DEFAULT NULL,
  `TCLT` int(2) DEFAULT NULL,
  `TCTH` int(2) DEFAULT NULL,
  `Loai` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DC',
  `KieuMH` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DON',
  `TietLT` int(11) NOT NULL,
  `TietTH` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=89 ;

--
-- Dumping data for table `monhoc`
--

INSERT INTO `monhoc` (`ID`, `MaMH`, `TenMH`, `SoTC`, `TCLT`, `TCTH`, `Loai`, `KieuMH`, `TietLT`, `TietTH`) VALUES
(1, 'SE103', 'Các Phương Pháp Lập Trình', 3, 2, 1, 'CN', 'DON', 30, 15),
(2, 'NT303', 'Công nghệ thoại IP', 3, 2, 1, 'CN', 'DON', 30, 15),
(3, 'SE323', 'Đồ án 1', 4, 3, 1, 'CN', 'DON', 30, 15),
(4, 'SE321', 'Đồ án 2', 4, 3, 1, 'CN', 'DON', 30, 15),
(5, 'SE106', 'Đồ Án môn học', 4, 4, 0, 'CN', 'DON', 30, 15),
(6, 'NT504', 'Đồ án tốt nghiệp', 4, 0, 4, 'CN', 'DON', 30, 15),
(7, 'NT30*', 'Học Phần Chuyên Nghành 1', 3, 2, 1, 'CN', 'NHOM', 30, 15),
(8, 'NT30*', 'Học phần chuyên nghành 2', 3, 2, 1, 'CN', 'NHOM', 30, 15),
(9, 'NT30*', 'Học phần chuyên nghành 3', 3, 2, 1, 'CN', 'NHOM', 30, 15),
(10, 'NT505', 'Khóa luận tốt nghiệp', 10, 0, 10, 'CN', 'DON', 30, 15),
(11, 'SE208', 'Kiểm Chứng Phần Mềm', 3, 2, 1, 'CN', 'DON', 30, 15),
(12, 'NT306', 'Kỹ thuật lập trình trên Linux', 3, 2, 1, 'CN', 'DON', 30, 15),
(13, 'SE105', 'Lập Trình Nhúng Căn Bản', 3, 2, 1, 'CN', 'DON', 30, 15),
(14, 'SE417', 'Mã nguồn mở 1', 2, 2, 0, 'CN', 'DON', 30, 15),
(15, 'NT502', 'Môn tốt nghiệp 1', 3, 0, 3, 'CN', 'DON', 30, 15),
(16, 'NT503', 'Môn tốt nghiệp 2', 3, 0, 0, 'CN', 'DON', 30, 15),
(17, 'SE104', 'Nhập Môn Phần Mềm', 4, 3, 1, 'CN', 'DON', 30, 15),
(18, 'SE102', 'Nhập môn phát triển game', 3, 2, 1, 'CN', 'DON', 30, 15),
(19, 'SE207', 'Phân tích thiết kế HT', 4, 3, 1, 'CN', 'DON', 30, 15),
(20, 'SE211', 'Phát triển phần mềm di động', 4, 3, 1, 'CN', 'DON', 30, 15),
(21, 'SE212', 'Phát triển phần mềm mã nguồn mở', 3, 2, 1, 'CN', 'DON', 30, 15),
(22, 'NT305', 'Phát triển ứng dụng di động', 3, 2, 1, 'CN', 'DON', 30, 15),
(23, 'SE209', 'Phát triển, bảo hành phần mềm', 3, 3, 0, 'CN', 'DON', 30, 15),
(24, 'SE101', 'Phương pháp mô hình hóa', 3, 3, 0, 'CN', 'DON', 30, 15),
(25, 'SE210', 'Quản lý dự án', 4, 3, 1, 'CN', 'DON', 30, 15),
(26, 'NT301', 'Quản trị hệ thống mạng', 3, 2, 1, 'CN', 'DON', 30, 15),
(27, 'NT501', 'Thực tập doanh nghiệp', 3, 0, 3, 'CN', 'DON', 30, 15),
(28, 'NT40*', 'Tự chọn 1', 3, 2, 1, 'TC', 'NHOM', 30, 15),
(29, 'NT40*', 'Tự chọn 2', 3, 2, 1, 'TC', 'NHOM', 30, 15),
(30, 'NT40*', 'Tự chọn 3', 3, 2, 1, 'TC', 'NHOM', 30, 15),
(31, 'NT40*', 'Tự chọn 5', 3, 2, 1, 'TC', 'NHOM', 30, 15),
(32, 'NT307', 'Ứng dụng lập trình web', 3, 2, 1, 'CN', 'DON', 30, 15),
(33, 'NT304', 'Ứng dụng truyền thông và an ninh thông tin', 3, 2, 1, 'CN', 'DON', 30, 15),
(34, 'STA01', 'Xác suất thống kê', 3, 3, 0, 'CN', 'DON', 30, 15),
(35, 'NT302', 'Xây dựng chuẩn chính sách', 3, 2, 1, 'CN', 'DON', 30, 15),
(36, 'SE213', 'Xử lý phân bố', 3, 2, 1, 'CN', 'DON', 30, 15),
(37, 'NT101', 'An Toàn Mạng Máy Tính', 4, 3, 1, 'CSN', 'DON', 30, 15),
(38, 'NT112', 'Công Nghệ Mạng Viễn Thông', 4, 3, 1, 'CSN', 'DON', 30, 15),
(39, 'NT102', 'Điện Tử Trong Công Nghệ Thông Tin', 4, 3, 1, 'CSN', 'DON', 30, 15),
(40, 'NT203', 'Đồ Án Chuyên Nghành', 2, 0, 2, 'CSN', 'DON', 30, 15),
(41, 'NT202', 'Đồ Án Lập Trình Ứng Dụng Mạng', 2, 0, 2, 'CSN', 'DON', 30, 15),
(42, 'NT103', 'Hệ Điều Hành Linux', 4, 3, 1, 'CSN', 'DON', 30, 15),
(43, 'NT106', 'Lập Trình Mạng Căn Bản', 3, 2, 1, 'CSN', 'DON', 30, 15),
(44, 'NT109', 'Lập Trình Ứng Dụng Mạng', 3, 2, 1, 'CSN', 'DON', 30, 15),
(45, 'NT104', 'Lý Thuyết Thông Tin', 3, 3, 0, 'CSN', 'DON', 30, 15),
(46, 'NT108', 'Mạng Truyền Thông & Di Động', 3, 2, 1, 'CSN', 'DON', 30, 15),
(47, 'NT201', 'Phân tích và thiết kế hệ thống', 3, 3, 0, 'CSN', 'DON', 30, 15),
(48, 'NT111', 'Thiết Bị Mạng và Truyền Thông', 4, 3, 1, 'CSN', 'DON', 30, 15),
(49, 'NT113', 'Thiết kế mạng', 3, 2, 1, 'CSN', 'DON', 30, 15),
(50, 'NT110', 'Tín hiệu & Mạch', 3, 3, 0, 'CSN', 'DON', 30, 15),
(51, 'NT105', 'Truyền Dữ Liệu', 4, 3, 1, 'CSN', 'DON', 30, 15),
(52, 'NT107', 'Xử Lý Tín Hiệu Số', 4, 3, 1, 'CSN', 'DON', 30, 15),
(53, 'ENG01', 'Anh văn 1', 5, 5, 0, 'DC', 'DON', 30, 15),
(54, 'ENG02', 'Anh văn 2', 5, 5, 0, 'DC', 'DON', 30, 15),
(55, 'ENG03', 'Anh văn 3', 2, 3, 0, 'DC', 'DON', 30, 15),
(56, 'ENG04', 'Anh văn 4', 3, 3, 0, 'DC', 'DON', 30, 15),
(57, 'DSAL1', 'Cấu trúc dữ liệu và giải thuật', 3, 3, 0, 'DC', 'DON', 30, 15),
(58, 'MAT04', 'Cấu Trúc Rời Rạc', 4, 3, 1, 'DC', 'DON', 30, 15),
(59, 'DBSS1', 'Cơ sở dữ liệu', 4, 4, 0, 'DC', 'DON', 30, 15),
(60, 'LIA01', 'Đại Số Tuyến Tính', 3, 3, 0, 'DC', 'DON', 30, 15),
(61, 'VCPL1', 'Đường Lối Đảng Cộng Sản Việt Nam', 3, 3, 0, 'DC', 'DON', 30, 15),
(62, 'MEDU1', 'Giáo Dục Quốc Phòng', 4, 4, 0, 'DC', 'DON', 30, 15),
(63, 'PEDU1', 'Giáo Dục Thể Chất 1', 5, 0, 5, 'DC', 'DON', 30, 15),
(64, 'PEDU2', 'Giáo Dục Thể Chất 2', 5, 0, 5, 'DC', 'DON', 30, 15),
(65, 'OSYS1', 'Hệ điều hành', 4, 3, 1, 'DC', 'DON', 30, 15),
(66, 'CARC1', 'Kiến trúc máy tính', 3, 3, 0, 'DC', 'DON', 30, 15),
(67, 'OOPT1', 'Lập Trình Hướng Đối Tượng', 4, 3, 1, 'DC', 'DON', 30, 15),
(68, 'WINP1', 'Lập Trình Windows', 4, 3, 1, 'DC', 'DON', 30, 15),
(69, 'CNET1', 'Mạng Máy Tính', 4, 4, 0, 'DC', 'DON', 30, 15),
(70, 'ITEW1', 'Nhập môn công tác kỹ sư', 2, 2, 0, 'DC', 'DON', 30, 15),
(71, 'ITEM1', 'Nhập môn quảng trị doanh nghiệp', 2, 2, 0, 'DC', 'DON', 30, 15),
(72, 'PHIL2', 'Những Nguyên Lý Cơ Bản Mac-Lenin', 5, 5, 0, 'DC', 'DON', 30, 15),
(73, 'SMET1', 'Phương pháp luận sáng tạo', 2, 2, 0, 'DC', 'DON', 30, 15),
(74, 'CSC21', 'Tin học đại cương', 4, 3, 1, 'DC', 'DON', 30, 15),
(75, 'MAT21', 'Toán cao cấp A1', 3, 3, 0, 'DC', 'DON', 30, 15),
(76, 'MAT22', 'Toán cao cấp A2', 3, 3, 0, 'DC', 'DON', 30, 15),
(77, 'HCMT1', 'Tư tưởng hồ chí minh', 2, 2, 0, 'DC', 'DON', 30, 15),
(78, 'PHY01', 'Vật lý đại cương A1', 3, 3, 0, 'DC', 'DON', 30, 15),
(79, 'PHY02', 'Vật lý đại cương A2', 3, 3, 0, 'DC', 'DON', 30, 15),
(83, 'NT401', 'An toàn mạng nâng cao', 3, 2, 1, 'TC', 'DON', 30, 15),
(84, 'NT406', 'Hệ thống thời gian thực', 3, 2, 1, 'TC', 'DON', 30, 15),
(85, 'NT402', 'Thiết kế phần mềm diệt virus', 3, 2, 1, 'TC', 'DON', 30, 15),
(86, 'NT403', 'Tính toán lưới', 3, 2, 1, 'TC', 'DON', 30, 15),
(87, 'NT405', 'Truyền thông quang', 3, 2, 1, 'TC', 'DON', 30, 15),
(88, 'NT404', 'Truyền thông vệ tin', 3, 2, 1, 'TC', 'DON', 30, 15);

-- --------------------------------------------------------

--
-- Table structure for table `monhoc_nhom`
--

CREATE TABLE IF NOT EXISTS `monhoc_nhom` (
  `ID` int(3) NOT NULL,
  `MaNhom` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`,`MaNhom`),
  KEY `MaNhom` (`MaNhom`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monhoc_nhom`
--

INSERT INTO `monhoc_nhom` (`ID`, `MaNhom`) VALUES
(2, 'NT30*'),
(2, 'NT40*'),
(6, 'NT50*'),
(10, 'NT50*'),
(12, 'NT30*'),
(12, 'NT40*'),
(15, 'NT50*'),
(16, 'NT50*'),
(22, 'NT30*'),
(22, 'NT40*'),
(26, 'NT30*'),
(26, 'NT40*'),
(32, 'NT30*'),
(32, 'NT40*'),
(33, 'NT30*'),
(33, 'NT40*'),
(35, 'NT30*'),
(35, 'NT40*'),
(83, 'NT40*'),
(84, 'NT40*'),
(85, 'NT40*'),
(86, 'NT40*'),
(87, 'NT40*'),
(88, 'NT40*');

-- --------------------------------------------------------

--
-- Table structure for table `monhoc_nhom_info`
--

CREATE TABLE IF NOT EXISTS `monhoc_nhom_info` (
  `MaNhom` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TenNhom` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaNhom`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monhoc_nhom_info`
--

INSERT INTO `monhoc_nhom_info` (`MaNhom`, `TenNhom`) VALUES
('NT30*', 'Nhóm chuyên nghành khoa MMT'),
('NT40*', 'Nhóm tự chọn khoa MMT'),
('NT50*', 'Khóa Luận');

-- --------------------------------------------------------

--
-- Table structure for table `phong`
--

CREATE TABLE IF NOT EXISTS `phong` (
  `TenPhong` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`TenPhong`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `phong`
--

INSERT INTO `phong` (`TenPhong`) VALUES
('101'),
('102'),
('103'),
('104'),
('105'),
('106'),
('107'),
('108'),
('201'),
('202'),
('203'),
('204'),
('205'),
('206'),
('207'),
('208'),
('301'),
('302'),
('303'),
('304'),
('305'),
('306'),
('307'),
('308'),
('PM1'),
('PM2');

-- --------------------------------------------------------

--
-- Table structure for table `quanlylop`
--

CREATE TABLE IF NOT EXISTS `quanlylop` (
  `MaLop` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `MaGV` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `MaMH` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Thu` int(1) DEFAULT NULL,
  `TietBD` int(11) NOT NULL,
  `TietKT` int(11) NOT NULL,
  PRIMARY KEY (`MaLop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quanlylop`
--

INSERT INTO `quanlylop` (`MaLop`, `MaGV`, `MaMH`, `Thu`, `TietBD`, `TietKT`) VALUES
('NT101.C11', 'GV02', 'NT101', 7, 9, 10),
('NT305.C11', 'GV03', 'NT305', 6, 6, 9),
('NT303.C11', 'GV01', 'NT303', 7, 6, 8);

-- --------------------------------------------------------

--
-- Table structure for table `sv_cnpm`
--

CREATE TABLE IF NOT EXISTS `sv_cnpm` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TenSV` varchar(27) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Lop` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `K` int(2) NOT NULL,
  `NgaySinh` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `NoiSinh` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `SDT` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `MaCN` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaSV`),
  KEY `K` (`K`),
  KEY `Lop` (`Lop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sv_cnpm`
--

INSERT INTO `sv_cnpm` (`MaSV`, `TenSV`, `Lop`, `K`, `NgaySinh`, `NoiSinh`, `SDT`, `email`, `MaCN`) VALUES
('08520005', 'Thái Thu Thủy', 'CNPM05', 5, '10/12/1990', 'Vũng Tàu', '271899100', '', ''),
('08520221', 'Phan Thiên Quốc', 'CNPM03', 4, '*', '*', '491881111', '', ''),
('08520234', 'Phan Thiên Quốc', 'CNPM04', 4, '*', '*', '*', '', ''),
('08520462', 'Trương Hoàng An', 'CNPM04', 4, '*', '*', '900881134', '', ''),
('09520008', 'Quách Minh', 'CNPM04', 4, '', 'Đồng Nai', '', '', ''),
('09520035', 'Mai Tiến Hoài', 'CNPM02', 4, '18/02/1989', 'Khánh Hòa', '288179919', '', ''),
('09520234', 'Phan Thiên Quốc', 'CNPM04', 4, '*', '*', '*', '', ''),
('10520221', 'Phan Thiên Quốc', 'CNPM04', 4, '*', '*', '987777113', '', ''),
('10520234', 'Phan Quốc Vinh', 'CNPM04', 4, '*', '*', '991922415', '', ''),
('10520235', 'Phan Thiên Quốc', 'CNPM04', 4, '*', '*', '*', '', ''),
('11520221', 'Phan Thiên Quốc', 'CNPM04', 4, '*', '*', '*', '', ''),
('11520235', 'Phan Mạnh Tiến', 'CNPM04', 4, '*', '*', '271999411', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sv_httt`
--

CREATE TABLE IF NOT EXISTS `sv_httt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TenSV` varchar(27) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Lop` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `K` int(2) NOT NULL,
  `NgaySinh` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `NoiSinh` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `SDT` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `MaCN` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaSV`),
  KEY `K` (`K`),
  KEY `Lop` (`Lop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sv_httt`
--

INSERT INTO `sv_httt` (`MaSV`, `TenSV`, `Lop`, `K`, `NgaySinh`, `NoiSinh`, `SDT`, `email`, `MaCN`) VALUES
('08520235', 'Phan Thiên Quốc', 'HTTT04', 4, '*', 'Vinh', '009113313', '', ''),
('09520045', 'Hà Tú Anh', 'HTTT04', 4, '*', '*', '01699938918', '', '0'),
('09520048', 'Đồng Minh', 'HTTT03', 1, '', '', '', '', '0'),
('09520113', 'Hà Minh', 'HTTT04', 1, '', '', '', '', '0'),
('09520460', 'Trương Hoàng An', 'HTTT04', 4, '*', '*', '*', '', '0'),
('09520462', 'Trương Hoàng An', 'HTTT04', 4, '*', '*', '*', '', '0'),
('09520478', 'Trương Hoàng An', 'HTTT04', 4, '*', '*', '*', '', '0'),
('10520460', 'Trương Hoàng An', 'HTTT04', 4, '*', '*', '*', '', '0'),
('10520462', 'Trương Hoàng An', 'HTTT04', 4, '*', '*', '*', '', '0'),
('10520478', 'Trương Hoàng An', 'HTTT04', 4, '*', '*', '*', '', '0'),
('11520460', 'Trương Hoàng An', 'HTTT04', 4, '*', '*', '*', '', '0'),
('11520462', 'Trương Hoàng An', 'HTTT04', 5, '*', '*', '881922144', '', '0'),
('11520478', 'Trương Hoàng An', 'HTTT04', 4, '*', '*3', '818811243', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `sv_khmt`
--

CREATE TABLE IF NOT EXISTS `sv_khmt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TenSV` varchar(27) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Lop` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `K` int(2) NOT NULL,
  `NgaySinh` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `NoiSinh` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `SDT` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `MaCN` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaSV`),
  KEY `K` (`K`),
  KEY `Lop` (`Lop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sv_khmt`
--

INSERT INTO `sv_khmt` (`MaSV`, `TenSV`, `Lop`, `K`, `NgaySinh`, `NoiSinh`, `SDT`, `email`, `MaCN`) VALUES
('08520004', 'Đinh Tiến Đạt', 'KHMT04', 4, '20/10/2010', 'Nha Trang', '0990113422', 'rinodung.uit@gmail.com', ''),
('08520031', 'Nguyên Phúc', 'KHMT04', 4, '27/02/1986', 'Đồng Nai', '271918441', '', ''),
('08520047', 'Minh Trí', 'KHMT04', 4, '', '', '', '', ''),
('08520055', 'Nguyễn Văn Ninh', 'KHMT04', 4, '18/08/1992', 'Bình Dương', '222288181', '', ''),
('08520056', 'Nguyễn Văn AB', 'KHMT04', 4, '12/01/1992', 'Hà Nam', '261556445', '', ''),
('08524566', 'Nguyễn Văn Bình', 'KHMT04', 4, '20/02/1881', 'Bình Thuận', '888122134', '', ''),
('09520005', 'Hải Hưng ', 'KHMT04', 4, '', '', '', 'abc.uit@gmail.com', ''),
('09520052', 'Nguyễn Văn A', 'KHMT04', 4, '*', '*', '*', '', ''),
('09520055', 'Nguyễn Văn A', 'KHMT01', 4, '*', '*', '281991441', '', ''),
('09520056', 'Nguyễn Văn A', 'KHMT04', 4, '*', '*', '*', '', ''),
('10520052', 'Nguyễn Văn A', 'KHMT04', 4, '*', '*', '*', '', ''),
('10520055', 'Nguyễn Văn A', 'KHMT04', 4, '*', '*', '*', '', ''),
('10520056', 'Nguyễn Văn A', 'KHMT04', 4, '*', '*', '*', '', ''),
('11520052', 'Nguyễn Văn A', 'KHMT04', 4, '*', '*', '*', '', ''),
('11520055', 'Nguyễn Văn A', 'KHMT04', 4, '*', '*', '*', '', ''),
('11520056', 'Nguyễn Văn A', 'KHMT04', 4, '*', '*', '*', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sv_ktmt`
--

CREATE TABLE IF NOT EXISTS `sv_ktmt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TenSV` varchar(27) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Lop` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `K` int(2) NOT NULL,
  `NgaySinh` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `NoiSinh` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `SDT` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `MaCN` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaSV`),
  KEY `K` (`K`),
  KEY `Lop` (`Lop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sv_ktmt`
--

INSERT INTO `sv_ktmt` (`MaSV`, `TenSV`, `Lop`, `K`, `NgaySinh`, `NoiSinh`, `SDT`, `email`, `MaCN`) VALUES
('08520001', 'Lê Quốc Bảo', 'KTMT04', 4, '29/2/2000', 'Hà Tĩnh', '881228991', '', ''),
('08520112', 'Âu Đình Phong', 'KTMT04', 4, '*', 'Hải Phòng', '727112994', '', ''),
('08520115', 'Hải Hùng', 'KTMT04', 4, '', 'Nha Trang', '991221218', '', ''),
('08520211', 'Lê Quốc B', 'KTMT04', 4, '29/2/2000', 'Hà Tĩnh', '881228991', '', ''),
('08520460', 'Trương Hoàng An', 'KTMT04', 4, '*', '*', '098113441', '', ''),
('09520111', 'Lê Quốc Hưng', 'KTMT04', 4, '*', '*', '016888388', '', ''),
('09520112', 'Lê Quốc Hưng', 'KTMT04', 4, '*', '*', '*', '', ''),
('09520115', 'Lê Quốc Hưng', 'KTMT04', 4, '*', '*', '*', '', ''),
('10520111', 'Lê Quốc Hưng', 'KTMT04', 4, '*', '*', '*', '', ''),
('10520112', 'Lê Quốc Hưng', 'KTMT04', 4, '*', '*', '*', '', ''),
('10520115', 'Lê Quốc Hưng', 'KTMT04', 4, '*', '*', '*', '', ''),
('11520111', 'Hà Bảo Nam', 'KTMT04', 6, '*', '*', '271953112', '', ''),
('11520112', 'Lê Quốc Hưng', 'KTMT04', 4, '*', '*', '*', '', ''),
('11520115', 'Lê Quốc Hưng', 'KTMT04', 4, '*', '*', '*', '', ''),
('11520215', 'Lê Quốc Hưng', 'KTMT04', 4, '*', '*', '*', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sv_mmt`
--

CREATE TABLE IF NOT EXISTS `sv_mmt` (
  `MaSV` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TenSV` varchar(27) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Lop` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `K` int(2) NOT NULL,
  `NgaySinh` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `NoiSinh` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `SDT` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `MaCN` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaSV`),
  KEY `K` (`K`),
  KEY `Lop` (`Lop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sv_mmt`
--

INSERT INTO `sv_mmt` (`MaSV`, `TenSV`, `Lop`, `K`, `NgaySinh`, `NoiSinh`, `SDT`, `email`, `MaCN`) VALUES
('09520032', 'Nguyễn Quý Danh', 'MMT04', 4, '*', '*', '0121113331', '', 'MMT_4_1'),
('09520044', 'Đồng Tiến Dũng', 'MMT04', 4, '*', '*', '01699938919', '', ''),
('09520221', 'Phan Thiên Quốc', 'MMT04', 4, '*', '*', '271881331', '', ''),
('10520002', 'Hồ Trần Bắc An', 'MMT04', 4, '*', '*', '*', '', ''),
('09520001', 'Hồ Trần Bắc An', 'MMT04', 4, '*', '*', '098231455', '', ''),
('10520011', 'Hồ Trần Bắc An', 'MMT04', 4, '*', '*', '*', '', ''),
('10520032', 'Nguyễn Quý Danh', 'MMT04', 4, '*', '*', '*', '', ''),
('10520033', 'Nguyễn Quý Danh', 'MMT04', 4, '*', '*', '*', '', ''),
('10520040', 'Đồng Tiến Dũng', 'MMT04', 4, '*', '*', '*', '', ''),
('10520041', 'Nguyễn Quý Danh2', 'MMT04', 4, '*', '*', '*', '', ''),
('10520045', 'Hồ Hoài Anh', 'MMT04', 4, '*', '*', '*', '', ''),
('10520130', 'Võ Đoàn Như Khánh', 'MMT04', 4, '*', '*', '*', '', ''),
('10520131', 'Võ Đoàn Như Khánh', 'MMT04', 4, '*', '*', '*', '', ''),
('10520137', 'Võ Đoàn Như Khánh', 'MMT04', 4, '*', '*', '0', 'rinodung.uit@gmail.com', ''),
('11520001', 'Hồ Trần Bắc An', 'MMT04', 4, '*', '*', '*', '', ''),
('11520002', 'Hồ Trần Bắc An', 'MMT04', 4, '*', '*', '*', '', ''),
('11520011', 'Hồ Trần Bắc An', 'MMT04', 4, '*', '*', '*', '', ''),
('11520033', 'Nguyễn Quý Danh', 'MMT04', 4, '*', '*', '*', '', ''),
('11520040', 'Đồng Tiến Dũng', 'MMT04', 5, '*', '*', '0911129913', '', ''),
('11520041', 'Nguyễn Quý Danh', 'MMT04', 4, '*', '*', '*', '', ''),
('11520044', 'Đồng Tiến Dũng', 'MMT04', 4, '*', '*', '*', '', ''),
('11520045', 'Đồng Tiến Dũng', 'MMT04', 4, '*', '*', '*', '', ''),
('11520130', 'Võ Đoàn Như Khánh', 'MMT04', 4, '*', '*', '*', '', ''),
('11520132', 'Võ Đoàn Như Khánh', 'MMT04', 4, '*', '*', '*', '', ''),
('09520002', 'Phan Tuấn Anh', 'MMT04', 4, '*', '*', '*', '', ''),
('09520003', 'Trần Minh Tuấn Anh', 'MMT04', 4, '*', '*', '*', '', ''),
('09520004', 'Lê Trung Chánh', 'MMT04', 4, '*', '*', '*', '', ''),
('09520005', 'Lê Quang Công', 'MMT04', 4, '*', '*', '*', '', ''),
('09520006', 'Nguyễn Thế Cường', 'MMT04', 4, '*', '*', '*', '', ''),
('09520007', 'Phạm Văn Cường', 'MMT04', 4, '*', '*', '*', '', ''),
('09520008', 'Hứa Vĩ Cường', 'MMT04', 4, '*', '*', '*', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `taikhoan`
--

CREATE TABLE IF NOT EXISTS `taikhoan` (
  `Username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `last_active` int(11) NOT NULL,
  `Type` int(11) NOT NULL,
  `Khoa` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `MXT` int(11) NOT NULL,
  `Status` varchar(1) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `taikhoan`
--

INSERT INTO `taikhoan` (`Username`, `Password`, `last_active`, `Type`, `Khoa`, `MXT`, `Status`) VALUES
('09520032', 'e10adc3949ba59abbe56e057f20f883e', 1392370723, 2, 'mmt', 0, '1'),
('09520044', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 'mmt', 0, '1'),
('admin', 'e10adc3949ba59abbe56e057f20f883e', 1386407047, 0, '', 0, '1'),
('admin', 'e10adc3949ba59abbe56e057f20f883e', 1386407047, 0, '', 1, '1'),
('GV01', 'e10adc3949ba59abbe56e057f20f883e', 1389111838, 1, '', 0, '1'),
('GV02', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('GV03', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('09520001', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 'mmt', 0, '1'),
('09520002', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 'mmt', 0, '1'),
('09520004', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 'mmt', 0, '1'),
('09520003', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 'mmt', 0, '1'),
('09520005', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 'mmt', 0, '1'),
('09520006', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 'mmt', 0, '1'),
('09520007', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 'mmt', 0, '1'),
('09520008', 'e10adc3949ba59abbe56e057f20f883e', 0, 2, 'mmt', 0, '1'),
('HAIDAM', 'e10adc3949ba59abbe56e057f20f883e', 1392372936, 1, '', 0, '1'),
('NAMNGUYEN', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('TUANNGUYEN', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('NHIEMTB', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('DUYN', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('NGHILH', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('MANHLE', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('MINHLN', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('HUNGTM', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('DUNGVT', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('MINHNQ', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('QUANGTNN', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('HUNGLK', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', 0, '1'),
('aaa', '', 0, 0, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `thongbao`
--

CREATE TABLE IF NOT EXISTS `thongbao` (
  `id` int(1) NOT NULL DEFAULT '0',
  `title` varchar(93) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `content` varchar(404) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attach` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `thongbao`
--

INSERT INTO `thongbao` (`id`, `title`, `date`, `content`, `attach`) VALUES
(1, 'Thông báo lịch thi và danh sách phòng thi HK(2011-2012)', '2012-03-01 00:00:00', 'Dựa vào kế hoạch học tập của trường Đại Học Công Nghệ Thông. Nay Phòng Đào Tạo chính thức đưa ra lịch thi và danh sách phòng thi Học Kỳ 2(2011-2012).\nĐề nghị sinh viên theo dõi và thực thi, tránh những trường hợp đáng tiếc.\nThông tin chi tiết có trong tập tin đính kèm.', ''),
(2, 'Thông Báo Mở Lớp:CARC1.C27', '2012-04-01 12:36:40', 'PDT thông báo Lớp:CARC1.C27 bắt đầu học ngày 12/04/2012 tại lầu 1 toà nhà mới của trường (sáng thứ 5).\nĐồng thời sinh viên xem danh sách lớp học trong tập tin đính kèm ', ''),
(3, 'Thông báo đóng tiền các môn học chính trị.', '2012-04-04 11:33:00', 'Trung tâm lý luận Chính trị - ĐHQGHCM thông báo:Các sinh viên khóa 1 và khóa 2 học lại các môn chính trị yêu cầu hoàn tất học phí  đầy đủ trước khi dự thi.\nĐịa điểm đóng học phí: phòng 707 -  Nhà Điều hành ĐHQG HCM', ''),
(4, 'Lich học lớp Bổ túc kiến thức năm 2012', '2012-04-26 12:36:40', 'Nhà trường thông báo đến các anh/chị đăng ký học Ôn lớp Bổ túc kiến thức lịch học năm 2012.\nRất mong các anh chị sắp xếp, tham dự lớp học đầy đủ.Phòng Đào tạo SĐH,KHCN&QHĐN.', 'danh sach on tap.xls');

-- --------------------------------------------------------

--
-- Table structure for table `thu`
--

CREATE TABLE IF NOT EXISTS `thu` (
  `TenThu` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `thu`
--

INSERT INTO `thu` (`TenThu`) VALUES
(2),
(3),
(4),
(5),
(6),
(7);

-- --------------------------------------------------------

--
-- Table structure for table `tiet`
--

CREATE TABLE IF NOT EXISTS `tiet` (
  `MaTiet` int(11) NOT NULL,
  `BatDau` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `KetThuc` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tiet`
--

INSERT INTO `tiet` (`MaTiet`, `BatDau`, `KetThuc`) VALUES
(1, '7h30', '8h15'),
(2, '8h15', '9h00'),
(3, '9h00', '9h45'),
(4, '10h00', '10h45'),
(5, '10h45', '11h30'),
(6, '13h00', '13h45'),
(7, '13h45', '14h30'),
(8, '14h30', '15h15'),
(9, '15h15', '16h00'),
(10, '16h00', '16h45');

-- --------------------------------------------------------

--
-- Table structure for table `tkb`
--

CREATE TABLE IF NOT EXISTS `tkb` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MaLop` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ID_LichHoc` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
